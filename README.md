<img src="doc/pics/banner.png">

# Virtual Terrain (`vterrain`)

Python package to generate two-dimensional terrain heightmaps for software rendering or video games.

[toc]

## License

This project is licensed under the GNU General Public License v3.0.

## Getting started

### Installation

At some point a `PyPi` package will be able but for now this has to be done manually.

Use git to clone this repository into your computer:
```shell
git clone https://gitlab.com/procedural2/vterrain
```

The package makes use of `f2py` and therefore requires to be built before usage: 
```shell
cd vterrain
python3 setup.py build
```

Add to the `PYTHONPATH` the path to the python lib that has just been build (replace `XXX` by the proper lib extension):
```
export PYTHONPATH="${PYTHONPATH}:[BUILD PATH/lib.XXX]"
```

### Usage examples

Examples are available in the [`doc/source/examples/src_python`](doc/source/examples/src_python) folder.

Producing a fractal heightmap and adding a bit of hydraulic erosion can be done in just a few lines:
```python
import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
w = vt.erosion.hydraulic_particles(w)
vt.plot.topo(w)

plt.show()
```

<img src="doc/pics/basic_example.png">

### Documentation

```shell
cd doc
make html
```

# Development roadmap

- Heightmap generation and alteration
  - [X] procedural noise (Perlin, fractal...)
  - controlled generation
    - [X] terrain surface from a set of points (thinplate interpolation)
    - [ ] terrain surface from a sketch / splatmap
    - [X] shape-conservative pointwise modifications (i.e. enforce elevation at some locations while keeping the overall shape of the heightmap)
- Physics-based mechanisms
  - erosion / deposition
    - [X] thermal
    - [X] hydraulic
    - [X] sediment deposition
    - [ ] wind / in progress
  - hydrology
    - [X] surface water system (stream and pool maps)
    - [ ] downcutting (vertical erosion of stream's bed or valley's floor)
    - [X] snow deposition
    - [ ] ice floe
    - [X] lava/glacier viscous flow
- Biomes
  - micro-climate
    - [ ] dominant wind
    - [X] precipitation map
    - [X] snowfall map
    - [X] sun exposure
  - [ ] soil type
  - [X] vegetation (flora)
  - [ ] animals (fauna)
- Object automatic placement
  - [ ] rocks, boulders, cliffs...
  - [ ] trees
  - [ ] cities
- Anthropic features
  - roads / paths
    - [X] communication network
    - [X] path/road digging and leveling
  - cities
    - [ ] ground leveling
    - [ ] inner structure (streets)
  - [ ] agriculture
- UI
  - [ ] splatmap generation
  - [ ] multiscale terrain class
  - [ ] gateways to Unity, Unreal Engine...
- Performances
  - [ ] multithreading



