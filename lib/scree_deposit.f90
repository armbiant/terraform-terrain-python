subroutine scree_deposition(z, talus, drop_map, iterations,&
     & n_particles, dt, gravity, p_mass, p_sediment, p_deposition,&
     & p_inertia, p_drag, radius, seed, kernel, nk, nx, ny)
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, nk, iterations, n_particles, seed
   real(8), intent(in) :: dt, gravity
   real(8), intent(in) :: p_mass, p_sediment, p_deposition
   real(8), intent(in) :: p_inertia, radius
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: drop_map, talus, p_drag
   real(8), intent(in), dimension(0:nk - 1, 0:nk - 1) :: kernel
   !f2py intent(in, out) :: z
   !
   integer :: ip, i, j, ib, jb, it, ir
   real(8) :: rd, x, y, u, v, grad(0:1), gn, vel(0:1), vel_new(0:1)
   real(8) :: vel_norm, dir(0:1), dir_new(0:1), surface_normal(0:2)
   real(8) :: force(0:1), drag(0:1)
   real(8) :: amount, drag_coeff, sediment, slope_coeff
   logical :: keep_looking
   real(8), dimension(0:nx - 1, 0:ny - 1) :: z0
   !
   real(8) :: bilinear_interp_array

   if (seed > 0) call set_rd_seed(seed)

   ! particle simulations
   do ip = 1, n_particles

      ! new particle
      keep_looking = .true.

      do while (keep_looking .eqv. .true.)

         ! initial position
         call random_number(rd)
         x = rd*real(nx - 1)
         call random_number(rd)
         y = rd*real(ny - 1)

         call get_ijuv(x, y, i, j, u, v)

         if (drop_map(i, j) > 0.05) keep_looking = .false.

      end do

      sediment = p_sediment*drop_map(i, j)

      ! particle parameters
      call random_number(rd)
      vel(0) = 0.1*(2*rd - 1)
      call random_number(rd)
      vel(1) = 0.1*(2*rd - 1)

      do it = 1, iterations

         ! normal (NB - dx = 1/nx and dy = 1/ny)
         call bilinear_grad(z, i, j, u, v, nx, ny, grad)
         surface_normal = (/-grad(0)*nx, -grad(1)*ny, 1d0/)
         surface_normal = surface_normal/sum(surface_normal**2)**0.5

         ! update drag coeff based on local gradient
         gn = sum(grad**2)**0.5
         if (gn > talus(i, j)) then
            drag_coeff = 0.
         else
            slope_coeff = (1d0 - gn/(talus(i, j) + 1e-30))**2
            drag_coeff = 0.+p_drag(i, j)*slope_coeff

            amount = dt*min(p_deposition*sediment*slope_coeff, sediment)
            if (radius < 1d0) then
               call depose(z, i, j, u, v, amount, nx, ny)
            else
               call depose_kernel(z, i, j, kernel, amount, nx, ny, (nk - 1)/2)
            end if
            sediment = sediment - amount

            if (sediment < 1e-2) exit
         end if

         ! forces
         dir = vel/sum(vel**2)**0.5
         force = p_mass*gravity*surface_normal(0:1)
         drag = -p_drag(i, j)*dir*sum(vel**2)

         ! update velocity
         vel_new = vel + dt/p_mass*(force + drag)

         ! velocity relaxation
         vel = (1d0 - dt*p_inertia)*vel_new + dt*p_inertia*vel

         ! exit if the particle stops
         if (sum(vel**2) < 1e-4) then
            amount = p_deposition*sediment*dt
            if (radius < 1d0) then
               call depose(z, i, j, u, v, amount, nx, ny)
            else
               call depose_kernel(z, i, j, kernel, amount, nx, ny, (nk - 1)/2)
            end if
            exit
         end if

         x = x + dt*vel(0)
         y = y + dt*vel(1)

         call get_ijuv(x, y, i, j, u, v)

         ! interrupt if the particle exit the domain
         if (x < 0) exit
         if (y < 0) exit
         if (x > nx - 1) exit
         if (y > ny - 1) exit

      end do

   end do

end subroutine scree_deposition
