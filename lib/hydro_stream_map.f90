subroutine stream_map(w, rmap, iterations, n_particles, moisture_map,&
     & p_inertia, seed, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, iterations, n_particles, seed
   real(8), intent(in) :: p_inertia
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: w, moisture_map
   real(8), intent(out), dimension(0:nx - 1, 0:ny - 1) :: rmap
   !
   integer :: i, j, i0, j0, ip, nit
   real(8) :: rd, x, y, xprev, yprev, u, v, direction(0:1), grad(0:1), water_pile

   if (seed > 0) call set_rd_seed(seed)

   rmap = 0.

   do ip = 1, n_particles

      ! initial position
      call random_number(rd)
      x = rd*real(nx - 1)
      call random_number(rd)
      y = rd*real(ny - 1)

      i = min(floor(x), nx - 1)
      j = min(floor(y), ny - 1)

      ! bakcup starting point
      i0 = i
      j0 = j

      water_pile = 0.
      direction = 0d0

      do nit = 1, iterations
         xprev = x
         yprev = y

         ! new direction and position
         call bilinear_grad(w, i, j, u, v, nx, ny, grad)

         ! new direction and position (but skip completely flat areas)
         if (grad(0)**2 + grad(1)**2 < 1e-30) exit

         direction = p_inertia*direction - grad*(1 - p_inertia)
         direction = direction/sum(direction**2)**0.5

         x = x + direction(0)
         y = y + direction(1)

         if (x < 0) exit
         if (y < 0) exit
         if (x > nx - 1) exit
         if (y > ny - 1) exit

         i = floor(x)
         j = floor(y)

         ! scale by the amount of displacement to avoid pile up of
         ! water where the particles are steady
         water_pile = water_pile + abs(x - xprev)*abs(y - yprev)*moisture_map(i0, j0)

         u = x - real(i)
         v = y - real(j)
         call depose(rmap, i, j, u, v, water_pile, nx, ny)
      end do

   end do

   rmap(0, :) = rmap(1, :)
   rmap(nx - 1, :) = rmap(nx - 2, :)

   rmap(:, 0) = rmap(:, 1)
   rmap(:, ny - 1) = rmap(:, ny - 2)

end subroutine stream_map
