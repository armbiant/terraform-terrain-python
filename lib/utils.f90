! some utils

subroutine set_rd_seed(myseed)
   implicit none
   integer :: myseed
   !
   integer :: n
   integer, allocatable :: seed(:)

   call random_seed(size=n)
   allocate (seed(n))
   seed = myseed
   call random_seed(put=seed)
   deallocate (seed)
end subroutine set_rd_seed

! operators

subroutine cross(a, b, s)
   implicit none
   real(8), intent(in) :: a(3), b(3)
   real(8), intent(inout) :: s(3)

   s(1) = a(2)*b(3) - a(3)*b(2)
   s(2) = a(3)*b(1) - a(1)*b(3)
   s(3) = a(1)*b(2) - a(2)*b(1)
end subroutine cross

subroutine get_ijuv(x, y, i, j, u, v)
   implicit none
   real(8), intent(in) :: x, y
   real(8), intent(out) :: u, v
   integer, intent(out) :: i, j
   i = floor(x)
   j = floor(y)
   u = x - real(i)
   v = y - real(j)
end subroutine get_ijuv

subroutine bilinear_grad(w, i, j, u, v, nx, ny, dw)
   implicit none
   real(8), intent(in) :: w(0:nx - 1, 0:ny - 1), u, v
   integer, intent(in) :: i, j, nx, ny
   real(8), dimension(0:1), intent(out) :: dw
   real(8) :: diff_i, diff_j, bilinear_interp

   dw(0) = bilinear_interp(diff_i(w, i, j, nx, ny), diff_i(w, i + 1, j, nx, ny), &
        &diff_i(w, i, j + 1, nx, ny), diff_i(w, i + 1, j + 1, nx, ny), u, v)
   dw(1) = bilinear_interp(diff_j(w, i, j, nx, ny), diff_j(w, i + 1, j, nx, ny), &
        &diff_j(w, i, j + 1, nx, ny), diff_j(w, i + 1, j + 1, nx, ny), u, v)
end subroutine bilinear_grad

function bilinear_interp(f00, f10, f01, f11, u, v) result(fout)
   implicit none
   real(8), intent(in) :: f00, f10, f01, f11, u, v
   real(8) :: fout
   !
   fout = f00 + (f10 - f00)*u + (f01 - f00)*v + (f11 - f10 - f01 + f00)*u*v

end function bilinear_interp

function bilinear_interp_array(w, i, j, u, v, nx, ny) result(fout)
   implicit none
   real(8), intent(in) :: w(0:nx - 1, 0:ny - 1), u, v
   integer, intent(in) :: i, j, nx, ny
   !
   real(8) :: fout, bilinear_interp

   fout = bilinear_interp(w(i, j), w(i + 1, j), w(i, j + 1), w(i + 1, j + 1), u, v)
end function bilinear_interp_array

subroutine depose(w, i, j, u, v, amount, nx, ny)
   implicit none
   real(8), intent(inout) :: w(0:nx - 1, 0:ny - 1)
   real(8), intent(in) :: u, v, amount
   integer, intent(in) :: i, j, nx, ny

   w(i, j) = w(i, j) + amount*(1 - u)*(1 - v)
   w(i + 1, j) = w(i + 1, j) + amount*u*(1 - v)
   w(i, j + 1) = w(i, j + 1) + amount*(1 - u)*v
   w(i + 1, j + 1) = w(i + 1, j + 1) + amount*u*v
end subroutine depose

subroutine depose_kernel(w, i, j, kernel, amount, nx, ny, ir)
   implicit none
   real(8), intent(inout) :: w(0:nx - 1, 0:ny - 1)
   real(8), intent(in) :: amount, kernel(-ir:ir, -ir:ir)
   integer, intent(in) :: i, j, nx, ny, ir
   !
   integer :: p, q, pmin, pmax, qmin, qmax
   pmin = max(0, i - ir)
   qmin = max(0, j - ir)
   pmax = min(i + ir, nx - 1)
   qmax = min(j + ir, ny - 1)

   do p = pmin, pmax
      do q = qmin, qmax
         w(p, q) = w(p, q) + amount*kernel(p - i, q - j)
      end do
   end do
end subroutine depose_kernel

subroutine depose_kernel_itp(w, i, j, u, v, ir, radius, amount, nx, ny)
   implicit none
   real(8), intent(inout) :: w(0:nx - 1, 0:ny - 1)
   real(8), intent(in) :: u, v, amount, radius
   integer, intent(in) :: i, j, nx, ny, ir
   !
   integer :: p, q, pmin, pmax, qmin, qmax
   real(8) :: kernel_sum, kernel(-ir:ir, -ir:ir), dist

   kernel_sum = 0d0
   kernel = 0d0
   do p = -ir, ir
      do q = -ir, ir
         dist = max(0d0, radius - sqrt((real(p) - u)**2 + (real(q) - v)**2))
         kernel_sum = kernel_sum + dist
         kernel(p, q) = dist
      end do
   end do
   kernel = kernel/kernel_sum

   pmin = max(0, i - ir)
   qmin = max(0, j - ir)
   pmax = min(i + ir, nx - 1)
   qmax = min(j + ir, ny - 1)

   do p = pmin, pmax
      do q = qmin, qmax
         w(p, q) = w(p, q) + amount*kernel(p - i, q - j)
      end do
   end do
end subroutine depose_kernel_itp

subroutine smooth_avg(w, nx, ny, ir)
   implicit none
   real(8), intent(inout) :: w(0:nx - 1, 0:ny - 1)
   integer :: nx, ny, ir
   !
   integer :: i, j, k
   real(8) :: wa(0:nx - 1, 0:ny - 1)

   wa = 0d0

   do k = -ir, ir
      wa = wa + cshift(w, k, dim=1)
      wa = wa + cshift(w, k, dim=2)
   end do

   ! central value is added 2-times
   w = (wa - w)/(4*ir + 1)

   !
   w = transpose(w)
   wa = 0d0
   do k = -ir, ir
      wa = wa + cshift(w, k, dim=1)
      wa = wa + cshift(w, k, dim=2)
   end do
   w = (wa - w)/(4*ir + 1)

   w = transpose(w)

end subroutine smooth_avg

subroutine update_buffer(zb, nx, ny)
   implicit none
   real(8), intent(inout) :: zb(-1:nx, -1:ny)
   integer :: nx, ny

   zb(-1, :) = zb(1, :)
   zb(nx, :) = zb(nx - 2, :)
   zb(:, -1) = zb(:, 1)
   zb(:, ny) = zb(:, ny - 2)

end subroutine update_buffer

function diff_i(w, i, j, nx, ny) result(dw)
   implicit none
   real(8), intent(in) :: w(0:nx - 1, 0:ny - 1)
   integer, intent(in) :: i, j, nx, ny
   real(8) :: dw

   if (i == 0) then
      dw = w(i + 1, j) - w(i, j)
   else if (i == nx - 1) then
      dw = w(i, j) - w(i - 1, j)
   else
      dw = (w(i + 1, j) - w(i - 1, j))/2d0
   end if
end function diff_i

function diff_j(w, i, j, nx, ny) result(dw)
   implicit none
   real(8), intent(in) :: w(0:nx - 1, 0:ny - 1)
   integer, intent(in) :: i, j, nx, ny
   real(8) :: dw

   if (j == 0) then
      dw = w(i, j + 1) - w(i, j)
   else if (j == ny - 1) then
      dw = w(i, j) - w(i, j - 1)
   else
      dw = (w(i, j + 1) - w(i, j - 1))/2d0
   end if
end function diff_j

function dot(u, v) result(c)
   implicit none
   real(8), intent(in) :: u(2), v(2)
   real(8) :: c
   c = u(1)*v(1) + u(2)*v(2)
end function dot

function lerp(a, b, t) result(c)
   implicit none
   real(8), intent(in) :: a, b, t
   real(8) :: c, t2
   t2 = t*t*t*(t*(t*6d0 - 15d0) + 10d0)
   c = (1 - t2)*a + t2*b
end function lerp

