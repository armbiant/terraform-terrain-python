subroutine viscous_flow(z, h, gnu, source_amp, dt, tol, iterations, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, iterations
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: h
   real(8), intent(inout) :: dt
   real(8), intent(in) :: gnu, source_amp, tol
   !f2py intent(in, out) :: h
   !
   integer :: it, i, j
   real(8) :: rhs(0:nx - 1, 0:ny - 1), df(0:nx - 1, 0:ny - 1)
   real(8) :: h0(0:nx - 1, 0:ny - 1), tau, dx, dy

   h0 = h

   dx = 1d0/real(nx)
   dy = 1d0/real(ny)

   dx = 1d0/(2d0*dx)
   dy = 1d0/(2d0*dy)

   do it = 1, iterations

      rhs = 0d0

      ! / x

      df = 0d0
      do i = 1, nx - 2
         df(i, :) = (h(i + 1, :) + z(i + 1, :) - h(i - 1, :) - z(i - 1, :))
      end do
      rhs = h**3/3d0*df*dx

      do i = 1, nx - 2
         df(i, :) = (rhs(i + 1, :) - rhs(i - 1, :))
      end do
      rhs = gnu*df*dx

      h = h + dt*rhs

      ! / y

      df = 0d0
      do j = 1, ny - 2
         df(:, j) = (h(:, j + 1) + z(:, j + 1) - h(:, j - 1) - z(:, j - 1))
      end do
      rhs = h**3/3d0*df*dy

      do j = 1, ny - 2
         df(:, j) = (rhs(:, j + 1) - rhs(:, j - 1))
      end do
      rhs = gnu*df*dy

      h = h + dt*rhs

      ! update time step
      tau = dt/2d0*maxval(abs(rhs))
      dt = 0.9d0*dt*min(max((tol/2d0/tau)**0.5, 0.3d0), 2d0)

      ! source term
      h = h + dt*source_amp*h0

      ! remove spurious background noise
      where (h < 1d-6)
         h = 0d0
      elsewhere
         h = h
      end where

      ! update boundary conditions
      h(0, :) = h(1, :)
      h(nx - 1, :) = h(nx - 2, :)

      h(:, 0) = h(:, 1)
      h(:, ny - 1) = h(:, ny - 2)

   end do

   ! negative values clipping
   h = (abs(h) + h)/2d0

end subroutine viscous_flow

