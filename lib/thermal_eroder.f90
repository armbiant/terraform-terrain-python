subroutine compute_talus(z, talus, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(out), dimension(0:nx - 1, 0:ny - 1) :: talus
   !f2py intent(in) :: z
   !f2py intent(out) :: talus
   !
   integer :: i, j, p, q, pmin, pmax, qmin, qmax
   real(8) :: delta, diag_coeff

   talus(i, j) = 0d0

   do i = 0, nx - 1
      pmin = max(0, i - 1)
      pmax = min(i + 1, nx - 1)

      do j = 0, ny - 1
         qmin = max(0, j - 1)
         qmax = min(j + 1, ny - 1)

         do p = pmin, pmax
            do q = qmin, qmax

               if ((p /= i) .and. (q /= j)) then
                  diag_coeff = 1.414d0
               else
                  diag_coeff = 1d0
               end if

               delta = abs(z(p, q) - z(i, j))/diag_coeff

               if (delta > talus(i, j)) talus(i, j) = delta

            end do
         end do

      end do
   end do

end subroutine compute_talus

subroutine thermal_eroder_cycle(z, talus, z_bedrock, ct, iterations,&
     & cv_tol, nx, ny)
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, iterations
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z, talus, z_bedrock
   real(8), intent(in) :: ct, cv_tol
   !
   !f2py intent(in) :: talus, z_bedrock
   !f2py intent(in, out) :: z
   !f2py intent(in) :: ct, iterations, cv_tol
   !
   integer :: it, i, j, ia, ja, k, p, q
   real(8) :: delta, sum_diff, zptp, cv_norm, amount
   real(8) :: dmax
   integer :: plist(1:8), qlist(1:8)
   real(8) :: clist(1:8), c

   plist = (/0, 1, 1, 1, 0, -1, -1, -1/)
   qlist = (/1, 1, 0, -1, -1, -1, 0, 1/)
   clist = (/1d0, 1.414d0, 1d0, 1.414d0, 1d0, 1.414d0, 1d0, 1.414d0/)

   ! used to un-normalize talus value
   zptp = maxval(z) - minval(z)
   cv_norm = cv_tol*zptp

   it = 0
   sum_diff = 1d9

   do while ((it < iterations) .and. (sum_diff > cv_norm))
      sum_diff = 0.

      do i = 0, nx - 1
         do j = 0, ny - 1

            if (z(i, j) > z_bedrock(i, j)) then

               ia = -1
               ja = -1
               dmax = 0

               do k = 1, 8
                  p = plist(k)
                  q = qlist(k)
                  c = clist(k)

                  if ((i + p > -1) .and. (i + p < nx) &
                      .and. (j + q > -1) .and. (j + q < ny)) then
                     delta = z(i, j) - z(i + p, j + q) - talus(i, j)*c

                     if (delta > dmax) then
                        dmax = delta
                        ia = i + p
                        ja = j + q
                     end if
                  end if

               end do

               if (ia > 0) then
                  amount = min(ct*dmax, z(i, j) - z_bedrock(i, j))
                  z(i, j) = z(i, j) - amount
                  z(ia, ja) = z(ia, ja) + amount

                  sum_diff = sum_diff + amount
               end if

            end if

         end do
      end do

      it = it + 1

   end do

end subroutine thermal_eroder_cycle
