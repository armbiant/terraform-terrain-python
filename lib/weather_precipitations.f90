subroutine precipation_map_particles(z, soil_moisture_map, wind_angle,&
     & iterations, n_particles, p_inertia, k_soil_evap, kernel, nk,&
     & seed, nx, ny, pmap)
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, nk, iterations, n_particles, seed
   real(8), intent(in) :: p_inertia, k_soil_evap
   real(8), intent(out), dimension(0:nx - 1, 0:ny - 1) :: pmap
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z, soil_moisture_map, wind_angle
   real(8), intent(in), dimension(0:nk - 1, 0:nk - 1) :: kernel
   !f2py intent(out) :: pmap
   !
   integer :: ip, i, j, it
   real(8) :: rd, x, y, u, v, direction(0:1), direction_wind(0:1), grad(0:1)
   real(8) :: local_angle, water, water_capacity
   real(8) :: amount
   !
   real(8) :: bilinear_interp_array

   if (seed > 0) call set_rd_seed(seed)

   pmap = 0d0

   ! particle simulations
   do ip = 1, n_particles

      ! initial position
      call random_number(rd)
      x = rd*real(nx - 1)
      call random_number(rd)
      y = rd*real(ny - 1)

      i = min(floor(x), nx - 1)
      j = min(floor(y), ny - 1)

      ! particle parameters
      water = 0d0
      direction(0) = cos(wind_angle(i, j))
      direction(1) = sin(wind_angle(i, j))

      do it = 1, iterations

         u = x - real(i)
         v = y - real(j)

         call bilinear_grad(z, i, j, u, v, nx, ny, grad)

         ! new direction and position
         direction = p_inertia*direction - grad*(1 - p_inertia)
         direction = direction/sum(direction**2)**0.5

         ! still (eventually) keep the particle in aligned with the wind
         local_angle = bilinear_interp_array(wind_angle, i, j, u, v, nx, ny)
         direction_wind(0) = cos(local_angle)
         direction_wind(1) = sin(local_angle)
         direction = p_inertia*direction + direction_wind*(1 - p_inertia)
         direction = direction/sum(direction**2)**0.5

         x = x + direction(0)
         y = y + direction(1)

         if (x < 0) x = nx - 1 + x
         if (y < 0) y = ny - 1 + y
         if (x > nx - 1) x = nx - 1 - x
         if (y > ny - 1) y = ny - 1 - y

         i = floor(x)
         j = floor(y)

         water_capacity = min(1d0, max(0d0, 1d0 - z(i, j)**0.5))

         if (water < water_capacity) then
            if (soil_moisture_map(i, j) > 0) then
               amount = min(soil_moisture_map(i, j), k_soil_evap*(water_capacity - water)*(1 - water**0.5))
               water = water + amount
               call depose_kernel(soil_moisture_map, i, j, kernel, -amount, nx, ny, (nk - 1)/2)
            end if
         end if

         ! rain
         if (water >= water_capacity) then
            amount = water
            water = 0d0
            call depose_kernel(soil_moisture_map, i, j, kernel, amount, nx, ny, (nk - 1)/2)
         end if

      end do

   end do

   pmap = soil_moisture_map/maxval(soil_moisture_map)

end subroutine precipation_map_particles
