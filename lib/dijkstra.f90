subroutine dijkstra(w, ist, jst, ie, je, path_idx, distance_exponent, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, ist, jst, ie, je
   real(8), intent(in) :: distance_exponent
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: w
   integer, intent(out) :: path_idx(nx*ny, 0:1)
   !
   real(8) :: distance(0:nx - 1, 0:ny - 1), dist
   integer :: next_idx(0:nx - 1, 0:ny - 1, 0:1), ij(0:1)
   integer :: i, j, pmin, pmax, qmin, qmax, p, q, nit
   logical :: mask(0:nx - 1, 0:ny - 1)

   distance(:, :) = 0d0
   next_idx(:, :, :) = 0
   mask(:, :) = .false.

   ! initialize
   mask(ist, jst) = .true.
   nit = 0

   do while (count(mask) > 0 .and. nit < 1e7)

      ij = minloc(distance, mask=mask)
      i = ij(0) - 1
      j = ij(1) - 1
      mask(i, j) = .false.

      ! loop over neighbors
      pmin = max(0, i - 1)
      qmin = max(0, j - 1)
      pmax = min(i + 1, nx - 1)
      qmax = min(j + 1, ny - 1)

      do p = pmin, pmax
         do q = qmin, qmax
            if ((p /= i) .or. (q /= j)) then
               dist = distance(i, j) + abs(w(i, j) - w(p, q))**distance_exponent

               if (distance(p, q) > 0) then
                  ! if (dist < distance(i, j)) then
                  !    write(6,*) 'ERROR'
                  !    exit
                  ! end if
               else if ((mask(p, q) .eqv. .false.) .or. (dist < distance(p, q))) then
                  distance(p, q) = dist
                  mask(p, q) = .true.
                  next_idx(p, q, :) = (/i, j/)
               end if
            end if
         end do
      end do

      nit = nit + 1
   end do

   ! "reverse" build path
   path_idx = -1

   i = 1
   ij = (/ie, je/)
   do while (.true.)
      path_idx(i, :) = ij
      if ((ij(0) == ist) .and. (ij(1) == jst)) exit
      ! next point
      ij = next_idx(ij(0), ij(1), :)
      i = i + 1
   end do

end subroutine dijkstra

