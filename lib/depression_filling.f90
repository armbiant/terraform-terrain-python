subroutine depression_filling(z, w, iterations, eps, nx, ny)
   implicit none
   real(8), intent(in) :: eps
   integer, intent(in) :: nx, ny, iterations
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(out), dimension(0:nx - 1, 0:ny - 1) :: w
   !f2py intent(out) :: w
   !
   integer :: nit, i, j, k, ip(8), jq(8)

   ! initializaton
   w = z
   w(1:nx - 2, 1:ny - 2) = 1e5

   ip = (/1, 1, 1, 0, 0, -1, -1, -1/)
   jq = (/1, 0, -1, 1, -1, 1, 0, -1/)

   do nit = 1, iterations

      do i = 1, nx - 2
         do j = 1, ny - 2

            if (w(i, j) > z(i, j)) then
               do k = 1, 8
                  if (z(i, j) >= w(i + ip(k), j + jq(k)) + eps) then
                     w(i, j) = z(i, j)
                     exit
                  end if
                  if (w(i, j) > w(i + ip(k), j + jq(k)) + eps) then
                     w(i, j) = w(i + ip(k), j + jq(k)) + eps
                  end if
               end do
            end if

         end do
      end do

   end do

end subroutine depression_filling
