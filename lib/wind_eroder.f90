subroutine wind_eroder_particles(z, smap, windx, windy, iterations,&
     & n_particles, dt, p_initial_mass, p_gravity,&
     & p_velocity_relaxation, p_suspension, p_abrasion, radius, seed &
     &, kernel, nk, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, nk, iterations, n_particles, seed
   real(8), intent(in) :: dt, p_initial_mass, p_gravity,&
        & p_velocity_relaxation, p_suspension, p_abrasion, radius
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: smap
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: windx, windy
   real(8), intent(in), dimension(0:nk - 1, 0:nk - 1) :: kernel
   !
   !f2py intent(in) :: z
   !f2py intent(in,out) :: smap
   !
   integer :: ip, it, i, j
   real(8) :: eps, rd, x, y, u, v, vel(0:2), mass, h
   real(8) :: grad(0:1), z_loc, smap_loc, dn, normal(0:2)
   real(8) :: sa(0:2), sb(0:2), wx_loc, wy_loc, amount, force
   !
   real(8) :: bilinear_interp_array

   eps = 1d-10

   if (seed > 0) call set_rd_seed(seed)

   do ip = 1, n_particles

      ! initial position
      call random_number(rd)
      x = rd*real(nx - 1)
      call random_number(rd)
      y = rd*real(ny - 1)

      ! initial conditions
      call get_ijuv(x, y, i, j, u, v)
      vel(0) = bilinear_interp_array(windx, i, j, u, v, nx, ny)
      vel(1) = bilinear_interp_array(windy, i, j, u, v, nx, ny)
      vel(2) = 0d0

      mass = p_initial_mass

      h = bilinear_interp_array(z + smap, i, j, u, v, nx, ny)

      do it = 1, iterations

         call get_ijuv(x, y, i, j, u, v)

         z_loc = bilinear_interp_array(z, i, j, u, v, nx, ny)
         smap_loc = bilinear_interp_array(smap, i, j, u, v, nx, ny)

         ! --- update kinetic
         if (h > z_loc + smap_loc) then
            ! flying
            vel(2) = vel(2) - dt*p_gravity
         else
            ! sliding / contact movement
            call bilinear_grad(z + smap, i, j, u, v, nx, ny, grad)
            dn = (grad(0)**2 + grad(1)**2)**0.5

            ! do not update the velocity if the gradient is too
            ! "small"
            if (dn > eps) then
               normal = (/-grad(0)/dn, -grad(1)/dn, 1d0/)
               call cross(vel, normal, sa)
               call cross(sa, normal, sb)
               vel(:) = vel(:) + dt*sb(:)/(sum(sb(:)**2)**0.5)
            end if
         end if

         ! exit if the particle stops moving
         if (sum(vel(:)**2) < eps) exit

         ! relaxation to wind speed
         wx_loc = bilinear_interp_array(windx, i, j, u, v, nx, ny)
         wy_loc = bilinear_interp_array(windy, i, j, u, v, nx, ny)

         vel(0) = vel(0) + p_velocity_relaxation*dt*(wx_loc - vel(0))
         vel(1) = vel(1) + p_velocity_relaxation*dt*(wy_loc - vel(1))
         vel(2) = vel(2)*(1d0 - p_velocity_relaxation*dt)

         ! new position
         x = x + vel(0)*dt
         y = y + vel(1)*dt
         h = h + vel(2)*dt

         ! interrupt if the particle exit the domain
         if (x < 0) exit
         if (y < 0) exit
         if (x > nx - 1) exit
         if (y > ny - 1) exit

         ! update position parameters
         call get_ijuv(x, y, i, j, u, v)

         ! --- sediment dynamic

         if (h <= z_loc + smap_loc) then ! on the ground
            force = sum(vel(:)**2)**0.5*(z_loc + smap_loc - h)

            amount = min(smap_loc, dt*p_abrasion*force)
            mass = mass + amount
            if (radius < 1d0) then
               call depose(smap, i, j, u, v, -amount, nx, ny)
            else
               call depose_kernel(smap, i, j, kernel, -amount, nx, ny, (nk - 1)/2)
            end if

         else ! flying
            amount = min(mass, dt*p_suspension*mass)

            mass = mass - amount
            if (radius < 1d0) then
               call depose(smap, i, j, u, v, amount, nx, ny)
            else
               call depose_kernel(smap, i, j, kernel, amount, nx, ny, (nk - 1)/2)
            end if

         end if

         ! "ground collider": eventually correct particle height
         if (h < z_loc + smap_loc) h = z_loc + smap_loc

      end do

      ! make sure sediment height remains positive (a bit of
      ! derivation is possible due to interpolations and kernel-based
      ! deposition)
      smap = (smap + abs(smap))/2d0

   end do

end subroutine wind_eroder_particles
