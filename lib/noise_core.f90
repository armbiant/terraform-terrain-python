! noise generators

subroutine diamond_square2d(nx, persistence, seed, w)
   implicit none
   integer, intent(in) :: nx, seed
   real(8), intent(in) :: persistence
   real(8), intent(out) :: w(0:nx, 0:nx) ! must be 2^n + 1
   !
   real(8) :: rd, amp, mean, nn
   integer :: i, j, st, shift

   if (seed > 0) call set_rd_seed(seed)

   w = 0.

   ! initialize corners
   call random_number(rd)
   w(0, 0) = 2*rd - 1
   call random_number(rd)
   w(0, nx) = 2*rd - 1
   call random_number(rd)
   w(nx, 0) = 2*rd - 1
   call random_number(rd)
   w(nx, nx) = 2*rd - 1

   st = nx
   amp = 1/persistence

   do while (st > 1)

      st = st/2
      amp = amp*persistence

      ! diamond
      do i = st, nx - st, 2*st
         do j = st, nx - st, 2*st
            mean = w(i - st, j - st) &
                   + w(i + st, j - st) &
                   + w(i - st, j + st) &
                   + w(i + st, j + st)
            call random_number(rd)
            w(i, j) = 0.25*mean + amp*(2*rd - 1)
         end do
      end do

      ! square
      shift = 0
      do i = 0, nx, st

         ! staggered grid
         if (shift == 0) then
            shift = st
         else
            shift = 0
         end if

         do j = shift, nx, 2*st
            mean = 0d0
            nn = 0

            if (i >= st) then
               mean = mean + w(i - st, j)
               nn = nn + 1
            end if
            if (i + st < nx) then
               mean = mean + w(i + st, j)
               nn = nn + 1
            end if
            if (j >= st) then
               mean = mean + w(i, j - st)
               nn = nn + 1
            end if
            if (j + st < nx) then
               mean = mean + w(i, j + st)
               nn = nn + 1
            end if
            call random_number(rd)
            w(i, j) = mean/real(nn) + amp*(2*rd - 1)
         end do
      end do

   end do

end subroutine diamond_square2d

subroutine fault2d_xy(x, y, w, seed, displacement, maxit, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, seed, maxit
   real(8), intent(in) :: displacement
   real(8), intent(in) :: x(0:nx - 1), y(0:ny - 1)
   real(8), intent(out) :: w(0:nx - 1, 0:ny - 1)
   !
   integer :: it, i, j
   real(8) :: x1, x2, y1, y2

   if (seed > 0) call set_rd_seed(seed)

   do it = 1, maxit
      ! parameters of cut line used to divide the domain in 2 regions
      call random_number(x1)
      call random_number(x2)
      call random_number(y1)
      call random_number(y2)

      do i = 0, nx - 1
         do j = 1, ny - 1
            if ((x2 - x1)*(y(j) - y1) - (y2 - y1)*(x(i) - x1) > 0) then
               w(i, j) = w(i, j) + displacement
            else
               w(i, j) = w(i, j) - displacement
            end if
         end do
      end do
   end do

end subroutine fault2d_xy

subroutine perlin2d_xy(x, y, res, w, seed, mu, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, res(0:1), seed
   real(8), intent(in) :: x(0:nx - 1), y(0:ny - 1), mu
   real(8), intent(out) :: w(0:nx - 1, 0:ny - 1)
   !
   integer :: i, j, ig, jg
   real(8) :: xg(0:res(0)), yg(0:res(1)), dxg, dyg, rd0, rd1
   real(8) :: grad(0:res(0), 0:res(1), 0:1)
   real(8) :: n0, n1, ix0, ix1, rx, ry, pi
   !
   real(8) :: dot, lerp

   if (seed > 0) call set_rd_seed(seed)

   pi = acos(-1d0)

   ! gradient grid
   dxg = 1d0/float(res(0))
   dyg = 1d0/float(res(1))
   do ig = 0, res(0)
      xg(ig) = float(ig)/float(res(0))
   end do
   do jg = 0, res(1)
      yg(jg) = float(jg)/float(res(1))
   end do

   ! initialize gradients
   do ig = 0, res(0)
      do jg = 0, res(1)
         call random_number(rd0)
         call random_number(rd1)
         grad(ig, jg, 0) = cos(2d0*pi*rd0)
         grad(ig, jg, 1) = sin(2d0*pi*rd1)
      end do
   end do

   ! "exponentially distributed gradients" (if requested...)
   if (mu .ne. 0) then
      do ig = 0, res(0)
         do jg = 0, res(1)
            call random_number(rd0)
            call random_number(rd1)
            grad(ig, jg, 0) = grad(ig, jg, 0)*exp(-abs(grad(ig, jg, 0))*rd0/mu)
            grad(ig, jg, 1) = grad(ig, jg, 1)*exp(-abs(grad(ig, jg, 1))*rd1/mu)
         end do
      end do
   end if

   !
   do i = 0, nx - 1
      ig = min(floor(x(i)/dxg), res(0) - 1)
      rx = (x(i) - xg(ig))/dxg

      do j = 0, ny - 1
         jg = min(floor(y(j)/dyg), res(1) - 1)
         ry = (y(j) - yg(jg))/dyg

         n0 = dot(grad(ig, jg, 0:1), (/(x(i) - xg(ig))/dxg, (y(j) - yg(jg))/dyg/))
         n1 = dot(grad(ig + 1, jg, 0:1), (/(x(i) - xg(ig + 1))/dxg, (y(j) - yg(jg))/dyg/))
         ix0 = lerp(n0, n1, rx)

         n0 = dot(grad(ig, jg + 1, 0:1), (/(x(i) - xg(ig))/dxg, (y(j) - yg(jg + 1))/dyg/))
         n1 = dot(grad(ig + 1, jg + 1, 0:1), (/(x(i) - xg(ig + 1))/dxg, (y(j) - yg(jg + 1))/dyg/))
         ix1 = lerp(n0, n1, rx)

         w(i, j) = lerp(ix0, ix1, ry)
      end do
   end do
end subroutine perlin2d_xy

subroutine voronoi2d_xy(x, y, xnode, ynode, distance_exponent, cell_filling, w, nx, ny, nnodes)
   implicit none
   integer, intent(in) :: nx, ny, nnodes, cell_filling
   real(8), intent(in) :: x(0:nx - 1), y(0:ny - 1), distance_exponent
   real(8), intent(in) :: xnode(0:nnodes - 1), ynode(0:nnodes - 1)
   real(8), intent(out) :: w(0:nx - 1, 0:ny - 1)
   !
   integer :: i, j, p, pmin
   real(8) :: dist, dist_new

   do i = 0, nx - 1
      do j = 0, ny - 1
         ! brute force, we don't have any informations on the node
         ! positions

         if (cell_filling == 0) then

            dist = 1e5
            do p = 0, nnodes - 1
               dist = min(dist, ((x(i) - xnode(p))**2 + (y(j) - ynode(p))**2)**distance_exponent)
            end do
            w(i, j) = dist

         else

            dist = 1d5
            do p = 0, nnodes - 1
               dist_new = ((x(i) - xnode(p))**2 + (y(j) - ynode(p))**2)**distance_exponent
               if (dist_new < dist) then
                  dist = dist_new
                  pmin = p
               end if
            end do
            w(i, j) = pmin

         end if

      end do
   end do

end subroutine voronoi2d_xy

subroutine worley2d_xy(x, y, res, w, seed, distance_exponent, cell_filling, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, res(0:1), seed, cell_filling
   real(8), intent(in) :: x(0:nx - 1), y(0:ny - 1), distance_exponent
   real(8), intent(out) :: w(0:nx - 1, 0:ny - 1)
   !
   integer :: i, j, ig, jg, ic, jc, p, q
   real(8) :: xg(-1:res(0)), yg(-1:res(1)), dxg, dyg, rd0, rd1
   real(8) :: nodes(-1:res(0), -1:res(1), 0:1)
   real(8) :: dist, dist_new

   if (seed > 0) call set_rd_seed(seed)

   ! cell grid
   dxg = 1d0/float(res(0))
   dyg = 1d0/float(res(1))
   do ig = -1, res(0)
      xg(ig) = float(ig)/float(res(0))
   end do
   do jg = -1, res(1)
      yg(jg) = float(jg)/float(res(1))
   end do

   ! initialize node locations
   do ig = -1, res(0)
      do jg = -1, res(1)
         call random_number(rd0)
         call random_number(rd1)
         nodes(ig, jg, 0:1) = (/xg(ig) + rd0*dxg, yg(jg) + rd1*dyg/)
      end do
   end do

   ! compute shortest distance
   do i = 0, nx - 1
      ig = min(floor(x(i)/dxg), res(0) - 1)

      do j = 0, ny - 1
         jg = min(floor(y(j)/dyg), res(1) - 1)

         if (cell_filling == 0) then
            dist = 1d5
            do p = -1, 1
               do q = -1, 1
                  dist = min(dist, ((x(i) - nodes(ig + p, jg + q, 0))**2 + (y(j) - nodes(ig + p, jg + q, 1))**2)**distance_exponent)
               end do
            end do
            w(i, j) = dist

         else
            ! fill cell with a constant value
            dist = 1d5
            do p = -1, 1
               do q = -1, 1
                  dist_new = ((x(i) - nodes(ig + p, jg + q, 0))**2 + (y(j) - nodes(ig + p, jg + q, 1))**2)**distance_exponent
                  if (dist_new < dist) then
                     dist = dist_new
                     ic = ig + p
                     jc = jg + q
                  end if
               end do
            end do
            w(i, j) = (ic + 1) + (jc + 1)*(res(0) - 1)

         end if
      end do
   end do
end subroutine worley2d_xy

