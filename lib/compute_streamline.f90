subroutine compute_streamline(fx, fy, x0, y0, h, iterations, nx, ny, xout, yout)
   implicit none
   integer, intent(in) :: nx, ny, iterations
   real(8), intent(in) :: x0, y0, h
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: fx, fy
   real(8), intent(out), dimension(0:nx*ny) :: xout, yout
   !f2py intent(out) :: xout, yout
   !
   real(8) :: x, y, u, v
   real(8) :: xk, yk, k1_x, k1_y, k2_x, k2_y, k3_x, k3_y, k4_x, k4_y
   integer :: ip, i, j
   !
   real(8) :: bilinear_interp_array

   xout = -1d0
   yout = -1d0

   x = x0
   y = y0

   xout(0) = x0
   yout(0) = y0

   do ip = 1, iterations

      ! --- 4th-order Runge-Kutta (RK4)
      call get_ijuv(x, y, i, j, u, v)
      k1_x = bilinear_interp_array(fx, i, j, u, v, nx, ny)
      k1_y = bilinear_interp_array(fy, i, j, u, v, nx, ny)

      !
      xk = x + h/2d0*k1_x
      yk = y + h/2d0*k1_y
      if (xk < 0) exit
      if (yk < 0) exit
      if (xk > nx - 1) exit
      if (yk > ny - 1) exit
      call get_ijuv(xk, yk, i, j, u, v)
      k2_x = bilinear_interp_array(fx, i, j, u, v, nx, ny)
      k2_y = bilinear_interp_array(fy, i, j, u, v, nx, ny)

      !
      xk = x + h/2d0*k2_x
      yk = y + h/2d0*k2_y
      if (xk < 0) exit
      if (yk < 0) exit
      if (xk > nx - 1) exit
      if (yk > ny - 1) exit
      call get_ijuv(xk, yk, i, j, u, v)
      k3_x = bilinear_interp_array(fx, i, j, u, v, nx, ny)
      k3_y = bilinear_interp_array(fy, i, j, u, v, nx, ny)

      !
      xk = x + h*k3_x
      yk = y + h*k3_y
      if (xk < 0) exit
      if (yk < 0) exit
      if (xk > nx - 1) exit
      if (yk > ny - 1) exit
      call get_ijuv(xk, yk, i, j, u, v)
      k4_x = bilinear_interp_array(fx, i, j, u, v, nx, ny)
      k4_y = bilinear_interp_array(fy, i, j, u, v, nx, ny)

      !
      x = x + h/6d0*(k1_x + 2d0*k2_x + 2d0*k3_x + k4_x)
      y = y + h/6d0*(k1_y + 2d0*k2_y + 2d0*k3_y + k4_y)

      ! interrupt if the particle exit the domain
      if (x < 0) exit
      if (y < 0) exit
      if (x > nx - 1) exit
      if (y > ny - 1) exit

      xout(ip) = x
      yout(ip) = y

   end do

end subroutine compute_streamline

