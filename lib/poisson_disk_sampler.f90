subroutine poisson_disk_sampler(rmin, nsearch, seed, samples, nx, ny)
   implicit none
   integer, intent(in) :: nx, ny, seed, nsearch
   real(8), intent(in) :: rmin(0:nx - 1, 0:ny - 1)
   real(8), intent(out) :: samples(0:nx - 1, 0:ny - 1, 0:1)
   !
   integer :: i, j, ip, jp, k, ij(0:1), imax, ikernel
   integer :: p, q, pmin, pmax, qmin, qmax
   real(8) :: rd1, rd2, x, y, pi, dist, rmin2
   logical :: mask(0:nx - 1, 0:ny - 1)
   logical :: is_too_close
   logical, allocatable :: mask_kernel(:, :)

   if (seed > 0) call set_rd_seed(seed)

   pi = acos(-1d0)

   ! initialize arrays
   mask(:, :) = .true. ! is cell to be treated
   samples(:, :, :) = 0.

   ikernel = int(maxval(rmin))
   allocate (mask_kernel(-ikernel - 1:ikernel + 1, -ikernel - 1:ikernel + 1))

   ! fill sample array with random numbers to use it as a random cell
   ! "picker" using minval later on
   call random_number(samples(:, :, 0))
   samples(:, :, 0) = samples(:, :, 0) - 2d0

   do while (count(mask) > 0)

      ij = minloc(samples(:, :, 0), mask=mask)
      i = ij(0) - 1
      j = ij(1) - 1

      ! --- first check if the particle neighborhood is not too crowded

      ! define which neighboring cells need to be checked
      imax = int(rmin(i, j) + 1)
      pmin = max(0, i - imax)
      qmin = max(0, j - imax)
      pmax = min(i + imax, nx - 1)
      qmax = min(j + imax, ny - 1)

      ! rough screening
      is_too_close = .false.
      mask_kernel = .false.
      rmin2 = rmin(i, j)**2
      do p = -imax - 1, imax + 1
         do q = -imax - 1, imax + 1
            if (p**2 + q**2 <= int(rmin2)) then
               mask_kernel(p, q) = .true.
            end if
         end do
      end do

      if (count(.not. (mask_kernel(pmin - i:pmax - i, qmin - j:qmax - j) .and. mask(pmin:pmax, qmin:qmax))) <= 1) then
         ! rough screening
         is_too_close = .true.
      end if

      if (is_too_close .eqv. .true.) then
         ! neighborhood clutered, definitely leave this cell empty and
         ! move on
         mask(i, j) = .false.

      else
         ! --- try to squeez a new point if not too crowded
         ksearch: do k = 1, nsearch
            ! create temporary points
            call random_number(rd1)
            call random_number(rd2)
            x = i + 0.5 + rmin(i, j)*(rd1 + 1d0)*cos(2d0*pi*rd2)
            y = j + 0.5 + rmin(i, j)*(rd1 + 1d0)*sin(2d0*pi*rd2)

            if (x < 0) exit ksearch
            if (y < 0) exit ksearch
            if (x > nx - 1) exit ksearch
            if (y > ny - 1) exit ksearch

            ip = int(x)
            jp = int(y)

            ! define which neighboring cells need to be checked (for
            ! this cell)
            imax = int(rmin(ip, jp) + 1)
            pmin = max(0, ip - imax)
            qmin = max(0, jp - imax)
            pmax = min(ip + imax, nx - 1)
            qmax = min(jp + imax, ny - 1)

            ! rough screening
            is_too_close = .false.
            mask_kernel = .false.
            rmin2 = rmin(i, j)**2
            do p = -imax - 1, imax + 1
               do q = -imax - 1, imax + 1
                  if (p**2 + q**2 <= int(rmin2)) then
                     mask_kernel(p, q) = .true.
                  end if
               end do
            end do

            if (count(.not. (mask_kernel(pmin - i:pmax - i, qmin - j:qmax - j) .and. mask(pmin:pmax, qmin:qmax))) <= 1) then

            else
               ! loop over neighbors
               is_too_close = .false.
               outerk: do p = pmin, pmax
                  do q = qmin, qmax
                     if ((p /= ip) .or. (q /= jp)) then
                        ! if there is point in the cell, check the distance
                        if (samples(p, q, 0) >= 0) then
                           dist = ((x - samples(p, q, 0))**2 + (y - samples(p, q, 1))**2)**0.5
                           if (dist < rmin(ip, jp)) then
                              is_too_close = .true.
                              exit outerk
                           end if
                        end if
                     end if
                  end do
               end do outerk
            end if

            if (is_too_close .eqv. .false.) then
               samples(ip, jp, 0) = x
               samples(ip, jp, 1) = y

               ! remove current cell from the queue and also remove
               ! all cells around because there can be not point there
               ! (take a bit of safety distance for 'imax' to be sure
               ! not to miss points
               rmin2 = rmin(i, j)**2
               imax = int(rmin(i, j) - 2)

               if (imax > 0) then
                  pmin = max(0, i - imax)
                  qmin = max(0, j - imax)
                  pmax = min(i + imax, nx - 1)
                  qmax = min(j + imax, ny - 1)

                  do p = pmin, pmax
                     do q = qmin, qmax
                        if ((p - i)**2 + (q - j)**2 <= int(rmin2)) then
                           mask(p, q) = .false.
                        end if
                     end do
                  end do

               else
                  mask(i, j) = .false.

               end if

               exit ksearch
            end if

         end do ksearch

         ! if after the 'k' search, no neigbors has been, change dummy
         ! values of 'samples' to force the choice of a new point at
         ! the next iteration (to avoid non-convergence issues)
         if (mask(i, j) .eqv. .true.) then
            call random_number(rd1)
            call random_number(rd2)
            samples(i, j, 0) = rd1 - 2d0
            samples(i, j, 1) = rd2 - 2d0
         end if

      end if

   end do

   deallocate (mask_kernel)

end subroutine poisson_disk_sampler
