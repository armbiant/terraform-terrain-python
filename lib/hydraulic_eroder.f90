subroutine hydraulic_eroder_musgrave(z, w, s, iterations, Kd, Kc, Ks, tau, nx, ny)
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, iterations
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z, w, s
   real(8), intent(in) :: Kd, Kc, Ks, tau
   !
   !f2py intent(in, out) :: z, w, s
   !f2py intent(in) :: iterations, Kd, Kc, Ks, tau
   !
   integer :: it, i, j, p, q, pmin, pmax, qmin, qmax
   real(8) :: dw, cs
   real(8), dimension(0:nx - 1, 0:ny - 1) :: w_bckp

   w_bckp = w
   it = 0

   do while (it < iterations)

      w = (1 - tau)*w + tau*w_bckp
      s = tau*s

      do i = 0, nx - 1
         pmin = max(i - 1, 0)
         pmax = min(i + 1, nx - 1)

         do j = 0, ny - 1
            qmin = max(j - 1, 0)
            qmax = min(j + 1, ny - 1)

            do p = pmin, pmax
               do q = qmin, qmax

                  dw = min(w(i, j), w(i, j) + z(i, j) - w(p, q) - z(p, q))

                  if (dw <= 0) then
                     z(i, j) = z(i, j) + Kd*s(i, j)
                     s(i, j) = s(i, j) - Kd*s(i, j)
                  else
                     w(i, j) = w(i, j) - dw/2d0
                     w(p, q) = w(p, q) + dw/2d0

                     ! sediment capacity of water gap
                     cs = Kc*dw/2d0

                     if (s(i, j) >= cs) then
                        s(p, q) = s(p, q) + cs

                        z(i, j) = z(i, j) + Kd*(s(i, j) - cs)
                        s(i, j) = (1 - Kd)*(s(i, j) - cs)
                     else
                        s(p, q) = s(p, q) + s(i, j) + Ks*(cs - s(i, j))

                        z(i, j) = z(i, j) - Ks*(cs - s(i, j))
                        s(i, j) = 0d0
                     end if

                  end if

               end do
            end do

         end do
      end do

      it = it + 1

   end do

end subroutine hydraulic_eroder_musgrave

subroutine hydraulic_eroder_particles(z, z_bedrock, moisture_map,&
     & iterations, n_particles, dt, gravity, water_evaporation_rate,&
     & velocity_lim_deposition, velocity_lim_erosion, p_mass,&
     & p_capacity, p_deposition, p_erosion, p_inertia, p_drag, radius,&
     & seed, kernel, nk, nx, ny)
   implicit none
   ! inputs
   integer, intent(in) :: nx, ny, nk, iterations, n_particles, seed
   real(8), intent(in) :: dt, gravity, water_evaporation_rate
   real(8), intent(in) :: velocity_lim_deposition, velocity_lim_erosion
   real(8), intent(in) :: p_mass, p_capacity
   real(8), intent(in) :: p_deposition, p_erosion, p_inertia, radius
   real(8), intent(inout), dimension(0:nx - 1, 0:ny - 1) :: z
   real(8), intent(in), dimension(0:nx - 1, 0:ny - 1) :: moisture_map, z_bedrock, p_drag
   real(8), intent(in), dimension(0:nk - 1, 0:nk - 1) :: kernel
   !f2py intent(in, out) :: z
   !
   integer :: ip, i, j, ib, jb, it, p, q, pmin, pmax, qmin, qmax, ir
   real(8) :: rd, x, y, u, v, grad(0:1), gn, vel(0:1), vel_new(0:1)
   real(8) :: vel_norm, dir(0:1), dir_new(0:1), surface_normal(0:2)
   real(8) :: force(0:1), drag(0:1), sediment
   real(8) :: z_bd, water, capacity_limit, vcoeff
   real(8) :: amount
   !
   real(8) :: bilinear_interp_array

   if (seed > 0) call set_rd_seed(seed)

   ! particle simulations
   do ip = 1, n_particles

      ! initial position
      call random_number(rd)
      x = rd*real(nx - 1)
      call random_number(rd)
      y = rd*real(ny - 1)

      call get_ijuv(x, y, i, j, u, v)

      ! particle parameters
      call random_number(rd)
      vel(0) = 0.1*(2*rd - 1)
      call random_number(rd)
      vel(1) = 0.1*(2*rd - 1)

      sediment = 0d0
      water = moisture_map(i, j)

      do it = 1, iterations

         call bilinear_grad(z, i, j, u, v, nx, ny, grad)

         surface_normal = (/-grad(0)*nx, -grad(1)*ny, 1d0/)
         surface_normal = surface_normal/sum(surface_normal**2)**0.5

         ! forces
         dir = vel/sum(vel**2)**0.5
         force = p_mass*gravity*surface_normal(0:1)
         drag = -p_drag(i, j)*dir*sum(vel**2)

         ! update velocity
         vel_new = vel + dt/p_mass*(force + drag)

         ! velocity relaxation
         vel = (1d0 - dt*p_inertia)*vel_new + dt*p_inertia*vel

         ! exit if the particle stops
         if (sum(vel**2) < 1e-4) then
            ! amount = sediment ! min(sediment, p_deposition * velocity_lim_deposition)
            ! call depose(z, i, j, u, v, amount, nx, ny)
            exit
         end if

         x = x + dt*vel(0)
         y = y + dt*vel(1)

         call get_ijuv(x, y, i, j, u, v)

         ! interrupt if the particle exit the domain
         if (x < 0) exit
         if (y < 0) exit
         if (x > nx - 1) exit
         if (y > ny - 1) exit

         ! erosion / transport / deposition
         capacity_limit = p_capacity*water

         vel_norm = sum(vel**2)**0.5

         if (vel_norm > velocity_lim_erosion) then
            ! --- erosion

            ! coefficent
            if (vel_norm > 2d0*velocity_lim_erosion) then
               vcoeff = velocity_lim_erosion
            else
               vcoeff = vel_norm - velocity_lim_erosion
            end if

            if (sediment < capacity_limit) then
               amount = max(-p_erosion*vcoeff*(capacity_limit - sediment), &
                            -capacity_limit + sediment)
            else
               ! actually deposition if th sediment amount is above
               ! the capacity limit
               amount = (sediment - capacity_limit)/dt
            end if

         else if (vel_norm <= velocity_lim_deposition) then
            ! --- deposition
            vcoeff = velocity_lim_deposition - vel_norm
            amount = min(sediment, p_deposition*sediment*vcoeff)

         else
            ! --- transport
            if (sediment > capacity_limit) then
               amount = (sediment - capacity_limit)/dt
            end if

         end if

         amount = amount*dt

         ! apply erosion/ deposition operator
         if (amount /= 0d0) then
            if (radius < 1d0) then
               call depose(z, i, j, u, v, amount, nx, ny)
            else
               call depose_kernel(z, i, j, kernel, amount, nx, ny, (nk - 1)/2)
            end if
            sediment = sediment - amount
         end if

         ! water evaporation
         water = water*(1d0 - dt*water_evaporation_rate)
         if (water < 1d-3) exit

         ! make sure bedrock elevation is not eroded
         if (nk > 0) then
            ir = (nk - 1)/2
            pmin = max(0, i - ir)
            qmin = max(0, j - ir)
            pmax = min(i + ir, nx - 1)
            qmax = min(j + ir, ny - 1)

            do p = pmin, pmax
               do q = qmin, qmax
                  z(p, q) = max(z(p, q), z_bedrock(p, q))
               end do
            end do

         else
            z(i, j) = max(z(i, j), z_bedrock(i, j))

         end if

      end do

   end do

end subroutine hydraulic_eroder_particles
