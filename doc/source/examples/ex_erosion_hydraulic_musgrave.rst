Hydraulic erosion (grid-based, Musgrave)
========================================

- Musgrave, F.K., Kolb, C.E., and Mace, R.S. 1989. The synthesis and
  rendering of eroded fractal terrains. ACM SIGGRAPH Computer Graphics
  23, 41–50 `(link) <https://dl.acm.org/doi/10.1145/74334.74337>`_.

.. plot:: examples/src_python/ex_erosion_hydraulic_musgrave.py
   :include-source:
