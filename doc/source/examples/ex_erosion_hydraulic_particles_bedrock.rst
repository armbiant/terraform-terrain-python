Hydraulic erosion with bedrock
==============================

.. plot:: examples/src_python/ex_erosion_hydraulic_particles_bedrock.py
   :include-source:
