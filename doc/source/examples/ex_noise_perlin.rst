Perlin's noise
==============

- Perlin, K. 1985. An image synthesizer. ACM SIGGRAPH Computer
  Graphics 19, 287–296 `(link1)
  <https://dl.acm.org/doi/10.1145/325165.325247>`_.

- Parberry, I. 2015. Modeling Real-World Terrain with Exponentially
  Distributed Noise. Journal of Computer Graphics Techniques (JCGT) 4,
  1–9 `(link2) <https://jcgt.org/published/0004/02/01/>`_.

.. plot:: examples/src_python/ex_noise_perlin.py
   :include-source:
