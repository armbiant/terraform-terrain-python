Gabor noise
===========

- Lagae, A., Lefebvre, S., Drettakis, G., and
  Dutré, P. 2009. Procedural noise using sparse Gabor convolution. ACM
  SIGGRAPH 2009 papers on - SIGGRAPH 09, ACM Press `(link)
  <https://dl.acm.org/doi/10.1145/1576246.1531360>`_.

.. plot:: examples/src_python/ex_noise_gabor.py
   :include-source:
