Hydraulic erosion (simple explicit model for riverbed)
======================================================

.. plot:: examples/src_python/ex_erosion_hydraulic_flow_stream.py
   :include-source:
