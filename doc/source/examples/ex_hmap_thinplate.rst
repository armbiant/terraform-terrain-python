Thin-plate interpolation
========================

Thin-splate interpolation for heightmap generation based on sparse
elevation distributions.

.. plot:: examples/src_python/ex_hmap_thinplate.py
   :include-source:
