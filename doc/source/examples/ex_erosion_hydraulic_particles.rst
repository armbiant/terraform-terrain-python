Hydraulic erosion (particle-based)
==================================

.. plot:: examples/src_python/ex_erosion_hydraulic_particles.py
   :include-source:
