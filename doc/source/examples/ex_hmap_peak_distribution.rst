Point-wise alteration
=====================

Locally enforce a new elevation value while maintaining the "shape"
(profil) of the heightmap.

.. plot:: examples/src_python/ex_hmap_peak_distribution.py
   :include-source:
