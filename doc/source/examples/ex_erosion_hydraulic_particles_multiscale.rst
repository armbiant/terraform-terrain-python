Multiscale hydraulic erosion
============================

.. plot:: examples/src_python/ex_erosion_hydraulic_particles_multiscale.py
   :include-source:
