2D-plot colorization
====================

.. plot:: examples/src_python/ex_plot_colorize_std.py
   :include-source:

.. plot:: examples/src_python/ex_plot_colorize_bivariate.py
   :include-source:

.. plot:: examples/src_python/ex_plot_colorize_itp.py
   :include-source:

.. plot:: examples/src_python/ex_plot_colorize_colormap.py
   :include-source:
