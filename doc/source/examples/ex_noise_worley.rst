Worley's (cellular) noise
=========================

- Worley, S. 1996. A cellular texture basis function. Proceedings of
  the 23rd annual conference on Computer graphics and interactive
  techniques - SIGGRAPH 96, ACM Press `(link)
  <https://dl.acm.org/doi/10.1145/237170.237267>`_.

.. plot:: examples/src_python/ex_noise_worley.py
   :include-source:
