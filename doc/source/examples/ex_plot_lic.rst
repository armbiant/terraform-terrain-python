(Bumped) Line Integral Convolution (LIC) plot
=============================================

- Sanna, A. and Montrucchio, B. 2000. Adding a scalar value to 2D
  vector field visualization: the BLIC (Bumped LIC). Eurographics
  2000 - Short Presentations, Eurographics Association `(link)
  <http://dx.doi.org/10.2312/egs.20001000>`_.

.. plot:: examples/src_python/ex_plot_lic.py
   :include-source:
