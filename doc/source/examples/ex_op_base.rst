Elementary operations
=====================

``op.arr_array``
----------------

.. plot:: examples/src_python/ex_op_add_array.py
   :include-source:

``op.clamp``
------------

.. plot:: examples/src_python/ex_op_clamp.py
   :include-source:

``op.detect_border``
--------------------

.. plot:: examples/src_python/ex_op_detect_border.py
   :include-source:

``op.detect_ridge_valley``
--------------------------

.. plot:: examples/src_python/ex_op_detect_ridge_valley.py
   :include-source:      
      
``op.dijkstra``
---------------

.. plot:: examples/src_python/ex_op_dijkstra.py
   :include-source:

``op.enforce_boundaries``
-------------------------

.. plot:: examples/src_python/ex_op_enforce_boundaries.py
   :include-source:

``op.equalize``
--------------------

.. plot:: examples/src_python/ex_op_equalize.py
   :include-source:

``op.extract_subarray``
-----------------------

.. plot:: examples/src_python/ex_op_extract_subarray.py
   :include-source:

``op.fractalize_mask_contours``
-------------------------------

.. plot:: examples/src_python/ex_op_fractalize_mask_contours.py
   :include-source:
      
``op.gamma_correction``
-----------------------

.. plot:: examples/src_python/ex_op_gamma_correction.py
   :include-source:

``op.gradient``
---------------

.. plot:: examples/src_python/ex_op_gradient.py
   :include-source:

``op.harden``
--------------

.. plot:: examples/src_python/ex_op_harden.py
   :include-source:

``op.lerp``
-----------

.. plot:: examples/src_python/ex_op_lerp.py
   :include-source:

``op.local_maxima``
-------------------

.. plot:: examples/src_python/ex_op_local_maxima.py
   :include-source:

``op.plateau``
---------------

.. plot:: examples/src_python/ex_op_plateau.py
   :include-source:

``op.redistribute_values``
--------------------------

.. plot:: examples/src_python/ex_op_redistribute_values.py
   :include-source:

``op.remap``
------------

.. plot:: examples/src_python/ex_op_remap.py
   :include-source:

``op.remap_local``
------------------

.. plot:: examples/src_python/ex_op_remap_local.py
   :include-source:

``op.rugosity``
---------------

.. plot:: examples/src_python/ex_op_rugosity.py
   :include-source:

``op.sharpen``
--------------

.. plot:: examples/src_python/ex_op_sharpen.py
   :include-source:

``op.smooth_cos``
-----------------

.. plot:: examples/src_python/ex_op_smooth_cos.py
   :include-source:

``op.smooth_gaussian``
----------------------

.. plot:: examples/src_python/ex_op_smooth_gaussian.py
   :include-source:

``op.smooth_laplace``
----------------------

.. plot:: examples/src_python/ex_op_smooth_laplace.py
   :include-source:

``op.smooth_sharpen``
---------------------

.. plot:: examples/src_python/ex_op_smooth_sharpen.py
   :include-source:

``op.stamp_kernel``
-------------------

.. plot:: examples/src_python/ex_op_stamp_kernel.py
   :include-source:

``op.spectral_filter``
----------------------

.. plot:: examples/src_python/ex_op_spectral_filter.py
   :include-source:

``op.steepen``
--------------

.. plot:: examples/src_python/ex_op_steepen.py
   :include-source:

``op.steps``
------------

.. plot:: examples/src_python/ex_op_steps.py
   :include-source:

``op.subsample``
----------------

.. plot:: examples/src_python/ex_op_subsample.py
   :include-source:

``op.truncate``
---------------

.. plot:: examples/src_python/ex_op_truncate.py
   :include-source:

``op.warp2d``
-------------

.. plot:: examples/src_python/ex_op_warp2d.py
   :include-source:
