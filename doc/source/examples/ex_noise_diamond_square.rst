Diamond-square noise
====================

- Fournier, A., Fussell, D., and Carpenter, L. 1982. Computer
  rendering of stochastic models. Communications of the ACM 25,
  371–384, `(link) <https://dl.acm.org/doi/10.1145/358523.358553>`_.

.. plot:: examples/src_python/ex_noise_diamond_square.py
   :include-source:
