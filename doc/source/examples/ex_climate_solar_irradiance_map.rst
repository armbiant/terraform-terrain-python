Solar irradiance map
====================

.. plot:: examples/src_python/ex_climate_solar_irradiance_map.py
   :include-source:
