Contour
=======

Fractalize contour
------------------

.. plot:: examples/src_python/ex_contour.py
   :include-source:

Convert a mask to contours
--------------------------

.. plot:: examples/src_python/ex_contour_map_to_xy.py
   :include-source:
