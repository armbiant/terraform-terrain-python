Texture noise based on probability functions
============================================

.. plot:: examples/src_python/ex_noise_pdf.py
   :include-source:
