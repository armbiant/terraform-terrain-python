Combined hydraulic and thermal erosion
======================================

.. plot:: examples/src_python/ex_erosion_hydraulic_thermal.py
   :include-source:
