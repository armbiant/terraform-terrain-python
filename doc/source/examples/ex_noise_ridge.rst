Ridge noise
===========

- Ebert, D.S., Musgrave, F.K., Peachey, D., Perlin, K., and
  Worley, S. 2002. Texturing and Modeling: A Procedural
  Approach. Morgan Kaufmann Publishers Inc., San Francisco, CA, USA.
  
.. plot:: examples/src_python/ex_noise_ridge.py
   :include-source:
