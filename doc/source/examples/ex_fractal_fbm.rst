Factral brownian motion (fBm) noise
===================================

- Mandelbrot, B.B. and Ness, J.W.V. 1968. Fractional Brownian Motions,
  Fractional Noises and Applications. SIAM Review 10, 422–437.

.. plot:: examples/src_python/ex_fractal_fbm.py
   :include-source:
