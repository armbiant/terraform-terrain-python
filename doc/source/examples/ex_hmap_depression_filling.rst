Depression filling
==================

Depressions (or pits) are areas of a landscape wherein flow ultimately
terminates without reaching the edges of the map. The depression
filling algorithm ensures that no such pit exists afterwards.

- Planchon, O. and Darboux, F. 2002. A fast, simple and versatile
  algorithm to fill the depressions of digital elevation
  models. CATENA 46, 159–176 `(link)
  <https://hal.archives-ouvertes.fr/hal-00956072>`_.
  
.. plot:: examples/src_python/ex_hmap_depression_filling.py
   :include-source:
