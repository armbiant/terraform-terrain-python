Spot noise
==========

- van Wijk, J.J. 1991. Spot noise texture synthesis for data
  visualization. Proceedings of the 18th annual conference on Computer
  graphics and interactive techniques - SIGGRAPH 91, ACM Press `(link)
  <https://dl.acm.org/doi/10.1145/122718.122751>`_.

.. plot:: examples/src_python/ex_noise_spot.py
   :include-source:
