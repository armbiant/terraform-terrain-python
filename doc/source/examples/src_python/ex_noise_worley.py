import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 64)
res = (4, 2)
seed = 1  # set to -1 for "no seed"

w = vt.noise.worley2d(shape, res, seed=seed, distance_exponent=0.5)
vt.plot.show(w)

w = vt.noise.worley2d(shape, res, seed=seed, cell_filling=True)
vt.plot.show(w)

plt.show()
