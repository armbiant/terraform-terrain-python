import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

extent = [0.5, 1.5, 0, 0.5]

# density
d = vt.noise.perlin2d(shape, res, seed)

x, y = vt.grid.random_grid_density(n=10000,
                                   density=d,
                                   extent=extent,
                                   seed=seed)

# plots
vt.plot.show(d, extent=extent)
plt.plot(x, y, 'k.', ms=2)

plt.show()
