import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# prepare heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.erosion.hydraulic_particles(w, radius=1e-3)

talus = 0.1 * vt.op.gradient_talus(w).max()

# use altitude to define a deposition mask with more deposition a low
# altitudes
mask = 1 - vt.op.remap(w)

ws, smap = vt.erosion.sediment_deposition(w,
                                          max_deposition=0.01,
                                          talus=talus,
                                          deposition_map=mask)

# plots
vt.plot.topo(w, title='before')
vt.plot.show(smap, title='sediment thickness', cmap='nipy_spectral')
vt.plot.topo(ws, title='after')

# --- 3D rendering

mesh = vt.mesh.hmap_to_trimesh(0.2 * ws)

vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [237, 201, 175, 255]],
                  values=vt.op.remap(smap)**0.5)

data = vt.render.show([mesh], offscreen=True)

plt.show()
