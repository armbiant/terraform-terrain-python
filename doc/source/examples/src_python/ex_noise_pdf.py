import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

# genrate pdf, resolution does not need to be particularly high
n = 16
x, y = np.meshgrid(np.linspace(0, 1, n), np.linspace(0, 1, n), indexing='ij')

# pdf where gradients are steeper when the amplitude is high
pdf = np.exp(-(y - x**2)**2 / 2 / 0.1**2)

# another pdf example, similar to Perlin's noise
# pdf = np.exp(-(y-2*x*(1-x))**2/2/0.1**2)

vt.plot.show(pdf, title='(amplitude, gradient norm) pdf')

# generate noise
w = vt.noise.random_amplitude_gradient_pdf(shape,
                                           res,
                                           pdf,
                                           scale=10,
                                           seed=seed)

vt.plot.show(w, title='primitive noise function')

# build up fractal texture
res = (2, 2)
w = vt.noise.fractal2d(shape,
                       res,
                       seed=seed,
                       noise_fct=vt.noise.random_amplitude_gradient_pdf,
                       pdf=pdf,
                       scale=20)

vt.plot.show(w, title='fractal sum')

plt.show()
