import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w, cmap='gray')

wx = vt.noise.perlin2d(shape, res, seed + 1)
wy = vt.noise.perlin2d(shape, res, seed + 2)

w = vt.op.warp2d(x=w, wx=wx, wy=wy, scale=0.4)
vt.plot.show(w, cmap='gray')

plt.show()
