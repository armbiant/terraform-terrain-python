import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w)

w = vt.op.subsample(w, subsampling=10)
vt.plot.show(w)

plt.show()
