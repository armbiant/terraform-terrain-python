import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

# w = vt.noise.perlin2d(shape, res, seed)
w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

mesh = vt.mesh.hmap_to_trimesh(0.2 * w)

fname = 'data/texture_512x256_test_topleft.png'

vt.mesh.set_texture_file(mesh, fname, heightmap_shape=shape)

# set offscreen to False to get the GUI
offscreen = True

vt.render.show(mesh, offscreen=offscreen)

plt.show()
