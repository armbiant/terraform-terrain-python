import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

n = 20
seed = 1  # set to -1 for "no seed"
extent = [0.5, 1.5, -1, 2]

# --- purely random (NB - 'extent' is always optional for random grid
# --- generators)
x, y = vt.grid.random_grid(n, seed=seed, extent=extent)

plt.figure()
plt.plot(x, y, 'k.')
plt.title('random_grid')
plt.gca().axis('equal')

# --- gather mask
shape = (256, 128)
res = (4, 4)

mask = vt.noise.perlin2d(shape, res, seed)
mask = np.where(mask > 0.8, 1, 0)

x, y = vt.grid.random_grid(n=500, seed=seed, extent=extent, mask=mask)

vt.plot.show(mask, cmap='gray', title='with mask gathering', extent=extent)
plt.plot(x, y, 'b.', ms=4)

# --- Halton
x, y = vt.grid.random_grid_halton(n=500, seed=seed, extent=extent)

plt.figure()
plt.plot(x, y, 'k.')
plt.title('random_grid_halton')
plt.gca().axis('equal')

# --- jittered
x, y = vt.grid.random_grid_jittered(dx=0.05, dy=0.05, seed=seed, extent=extent)

plt.figure()
plt.plot(x, y, 'k.')
plt.title('random_grid_jittered')
plt.gca().axis('equal')

plt.show()
