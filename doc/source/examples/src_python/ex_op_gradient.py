import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w)

dwx, dwy = vt.op.gradient(w)
dwa = vt.op.gradient_angle(w)
dwn = vt.op.gradient_norm(w)
dws = vt.op.gradient_slope(w)  # depend on x and y

vt.plot.show(np.dstack((dwx, dwy)), title='gradient components /x and /y')

# quiver plot of the gradient vector
u = dwn * np.cos(dwa)
v = dwn * np.sin(dwa)

vt.plot.quiver(u, v, z=w)

vt.plot.show(dws, title='slope in degrees')

plt.show()
