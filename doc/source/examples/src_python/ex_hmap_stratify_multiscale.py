import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (512, 512)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.erosion.hydraulic_particles(w, radius=5e-3)

# --- multiscale stratifying

# Default layer parameters:
# layers_param = [
#     {
#         'n_layers': 3,
#         'gamma': 0.4,
#         'skip_first_layer': True,
#     },
#     {
#         'n_layers': [1, 4],
#         'gamma': 0.7,
#         'noise_z': 0.4,
#         'skip_first_layer': False,
#     },
#     {
#         'n_layers': [1, 4],
#         'gamma': 0.5,
#         'skip_first_layer': False,
#     },
# ]

ws = vt.hmap.stratify_multiscale(w, partition=(4, 4), seed=1)

vt.plot.topo(ws, title='cell-based partitioning')

# --- 'hand-made' partitionning (in 3 stripes)
mask = np.zeros(shape)
i3 = int(shape[0] / 3)
mask[:i3, :] = 1
mask[i3:2 * i3, :] = 2
mask[2 * i3:, :] = 3

wh = vt.hmap.stratify_multiscale(w,
                                 transition_radius=0.01,
                                 partition=mask,
                                 seed=1)

vt.plot.topo(wh, title='use-defined partitioning')

# --- render

# add some cosmethic
w_rock = np.copy(ws)

ws, smap = vt.erosion.sediment_deposition(ws,
                                          max_deposition=0.001,
                                          talus=0.05 *
                                          vt.op.gradient_talus(ws),
                                          deposition_map=ws)
ws = vt.erosion.hydraulic_particles(ws, z_bedrock=w_rock, radius=1e-2)

mesh = vt.mesh.hmap_to_trimesh(0.2 * ws)
vt.render.show(mesh, offscreen=True)

plt.show()
