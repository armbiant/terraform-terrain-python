import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

rng = np.random.default_rng(seed)

# base heightmap

z = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)
vt.plot.topo(z, title='original heightmap', vmin=0, vmax=2, cmap='terrain')

# point-wise alteration: make a 'hole'
z = vt.hmap.alter(z, idx=[[82, 96]], new_elevations=-0.1)

vt.plot.topo(z, title='point-wise alteration', vmin=0, vmax=2, cmap='terrain')
plt.plot(82, 96, 'k.')

# find local extremas and set their height to 1.5 (instead of 1)
idx = vt.op.local_maxima(z, prefilter=True)
za = vt.hmap.alter(z, idx=idx, new_elevations=1.5, kernel_scaling=1)

vt.plot.topo(za, title='peak alteration', vmin=0, vmax=2, cmap='terrain')
plt.plot(idx[:, 0], idx[:, 1], 'k.')

# random peak heights
idx = vt.op.local_maxima(z, prefilter=True)
za = vt.hmap.alter(z, idx=idx, new_elevations=1 + rng.random(len(idx)))

vt.plot.topo(za, title='random peak height', vmin=0, vmax=2, cmap='terrain')
plt.plot(idx[:, 0], idx[:, 1], 'k.')

# alternative kernel
idx = vt.op.local_maxima(z, prefilter=True)
za = vt.hmap.alter(z,
                   idx=idx,
                   new_elevations=1.5,
                   kernel=vt.kernel.smooth_square,
                   distance_exponent=0.5)

vt.plot.topo(za, title='smooth square kernel', vmin=0, vmax=2, cmap='terrain')
plt.plot(idx[:, 0], idx[:, 1], 'k.')

plt.show()
