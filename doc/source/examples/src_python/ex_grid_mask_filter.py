import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

rng = np.random.default_rng(seed)

# random set of points
n = 500
x = rng.random(n)
y = rng.random(n)

w = vt.noise.perlin2d(shape, res, seed=seed)
mask = np.where(w > 0.5, True, False)

xf, yf, idx = vt.grid.mask_filter(x, y, mask)

vt.plot.show(mask, cmap='gray', title='mask')

plt.figure()
plt.plot(x, y, 'k.', ms=1)
plt.plot(xf, yf, 'r.', ms=4)
plt.gca().axis('equal')

plt.show()
