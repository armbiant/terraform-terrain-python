import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
seed = 1  # set to -1 for "no seed"

w = vt.noise.diamond_square2d(shape, seed=seed, persistence=0.7)
vt.plot.show(w)

# display
plt.show()
