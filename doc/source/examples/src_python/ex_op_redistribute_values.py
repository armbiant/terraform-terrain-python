import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

wpp = vt.op.redistribute_values(w, pdf='pyramid')
wpr = vt.op.redistribute_values(w, pdf='pyramid_r')
wpd = vt.op.redistribute_values(w, pdf='diamond', xc=0.1)
wph = vt.op.redistribute_values(w, pdf='hourglass')

# plot distributions
fig, axs = plt.subplots(5, 1)
axs[0].hist(w.ravel(), bins=40, color='k')
axs[1].hist(wpp.ravel(), bins=40, color='r')
axs[2].hist(wpr.ravel(), bins=40, color='g')
axs[3].hist(wpd.ravel(), bins=40, color='b')
axs[4].hist(wph.ravel(), bins=40, color='y')

# render
vt.render.show(vt.mesh.hmap_to_trimesh(0.2 * w), offscreen=True)
vt.render.show(vt.mesh.hmap_to_trimesh(0.2 * wpp), offscreen=True)
vt.render.show(vt.mesh.hmap_to_trimesh(0.2 * wpr), offscreen=True)
vt.render.show(vt.mesh.hmap_to_trimesh(0.2 * wpd), offscreen=True)
vt.render.show(vt.mesh.hmap_to_trimesh(0.2 * wph), offscreen=True)

plt.show()
