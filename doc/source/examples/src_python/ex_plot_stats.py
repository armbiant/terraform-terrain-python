import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
vt.plot.show(w, title='original')

# show distributions of elevations and gradient norms
vt.plot.histograms(w)

# 'sharpen' the map and for comparison
w = vt.op.harden(w, gain=2)
vt.plot.show(w, title='after sharpening')

vt.plot.histograms(w)

plt.show()
