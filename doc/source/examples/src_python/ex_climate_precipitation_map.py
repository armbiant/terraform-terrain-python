import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

# initial terrain
w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

pmap = vt.climate.precipitation_map(w,
                                    wind_angle=30 / 180 * np.pi,
                                    cloud_radius=0.05)

# resulting map can be used for the erosion moisture map
we = vt.erosion.hydraulic_particles(w, moisture_map=pmap)

vt.plot.show(w, cmap='terrain', title='initial')
vt.plot.show(pmap, cmap='plasma_r', title='precipitation map')
vt.plot.show(we, cmap='terrain', title='eroded terrain')

plt.show()
