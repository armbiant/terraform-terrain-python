import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.erosion.hydraulic_particles(w, radius=5e-3)

# --- stratify

# gamma < 1 => convexe layers
ws_cx, _ = vt.hmap.stratify(w, n_layers=4, gamma=0.5, seed=1)

# gamma > 1 => concave layers
ws_cv, _ = vt.hmap.stratify(w, n_layers=4, gamma=2, seed=1)

vt.plot.topo(w, title='initial')
vt.plot.topo(ws_cx, title='convexe')
vt.plot.topo(ws_cv, title='concave')

# --- render

mesh = vt.mesh.hmap_to_trimesh(0.2 * ws_cx)
vt.render.show(mesh, offscreen=True)

plt.show()
