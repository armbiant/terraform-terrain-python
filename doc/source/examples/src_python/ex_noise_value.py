import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (4, 2)
seed = 1  # set to -1 for "no seed"

w = vt.noise.value2d(shape, res, seed=seed)
vt.plot.show(w)

# display
plt.show()
