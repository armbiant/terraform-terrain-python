import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d,
                                   offset=0.3)
dw = vt.op.gradient_norm(w)

# --- bivariate colormap, depending on two parameters (here altitude
# --- and slope)

# create colormap (parameter #1: from green to white, parameter #2:
# from light gray to black)
colors_manual = np.array(
    [
        [[46, 139, 87, 255], [244, 164, 96, 255]],  # green / brown
        [[255, 255, 255, 255], [0, 0, 0, 255]],  # white / black
    ],
    dtype=int)

# colorize with elevation as parameter #1 and slope as #2 => low
# elevations are green, high elevations are white, strong gradients at
# high elevations are black and strong gradients at low elevations are
# brown
img = vt.plot.colorize_bivariate(w, dw, colors_manual)

plt.figure()
plt.imshow(img)

plt.show()
