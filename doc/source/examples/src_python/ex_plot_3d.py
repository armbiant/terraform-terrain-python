import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

vt.plot.show3d(w, title='3D view')

plt.show()
