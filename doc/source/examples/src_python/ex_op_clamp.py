import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w, vmin=0, vmax=1)

w = vt.op.clamp(w, vmin=0.4, vmax=0.6)
vt.plot.show(w, vmin=0, vmax=1)

plt.show()
