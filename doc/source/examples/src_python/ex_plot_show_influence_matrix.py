import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (2, 2)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

vt.plot.show(w, title='initial heightmap')

# custom function to separate the 2 parameters
func = lambda radius, p_capacity: vt.erosion.hydraulic_particles(
    w, particle_density=0.2, radius=radius, p_capacity=p_capacity)

# how the parameters vary
args_dict = {
    'radius': [None, 1e-3, 1e-2],
    'p_capacity': [1, 20, 100],
}

vt.plot.show_influence_matrix(func=func, args_dict=args_dict)

plt.show()
