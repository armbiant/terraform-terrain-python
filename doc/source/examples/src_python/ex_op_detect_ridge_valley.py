import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 2

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
vt.plot.show(w)

# "continuous output"
rm, va = vt.op.detect_ridge_valley(w)

plt.figure()
plt.imshow(w.T, origin='lower', cmap='gray')
plt.imshow(rm.T, origin='lower', cmap='jet', alpha=rm.T)

# "binary output"
rm, va = vt.op.detect_ridge_valley(w,
                                   binary_output=True,
                                   binary_threshold=0.05,
                                   size=10)

vt.plot.show(rm, cmap='gray')

plt.show()
