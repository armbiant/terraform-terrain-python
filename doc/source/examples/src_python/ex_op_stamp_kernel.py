import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (512, 512)
res = (2, 2)
seed = 1

# define stamping kernel
k = vt.noise.hybrid_multifractal2d((64, 64),
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
k = vt.op.enforce_boundaries(k)

# generate stamping intensity map
x, y = vt.grid.random_grid(n=20, seed=seed)
mask, idx = vt.hmap.generate_ridge(shape,
                                   x,
                                   y,
                                   fractal_iterations=4,
                                   kernel='gaussian',
                                   radius=0.02)

# --- stamp
w = vt.op.stamp_kernel(mask, k, density=0.1, seed=seed)

# plots
vt.plot.topo(k, title='stamping kernel')
vt.plot.show(mask, title='stamping intensity', cmap='gray')
vt.plot.topo(w, title='heightmap after stamping')

# render
mesh = vt.mesh.hmap_to_trimesh(0.2 * w)
vt.render.show(mesh, offscreen=True)

plt.show()
