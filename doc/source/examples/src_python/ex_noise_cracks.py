import matplotlib.pyplot as plt
import numpy as np
#
import pyrender
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.cracks(shape,
                    res,
                    width=0.05,
                    transition_radius=0.01,
                    noise_displacement=0.1,
                    seed=seed)

vt.plot.show(w, cmap='gray', title='based on a regular grid')

w = vt.noise.cracks(shape,
                    res=30,
                    width=0.001,
                    transition_radius=0.01,
                    noise_displacement=0.1,
                    seed=seed)

vt.plot.show(w, cmap='gray', title='random grid')

plt.show()
