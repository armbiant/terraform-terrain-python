import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (4, 2)
seed = 1

w = vt.noise.perlin2d(shape, res, seed=seed)
vt.plot.show(w)

mask_angle = vt.mask.from_angle(w, hard_mask=True, angle_thresholds=[85, 95])
vt.plot.show(mask_angle, title='angle', cmap='nipy_spectral')

mask_convexity = vt.mask.from_convexity(w)
vt.plot.show(mask_convexity, title='convexity', cmap='gray')

plt.show()
