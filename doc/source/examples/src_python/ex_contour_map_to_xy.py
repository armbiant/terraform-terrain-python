import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (4, 2)
seed = 1  # set to -1 for "no seed"

# create mask
w = vt.noise.perlin2d(shape, res, seed=seed)
w = np.where(w >= 0.6, 1, 0)

# build a list of contours based on this mask
xc_list, yc_list = vt.contour.map_to_xy(w, discretization_density=0.1)

vt.plot.show(w, cmap='gray')

for xc, yc in zip(xc_list, yc_list):
    plt.plot(xc, yc, 'r.-')

plt.show()
