import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

# default parameters
wp1 = vt.op.plateau(w)

# larger plateau, sharper transitions
wp2 = vt.op.plateau(w, plateau_size=0.2, gain=4)

vt.plot.topo(w, title='initial')
vt.plot.topo(wp1, title='default plateau')
vt.plot.topo(wp2, title='larger / sharper')

plt.show()
