import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# prepare heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.op.steepen(w, scale=0.2, elevation_scaling=1)
w = vt.erosion.hydraulic_particles(w, radius=1e-3)

vt.plot.show(w, title='heightmap')

mask = vt.climate.snowfall_map(w,
                               talus=0.015,
                               lowest_elevation=0.5,
                               sun_exposure_influence=1)

vt.plot.show(mask, cmap='gray', title='snowfall intensity')

plt.show()
