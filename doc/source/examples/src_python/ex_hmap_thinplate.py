import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 2

rng = np.random.default_rng(seed)

# "background" random amplitudes
n = 20
x = rng.random(n)
y = rng.random(n)
z = rng.random(n)

# add two prominent peaks
x = np.append(x, [0.4, 0.7])
y = np.append(y, [0.3, 0.5])
z = np.append(z, [4, 3])

w = vt.hmap.generate_thinplate(shape, x, y, z)

vt.plot.show(w, cmap='terrain')
plt.plot(x * shape[0], y * shape[1], 'k.')

# add some fractal noise for a more realistic terrain
res = (2, 2)
w += vt.noise.fractal2d(shape,
                        res,
                        seed=seed,
                        noise_fct=vt.noise.perlin2d,
                        octaves=8)
vt.plot.topo(w)

plt.show()
