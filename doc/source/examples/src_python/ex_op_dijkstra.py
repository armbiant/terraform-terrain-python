import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

idx_start = (30, 12)
idx_end = (248, 108)

indices = vt.op.dijkstra(w, idx_start, idx_end)

# increasing the "distance exponent" reduces the overall cumulative
# elevation gain (but the path is likely longer)
indices_low_gaps = vt.op.dijkstra(w, idx_start, idx_end, distance_exponent=4)

vt.plot.show(w, cmap='gray')
plt.plot(indices[:, 0], indices[:, 1], 'b-', lw=2)
plt.plot(indices_low_gaps[:, 0], indices_low_gaps[:, 1], 'r-', lw=2)

plt.show()
