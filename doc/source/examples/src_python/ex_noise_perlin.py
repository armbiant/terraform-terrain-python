import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (4, 2)
seed = 1  # set to -1 for "no seed"

# --- Perlin noise
w = vt.noise.perlin2d(shape, res, seed=seed)
vt.plot.show(w)

# Limited area of the noise can be computed independently. The whole
# domain is a unit square.
xmin = 0.1
xmax = 0.6
ymin = 0.2
ymax = 0.7

plt.plot(
    np.asarray([xmin, xmax, xmax, xmin, xmin]) * (shape[0] - 1),
    np.asarray([ymin, ymin, ymax, ymax, ymin]) * (shape[1] - 1), 'k--')

shape = (256, 256)
x = np.linspace(xmin, xmax, shape[0])
y = np.linspace(ymin, ymax, shape[1])

w = vt.noise.perlin2d_xy(x, y, res, seed=seed)
vt.plot.show(w)

# --- Exponentially distributed Perlin's noise: an additional
# --- parameter 'mu' allow to tweak the gradient distribution by
# --- reducing the occurence probability of large gradients
w = vt.noise.perlin2d(shape, res, seed=seed, mu=0.2)
vt.plot.show(w)

# display
plt.show()
