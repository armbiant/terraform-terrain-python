import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (4, 2)
seed = 1

w = vt.noise.perlin2d(shape, res, seed=seed)
vt.plot.show(w)

i = 10
j = 20
shape_out = (32, 64)
wloc = vt.op.extract_subarray(w, i, j, shape_out)

vt.plot.show(wloc)

plt.show()
