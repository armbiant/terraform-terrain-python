import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

# prepare heightmap and define a path
w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

idx_start = (10, 20)
idx_end = (shape[0] - 10, shape[1] - 20)

path = vt.roads.find_path(w,
                          idx_start=idx_start,
                          idx_end=idx_end,
                          subsampling=5,
                          distance_exponent=4)

# dig the path
wp, mask = vt.roads.dig_path(w,
                             path=path,
                             path_width=3,
                             path_depth=0.1,
                             sigma=2)

vt.plot.show(wp, title='heightmap with the path dug')
plt.plot(path[:, 0], path[:, 1], 'k:', lw=0.5)

vt.plot.show(mask, cmap='gray', title='path mask')

plt.show()
