import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

wg = vt.op.gamma_correction(w)

vt.plot.diff(w, wg)

# gamma < 1 => more concave (round) heightmap
mesh = vt.mesh.hmap_to_trimesh(0.2 * vt.op.gamma_correction(w, gamma=0.4))
data = vt.render.show(mesh, offscreen=True)

# gamma > 1 => more concexe (sharp) heightmap
mesh = vt.mesh.hmap_to_trimesh(0.2 * vt.op.gamma_correction(w, gamma=4))
data = vt.render.show(mesh, offscreen=True)

plt.show()
