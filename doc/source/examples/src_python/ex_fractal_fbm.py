import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# Fractal noise can be build up using any other primitive noise
# function
w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
vt.plot.show(w)

# parameters of the primitive function are automatically passed
# through
w = vt.noise.fractal2d(shape,
                       res,
                       seed=seed,
                       noise_fct=vt.noise.perlin2d,
                       mu=0.2)
vt.plot.show(w)

w = vt.noise.fractal2d(shape,
                       res,
                       seed=seed,
                       noise_fct=vt.noise.worley2d,
                       distance_exponent=1)
vt.plot.show(w)

plt.show()
