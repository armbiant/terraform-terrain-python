import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
w += 0.2 * vt.noise.white2d(shape, seed)

idx = vt.op.local_maxima(w, prefilter=True)

vt.plot.show(w, cmap='gray')
plt.plot(idx[:, 0], idx[:, 1], 'bo')

plt.show()
