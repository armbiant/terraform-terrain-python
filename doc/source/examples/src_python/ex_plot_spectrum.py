import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.gabor2d(shape,
                     width=0.1,
                     f0=20,
                     w0=30 / 180 * np.pi,
                     seed=seed,
                     sparse=True)

vt.plot.show(w, cmap='gray')

vt.plot.spectrum(w)

plt.show()
