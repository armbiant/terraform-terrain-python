import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (512, 512)
res = (2, 2)
seed = 1

w = 0.1 * vt.noise.hybrid_multifractal2d(
    shape, res, seed=seed, noise_fct=vt.noise.perlin2d, offset=0.3)

# outcrops
w = w + 0.2 * vt.hmap.outcrops(shape, radius=0.2, density=0.2, seed=1)

# cosmethic
w, smap = vt.erosion.sediment_deposition(w,
                                         max_deposition=0.001,
                                         talus=0.01 *
                                         vt.op.gradient_talus(w).max(),
                                         deposition_map=1 - vt.op.remap(w))

# plot and render
vt.plot.topo(w, cmap=None)

mesh = vt.mesh.hmap_to_trimesh(0.2 * w)
vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [237, 201, 175, 255]],
                  values=vt.op.remap(smap)**0.5)
vt.render.show(mesh, offscreen=True)

plt.show()
