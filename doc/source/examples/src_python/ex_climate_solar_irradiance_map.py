import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

# initial terrain (add erosion to make shadows more "readable")
w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
w = vt.erosion.hydraulic_particles(w, radius=1e-2)

wr = vt.climate.solar_irradiance_map(w)

# plots
vt.plot.show(w, title='heighmap')
vt.plot.show(wr, cmap='gray', title='irradiance map')

plt.show()
