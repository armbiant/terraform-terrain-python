import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
vt.plot.topo(w)

# simple hydraulic based on flow rate (uniform here)
wf = vt.erosion.hydraulic_flow_stream(w,
                                      slope_threshold=0.1,
                                      intensity=0.01,
                                      flow_stream=1)
vt.plot.topo(wf)

plt.show()
