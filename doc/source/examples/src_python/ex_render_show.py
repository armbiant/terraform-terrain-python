import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

# w = vt.noise.perlin2d(shape, res, seed)
w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

mesh = vt.mesh.hmap_to_trimesh(0.2 * w)

# base colormap
vt.mesh.set_color(mesh, color='terrain')

# color in transparent red an elevation strip
vt.mesh.set_color(mesh,
                  color=[255, 0, 0, 255],
                  mask=np.where((w > 0.5) & (w < 0.6), True, False))

# set offscreen to False to get the GUI
offscreen = True

vt.render.show(mesh, offscreen=offscreen)

plt.show()
