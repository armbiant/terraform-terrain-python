import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

z = 0.5 * vt.noise.fractal2d(
    shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

x, y = np.meshgrid(np.linspace(-1, 1, z.shape[0]),
                   np.linspace(-1, 1, z.shape[1]),
                   indexing='ij')
z += np.exp(-(x**2 + y**2) / 0.3**2)

# moisture/rain map: no rain on the left half part of the map
mask = np.zeros(shape)
mask[int(shape[0] / 3):, :] = 1

smap, pmap, mask_label = vt.hydrology.system_map(z,
                                                 moisture_map=mask,
                                                 area_threshold=100)

vt.plot.show(z, cmap='terrain', title='heightmap')
vt.plot.show(smap, cmap='jet', title='stream map')
vt.plot.show(pmap, cmap='jet', title='pool map')

# stream and pool maps (intersected)
vt.plot.show(z, cmap='gray')

plt.imshow(np.where(smap > 0, smap, np.nan).T, origin='lower', cmap='jet')

plt.imshow(pmap.T, origin='lower', cmap='Blues', alpha=0.7)

# --- render

# define water elevation array
z_water = np.where(pmap > 0, pmap, z)
mask_pool = np.where(pmap > 0, 1, 0)
mask_stream = np.where(smap > 2e-1, 1, 0)

mesh_water = vt.mesh.hmap_to_trimesh(0.2 * z_water)

vt.mesh.set_color(mesh_water, color=[0, 0, 255, 255], mask=mask_pool)
vt.mesh.set_color(mesh_water, color=[0, 50, 255, 255], mask=mask_stream)

vt.render.show(mesh_water, offscreen=True)

plt.show()
