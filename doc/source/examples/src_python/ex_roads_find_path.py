import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

idx_start = (30, 12)
idx_end = (248, 108)

# subsampling of the original map => much faster and similar results
subsampling = 5

path = vt.roads.find_path(w,
                          idx_start=idx_start,
                          idx_end=idx_end,
                          subsampling=subsampling)

# mask to avoid the center of the map
mask_nogo = np.zeros(shape)
mask_nogo[64:192, 32:96] = 1

path_ng = vt.roads.find_path(w,
                             idx_start=idx_start,
                             idx_end=idx_end,
                             mask_nogo=mask_nogo,
                             subsampling=subsampling)

# increasing the "distance exponent" reduces the overall cumulative
# elevation gain (but the path is likely longer)
path_d = vt.roads.find_path(w,
                            idx_start=idx_start,
                            idx_end=idx_end,
                            subsampling=subsampling,
                            distance_exponent=4)

vt.plot.show(w, cmap='gray')
plt.plot(path[:, 0], path[:, 1], 'b-', lw=2)
plt.plot(path_ng[:, 0], path_ng[:, 1], 'g-', lw=2)
plt.plot(path_d[:, 0], path_d[:, 1], 'r-', lw=2)

plt.show()
