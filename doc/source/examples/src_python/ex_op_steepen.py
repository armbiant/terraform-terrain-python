import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w0 = vt.noise.hybrid_multifractal2d(shape,
                                    res,
                                    noise_fct=vt.noise.perlin2d,
                                    seed=seed)

vt.plot.show(w0, title='initial heightmap')

w = vt.op.steepen(w0, scale=0.1)
vt.plot.show(w, title='after steepening')

w = vt.op.steepen(w0, scale=0.1, reverse=True, elevation_scaling=1)
vt.plot.show(w, title='reverse steepening => flattening')

plt.show()
