import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)

# base array, shape 'shape'
w = np.zeros(shape)

# smaller array, shape(62, 62)
k = vt.kernel.gabor_shape(shape=(64, 64), f0=3, w0=15 / 180 * np.pi)
vt.plot.show(k)

# add 'k' to 'w', with 'k' centered on indices (100, 50)
w = vt.op.add_array(w, 100, 50, k)
vt.plot.show(w)

plt.show()
