import matplotlib.pyplot as plt
import numpy as np
import scipy
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

# domain extent
extent = [0, 1, 0, shape[1] / shape[0]]

# generate a set of random nodes
rng = np.random.default_rng(seed)
n = 30
x, y = vt.grid.random_grid_halton(n, seed=seed, extent=extent)

# --- basic usage (convert point cloud to a minimal spanning tree and
# --- project it to the map to create a mask)

w, idx = vt.hmap.generate_ridge(shape, x, y, extent=extent)

vt.plot.show(w, cmap='gray', extent=extent, title='basic')

# add graph layout
plt.plot(x, y, 'g.')
for pq in idx:
    plt.plot(x[pq], y[pq], 'g-', lw=0.2)

# --- add Gaussian smoothing and fractalize contour

w, idx = vt.hmap.generate_ridge(shape,
                                x,
                                y,
                                extent=extent,
                                fractal_iterations=4,
                                kernel='gaussian',
                                radius=0.02)

# add for instance some background fractal elevation to get a more
# realistic map
wt = w + vt.noise.hybrid_multifractal2d(
    shape, res, noise_fct=vt.noise.perlin2d, seed=seed)

# plots
vt.plot.show(w,
             cmap='gray',
             extent=extent,
             title='fractal contour + smoothing')

plt.plot(x, y, 'g.')
for pq in idx:
    plt.plot(x[pq], y[pq], 'g-', lw=0.2)

vt.plot.show(wt,
             cmap='terrain',
             extent=extent,
             title='fractal contour + smoothing + fractal background')

plt.show()
