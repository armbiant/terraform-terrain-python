import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (512, 512)
res = (2, 2)
seed = 1

# prepare heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.op.steepen(w, scale=0.2, elevation_scaling=1)
w = vt.erosion.hydraulic_particles(w, radius='medium')

ws, smap = vt.erosion.snow_deposition(w,
                                      max_deposition=0.05,
                                      snow_talus=0.002,
                                      lowest_elevation=0.5,
                                      iterations=20)

# plots
vt.plot.topo(w, title='before')
vt.plot.show(smap, title='snow thickness', cmap='nipy_spectral')
vt.plot.topo(ws, title='after')

# --- 3D rendering

mesh = vt.mesh.hmap_to_trimesh(0.2 * ws)

vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [255, 255, 255, 255]],
                  values=vt.op.remap(smap)**0.5)

data = vt.render.show([mesh], offscreen=True)

plt.show()
