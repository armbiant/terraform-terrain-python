import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
vt.plot.show(w)

# Musgrave grid-based erosion
wm = vt.erosion.hydraulic_musgrave(w,
                                   water_level=0.001,
                                   iterations=200,
                                   tau=1e-2)
vt.plot.show(wm)

plt.show()
