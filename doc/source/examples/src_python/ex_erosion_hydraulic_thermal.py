import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

# talus limit
talus = 0.05 * vt.op.gradient_talus(w).max()

# combined erosion
hp = {
    'radius': 'large',
    'particle_density': 0.2,
}

tp = {
    'talus': talus,
    'iterations': 10,
    'smooth_landing': True,
    'auto_bedrock': True,
}

we, smap = vt.erosion.hydraulic_thermal(w,
                                        iterations=1,
                                        hydraulic_param=hp,
                                        thermal_param=tp)

vt.plot.show(w, title='initial')
vt.plot.topo(we, title='after erosion')

# --- 3D rendering show different layers based on splatmap
mesh = vt.mesh.hmap_to_trimesh(0.2 * we)

vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [237, 201, 175, 255]],
                  values=vt.op.remap(smap)**0.5)

vt.render.show(mesh, offscreen=True)

plt.show()
