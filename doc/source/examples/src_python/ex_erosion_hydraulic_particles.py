import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

# --- Basic usage (with moisture map)

shape = (256, 128)
res = (4, 4)
seed = 1

w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

vt.plot.topo(w, title='original')

x, y = np.meshgrid(np.linspace(0, 1, shape[0]),
                   np.linspace(0, 1, shape[1]),
                   indexing='ij')

# add a strong slope in the transverse 'x' direction
w += 4 * x

# only rain on the right part of the domain (but particles still
# travel to the left and erode the heightmap for x < 0.5)
moisture_map = (x > 0.5).astype(float)

# optional parameter 'radius=None': pixel-based erosion (no kernel) =>
# much faster
we = vt.erosion.hydraulic_particles(w, moisture_map=moisture_map)
vt.plot.topo(we, title='with moisture map')

# --- Heightmap resolution independence: resulting eroded maps show
# --- little variations with grid resolution (as long as 'radius'
# --- parameter is set)

for shape in [(64, 32), (128, 64), (256, 128), (512, 256)]:
    res = (4, 4)
    seed = 1

    w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

    # radius=1e-2 means an erosion kernel radius of 1/100th of the
    # domain size (large radius leads to large kernels... and large
    # restitution times)
    we = vt.erosion.hydraulic_particles(w, particle_density=0.2, radius=1e-3)
    vt.plot.topo(we, title=str(shape))

# --- User-defined erosion/deposition kernel

kernel = np.array([[0, 0.5, 0], [0.5, 1, 0.5], [0, 0.5, 0]])
we = vt.erosion.hydraulic_particles(w, particle_density=0.5, kernel=kernel)
vt.plot.topo(we, title='user-defined kernel')

plt.show()
