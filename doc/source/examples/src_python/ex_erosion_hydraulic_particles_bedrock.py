import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

# defined a bedrock elevation slightly under the initial elevation map
wbd = vt.op.steepen(w, scale=0.2, elevation_scaling=1)
wbd = vt.erosion.hydraulic_particles(wbd, radius=None)
wbd = vt.op.remap(wbd, w.min() - 0.01, w.max())

# erosion
we = vt.erosion.hydraulic_particles(w, z_bedrock=wbd, radius='large')
vt.plot.show(we, title='after erosion')

# --- splatmap
wsp = vt.erosion.splatmap(z_eroded=we, z_initial=w, z_bedrock=wbd)
vt.plot.show(wsp, title='splatmap')

# --- 3D rendering
mesh = vt.mesh.hmap_to_trimesh(0.2 * we)

# deposition
vt.mesh.set_color(mesh,
                  color=[72, 111, 56, 255],
                  mask=np.where(wsp == 1, 1, 0))

# erosion
vt.mesh.set_color(mesh,
                  color=[237, 201, 175, 255],
                  mask=np.where(wsp == 2, 1, 0))

# bedrock
vt.mesh.set_color(mesh, color=[30, 30, 40, 255], mask=np.where(wsp == 3, 1, 0))

data = vt.render.show(mesh, offscreen=True)

plt.show()
