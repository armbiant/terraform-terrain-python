import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
r0 = vt.op.rugosity(w)
r1 = vt.op.rugosity(w, radius=0.1)

vt.plot.topo(w, title='heightmap')
vt.plot.show(r0, title='averaging radius = 0', cmap='nipy_spectral')
vt.plot.show(r1, title='averaging radius = 0.1', cmap='nipy_spectral')

plt.show()
