import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (4, 4)

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

wr = vt.op.remap_local(w, radius=0.2, vmin=0, vmax=1)

vt.plot.show(w)
vt.plot.show(wr)

plt.show()
