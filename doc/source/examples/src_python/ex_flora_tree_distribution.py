import matplotlib.pyplot as plt
import numpy as np
import trimesh
#
import vterrain as vt

shape = (512, 512)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d)

# define tree species (dimensions in "meters", in [1, 10])
slist = [
    vt.flora.TreeGrowthGrammar().use_preset('test'),
    vt.flora.TreeGrowthGrammar().use_preset('third'),
    vt.flora.TreeGrowthGrammar().use_preset('low'),
    vt.flora.TreeGrowthGrammar().use_preset('mid_small'),
    vt.flora.TreeGrowthGrammar().use_preset('low_alt'),
]

# create distribution
dist = vt.flora.TreeDistribution(z=w,
                                 growth_grammar_list=slist,
                                 extent=[0, shape[0] - 1, 0, shape[1] - 1],
                                 seed=seed)

x, y, z, label, radius = dist.spawn_trees()

# --- plot
print('plotting...')

vt.plot.topo(w, title='tree distribution')


# helper
def plot_circle(xc, yc, rc, color='k'):
    t = np.linspace(0, 2 * np.pi, 10)
    x = xc + rc * np.cos(t)
    y = yc + rc * np.sin(t)
    plt.plot(x, y, '-', color=color)


for s in np.unique(label):
    idx = np.where(label == s)
    color = ['tab:blue', 'tab:orange', 'tab:green', 'm', 'c', 'k'][s]

    for i in idx[0]:
        plt.plot(x[i], y[i], '.', color=color, ms=1)
        plot_circle(x[i], y[i], radius[i], color=color)

vt.plot.show(dist.label, cmap='Accent', title='tree species')
vt.plot.show(dist.density, title='tree density')

# --- rendering
print('rendering...')

z_scale = 0.2
xy_scale = 1 / max(shape)
mlist = []

# hmap
mesh = vt.mesh.hmap_to_trimesh(z_scale * w)
mlist.append(mesh)

#
for s in np.unique(label):
    color = [
        [255, 60, 60, 123],
        [0, 255, 0, 123],
        [255, 255, 0, 123],
        [0, 255, 255, 123],
        [255, 123, 10, 123],
    ][s]

    idx = np.where(label == s)
    tmp_list = []

    for i in idx[0]:
        r = radius[i] * xy_scale  # / max(shape)
        ar = 4
        cone = trimesh.creation.cone(radius=r, height=ar * r, sections=4)
        cone.apply_translation(
            [x[i] * xy_scale, y[i] * xy_scale, z_scale * z[i]])

        tmp_list.append(cone)

    combined_mesh = trimesh.util.concatenate(tmp_list)
    vt.mesh.set_color(combined_mesh, color)

    mlist.append(combined_mesh)

vt.render.show(mlist, offscreen=True)

plt.show()
