import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 64)
seed = 1

w = vt.noise.spot2d(shape, kernel=np.ones((10, 10)), seed=seed)
vt.plot.show(w, cmap='gray')

w = vt.noise.spot2d(shape,
                    kernel=vt.kernel.smooth_cosine((16, 16)),
                    seed=seed,
                    sparse=True,
                    density=0.05)
vt.plot.show(w, cmap='gray')

plt.show()
