import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = 0.1 * vt.noise.hybrid_multifractal2d(
    shape, res, seed=seed, noise_fct=vt.noise.perlin2d, offset=0.3)

# convexe / sharp rocks
wcx = w + 0.2 * vt.hmap.rocks(shape, radius=0.1, density=0.2, gamma=2)

# concave / sharp rocks
wcv = w + 0.2 * vt.hmap.rocks(shape, radius=0.2, density=0.1, gamma=0.5)

# plots
vt.plot.topo(wcx, cmap=None, title='convexe')
vt.plot.topo(wcv, cmap=None, title='concave')

# --- add some sediments for the render
wcx, smap = vt.erosion.sediment_deposition(wcx,
                                           max_deposition=0.01,
                                           talus=0.01 *
                                           vt.op.gradient_talus(wcx).max())

mesh = vt.mesh.hmap_to_trimesh(0.2 * wcx)

vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [237, 201, 175, 255]],
                  values=vt.op.remap(smap)**0.5)
vt.render.show(mesh, offscreen=True)

plt.show()
