import matplotlib.pyplot as plt
import numpy as np
import os
import scipy
#
import vterrain as vt

shape = (512, 256)
res = (4, 4)
seed = 2

# base heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

# --- random set of node indices (representing some landmarks or
# --- cities for instance)
rng = np.random.default_rng(seed)
n = 10
i, j = vt.grid.random_grid_halton(n,
                                  seed=seed,
                                  extent=[30, shape[0] - 1, 20, shape[1] - 1])
idx = np.dstack((i.astype(int), j.astype(int)))[0]

# optional: add more importance to some nodes, here #4 (i.e. for a
# bigger city)
nodes_weight = np.ones(n)
nodes_weight[4] = 5

# --- road network
weights, path_dict = vt.roads.generate_network(
    z=w,
    nodes_idx=idx,
    nodes_weight=nodes_weight,  # optional
    subsampling=5,
    distance_exponent=2)

# --- plot everything
vt.plot.show(w, cmap='gray')

# nodes
plt.plot(i, j, 'wo')
for p in range(n):
    plt.text(idx[p, 0], idx[p, 1], str(p), color='w')

# primary roads (positive weights) = roads with higher weights and
# necessary to connect the graph
p, q = np.where(weights > 0)[:]
for p_, q_ in zip(p, q):
    path = path_dict[(p_, q_)]
    plt.plot(path[:, 0], path[:, 1], 'b-', lw=2)

# secondary roads (negative weights) = alternative routes, not
# necessary to connect the nodes
p, q = np.where(weights < 0)[:]
for p_, q_ in zip(p, q):
    path = path_dict[(p_, q_)]
    plt.plot(path[:, 0], path[:, 1], 'r-', lw=0.5)

# --- schematic plot
plt.figure()

# nodes
plt.plot(i, j, 'kx')
for p in range(n):
    plt.text(idx[p, 0], idx[p, 1], str(p), color='k', fontsize=14)

# network
p, q = np.where(weights != 0)[:]
for p_, q_ in zip(p, q):
    x_edge = [idx[p_, 0], idx[q_, 0]]
    y_edge = [idx[p_, 1], idx[q_, 1]]

    if weights[p_, q_] > 0:
        lw = 3
        c = 'b'
    else:
        lw = 0.5
        c = 'r'

    plt.plot(x_edge, y_edge, '-', lw=lw, color=c)
    plt.text(np.mean(x_edge), np.mean(y_edge), str(weights[p_, q_]), color=c)

plt.gca().axis('equal')

# all in-one function
vt.plot.network(idx[:, 0], idx[:, 1], weights)

plt.show()
