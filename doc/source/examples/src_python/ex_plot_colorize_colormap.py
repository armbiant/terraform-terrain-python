import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d,
                                   offset=0.3)
dw = vt.op.gradient_norm(w)

img_grey = vt.plot.colorize_bivariate(w, dw,
                                      vt.plot.colormap_bivariate('gray'))

img_dem = vt.plot.colorize_bivariate(w, dw, vt.plot.colormap_bivariate('dem'))

img_terrain = vt.plot.colorize_bivariate(w, dw,
                                         vt.plot.colormap_bivariate('terrain'))

# plot
plt.figure()
plt.imshow(img_grey)

plt.figure()
plt.imshow(img_dem)

plt.figure()
plt.imshow(img_terrain)

plt.show()
