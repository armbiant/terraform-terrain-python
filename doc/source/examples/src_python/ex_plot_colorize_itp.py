import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d,
                                   offset=0.3)
dw = vt.op.gradient_norm(w)

# --- bivariate (bis) -> the bivariate colormap can be define using
# --- interpolation

# colors
xyc = np.array([
    [0, 0, 46, 139, 87, 255],  # green
    [0.3, 0.3, 244, 164, 96, 255],  # brown
    [1, 0, 255, 255, 255, 255],  # white
    [0, 1, 47, 79, 79, 255],  # dark slate
    [1, 1, 0, 0, 0, 255],  # black
])
colors_grid = vt.plot.colors_to_regulargrid(xyc)

img = vt.plot.colorize_bivariate(w, dw, colors_grid)

# plot the colormap
plt.figure()
plt.imshow(colors_grid.transpose(1, 0, 2), origin='lower', extent=[0, 1, 0, 1])
plt.xlabel('parameter #1')
plt.ylabel('parameter #2')

# result
plt.figure()
plt.imshow(img)

plt.show()
