import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed=seed)
mask = np.where(w >= 0.8, 1, 0)

vt.plot.show(mask, cmap='gray', title='before')

mask = vt.op.fractalize_mask_contours(mask)

vt.plot.show(mask, cmap='gray', title='after')

plt.show()
