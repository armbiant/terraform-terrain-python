import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (25, 20)  # pdf shape
nsamples = 200000
seed = 1
extent = [-2, 2, -2, 2]

# generate 2D probability density function
x, y = np.meshgrid(np.linspace(extent[0], extent[1], shape[0]),
                   np.linspace(extent[2], extent[3], shape[1]),
                   indexing='ij')
pdf = np.exp(-((x - y + 0.5)**2 + y**2) / 2 / 0.5**2)

#
x, y = vt.grid.random_grid_pdf(n=nsamples, pdf=pdf, extent=extent, seed=seed)

# plots
vt.plot.show(pdf, title='input pdf')

h, _, _ = np.histogram2d(x, y, bins=(shape[0] - 1, shape[1] - 1))
vt.plot.show(h, title='generated samples')

plt.show()
