import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.worley2d(shape, res, seed=seed, cell_filling=True)

mask = vt.op.detect_border(w)

vt.plot.show(w, cmap='nipy_spectral')
vt.plot.show(mask, cmap='gray')

plt.show()
