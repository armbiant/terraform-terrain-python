import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w)

w = vt.op.harden(w, gain=1)
vt.plot.show(w, title='gain = 1')

w = vt.op.harden(w, gain=10)
vt.plot.show(w, title='gain = 10')

plt.show()
