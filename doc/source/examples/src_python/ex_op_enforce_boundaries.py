import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

ws = vt.op.enforce_boundaries(w)

vt.plot.topo(w, cmap=None)
vt.plot.topo(ws, cmap=None)

plt.show()
