import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

rng = np.random.default_rng(seed)

# random set of points
n = 500
x = 0.25 + 0.5 * rng.random(n)
y = 0.25 + 0.5 * rng.random(n)

values = vt.noise.perlin2d(shape, res, seed=seed)

w1 = vt.grid.xy_to_map(x, y, shape)
w2 = vt.grid.xy_to_map(x, y, shape, filling_value=values)

vt.plot.show(w1, cmap='gray', title='constant values', vmin=0, vmax=1)
vt.plot.show(w2, cmap='gray', title='Perlin-based values', vmin=0, vmax=1)

plt.show()
