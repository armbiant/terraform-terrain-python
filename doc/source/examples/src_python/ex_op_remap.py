import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(w, vmin=0, vmax=1)

# simply rescale the array values
w = vt.op.remap(w, vmin=0, vmax=0.5)
vt.plot.show(w, vmin=0, vmax=1, title='rescaled to [0, 0.5]')

plt.show()
