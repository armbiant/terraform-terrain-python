import matplotlib.pyplot as plt
import numpy as np
import scipy
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

z = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

x, y = np.meshgrid(np.linspace(-1, 1, z.shape[0]),
                   np.linspace(-1, 1, z.shape[1]),
                   indexing='ij')
z += np.exp(-(x**2 + y**2) / 0.3**2)

vt.plot.show(z, cmap='terrain', title='heightmap')

pmap, mask_label = vt.hydrology.pool_map(z, area_threshold=100)

vt.plot.show(pmap)

# plot center of mass of each pool
for v in np.unique(mask_label):
    xy = scipy.ndimage.center_of_mass(pmap, mask_label, v)
    z_pool = np.min(pmap[mask_label == v])

    print('center: {:.3f}, {:.3f}, elevation: {:.3f}'.format(
        xy[0], xy[1], z_pool))

    plt.plot(xy[0], xy[1], 'ko')

# overlay on heightmap
vt.plot.show(z, cmap='gray')
plt.imshow(pmap.T, origin='lower', cmap='Blues', alpha=1)

plt.show()
