import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# prepare heightmap
w = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                   np.linspace(-1, 1, shape[1]),
                   indexing='ij')
w += 3 * np.exp(-(x**2 + y**2) / 0.2**2 / 2)

wc = vt.hmap.dig_crater(w, 128, 128, radius=0.1, depth=0.5, seed=seed)

vt.plot.show(w, title='before')
vt.plot.show(wc, title='with crater')

plt.show()
