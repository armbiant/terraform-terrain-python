import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

z = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)

x, y = np.meshgrid(np.linspace(-1, 1, z.shape[0]),
                   np.linspace(-1, 1, z.shape[1]),
                   indexing='ij')
z += np.exp(-(x**2 + y**2) / 0.3**2)

vt.plot.show(z, cmap='terrain', title='heightmap')

# moisture/rain map: no rain on the left-half of the map
mask = np.zeros(shape)
mask[int(shape[0] / 2):, :] = 1

smap = vt.hydrology.stream_map(z, n_particles=10000, moisture_map=mask)

vt.plot.show(smap)

# overlay on heightmap
vt.plot.show(z, cmap='gray')

smap[smap == 0] = np.nan
plt.imshow(smap.T, origin='lower', cmap='jet')

plt.show()
