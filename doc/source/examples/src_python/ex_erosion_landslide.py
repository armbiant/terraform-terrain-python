import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# prepare heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.erosion.hydraulic_particles(w, radius=5e-3)
w0 = np.copy(w)

# landslide mask
mask = vt.noise.perlin2d(shape, (4, 4), seed=seed + 1)
mask = np.where((mask > 0.6) & (w < 0.8), 1.0, 0.0)

we = vt.erosion.landslide(w, mask)

# --- plots
vt.plot.show(mask, title='landslide(s) mask')

img = vt.plot.colorize_bivariate(we, vt.op.gradient_norm(we),
                                 vt.plot.colormap_bivariate('terrain'))
plt.figure()
plt.imshow(img)

# render
mesh = vt.mesh.hmap_to_trimesh(0.2 * we)
vt.mesh.set_color(mesh, [80, 80, 80, 255], mask)
vt.render.show(mesh, offscreen=True)

plt.show()
