import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
extent = [0, 1, 0, 1]

x, y = vt.grid.random_grid(n=100, seed=seed, extent=extent)

w = vt.grid.xy_to_filled_contours(x,
                                  y,
                                  radius=0.05,
                                  shape=shape,
                                  extent=extent)

# plot
vt.plot.show(w, cmap='gray', extent=extent)
plt.plot(x, y, 'b.', ms=2)

plt.show()
