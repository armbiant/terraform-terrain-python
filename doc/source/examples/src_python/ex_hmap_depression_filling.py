import matplotlib.pyplot as plt
import numpy as np
import scipy
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

z = vt.noise.fractal2d(shape, res, seed=seed, noise_fct=vt.noise.perlin2d)
vt.plot.topo(z, cmap=None)

z = vt.hmap.depression_filling(z)
vt.plot.topo(z, cmap=None)

plt.show()
