import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (128, 128)
res = (2, 2)
seed = 1

# initial heightmap

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w += 0.2 * vt.hmap.rocks(shape, radius=0.2, density=0.1, gamma=0.5, seed=seed)
w = vt.op.enforce_boundaries(w)

# flow source shape based on a smooth cosine kernel

ic = int(shape[0] / 2 - 1)
jc = int(shape[1] / 2 - 1)

k = vt.kernel.smooth_cosine((8, 8))
k /= np.sum(k)
h = 4 * vt.op.add_array(np.zeros(shape), ic, jc, k)

# flow simulation (can be a bit slow...)

w, h = vt.hmap.viscous_flow(w, h, gnu=0.01, iterations=30000, tol=1e-4)

# --- plots and render

vt.plot.show(h, title='flow thickness')
vt.plot.topo(w, title='heightmap')

mesh = vt.mesh.hmap_to_trimesh(0.2 * w)

vt.mesh.set_color(mesh,
                  color=[[30, 30, 40, 255], [207, 16, 32, 255]],
                  values=vt.op.remap(h))

vt.render.show(mesh, offscreen=True)

plt.show()
