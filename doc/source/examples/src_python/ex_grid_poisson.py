import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 128)
res = (4, 4)
seed = 1

extent = [0, 1, 0, 0.5]

# minimum radius
rmin = vt.noise.perlin2d(shape, res, seed)
rmin = vt.op.remap(rmin, 0.01, 0.2)

x, y = vt.grid.poisson_disk_sampler(rmin, nsearch=30, extent=extent, seed=seed)
vt.plot.show(rmin, extent=extent)
plt.plot(x, y, 'k.')

# plot distance circles
vt.plot.show(rmin, extent=extent)
plt.plot(x, y, 'k.')

theta = np.linspace(0, 2 * np.pi, 20)
xc = np.cos(theta)
yc = np.sin(theta)

for xi, yi in zip(x, y):
    i = int((xi - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1))
    j = int((yi - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1))
    plt.plot(xi + rmin[i, j] * xc, yi + rmin[i, j] * yc, 'k-', lw=0.5)

plt.show()
