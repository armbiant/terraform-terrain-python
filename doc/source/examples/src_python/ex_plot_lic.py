import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)

vt.plot.show(w, title='heightmap')

# plot gradient
dwx, dwy = vt.op.gradient(w)

vt.plot.blic(dwx, dwy, title='standard LIC')
vt.plot.blic(dwx, dwy, z=w, title='bumped LIC')

plt.show()
