import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1

# anisotropic
w = vt.noise.gabor2d(shape,
                     width=0.1,
                     f0=10,
                     w0=30 / 180 * np.pi,
                     seed=seed,
                     sparse=True)
vt.plot.show(w, cmap='gray', title='anisotropic Gabor noise')

# isotropic
# /!\ can be pretty slow...
w = vt.noise.gabor2d(shape,
                     width=0.1,
                     f0=8,
                     w0=None,
                     seed=seed,
                     sparse=True,
                     density=0.1)
vt.plot.show(w, cmap='gray', title='isotropic Gabor noise')

plt.show()
