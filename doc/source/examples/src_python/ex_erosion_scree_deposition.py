import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# prepare heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.op.enforce_boundaries(w)

talus = 0.4 * vt.op.gradient_talus(w).max()

# use altitude to define a rock dropping mask
mask = np.where((vt.op.remap(w) > 0.5), 1.0, 0.0)

ws, smap = vt.erosion.scree_deposition(w, talus=talus, drop_map=mask)

# plots
vt.plot.topo(w, title='before')
vt.plot.show(smap, title='scree thickness', cmap='nipy_spectral')
vt.plot.topo(ws, title='after')

plt.show()
