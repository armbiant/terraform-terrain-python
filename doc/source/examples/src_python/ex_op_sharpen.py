import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)

vt.plot.topo(w, cmap=None, title='original')

ws = vt.op.sharpen(w)
vt.plot.topo(ws, cmap=None, title='after sharpening')

ws = vt.op.sharpen(w, kernel=vt.kernel.cone((9, 9)))
vt.plot.topo(ws, cmap=None, title='after sharpening (cone kernel)')

plt.show()
