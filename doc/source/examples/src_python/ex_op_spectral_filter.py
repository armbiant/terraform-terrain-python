import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

# heightmap
w = vt.noise.diamond_square2d(shape, seed=seed)
w = vt.op.enforce_boundaries(w)

w_lp = vt.op.spectral_filter(w, kc=0.05)  # low pass
w_hp = vt.op.spectral_filter(w, kc=0.05, filtering='hp')  # high pass
w_bp = vt.op.spectral_filter(w, kc=[0.1, 0.2])  # band pass

vt.plot.show(w, title='original')
vt.plot.show(w_lp, title='low pass')
vt.plot.show(w_hp, title='high pass')
vt.plot.show(w_bp, title='band pass')

plt.show()
