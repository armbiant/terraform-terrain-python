import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

# --- heightmap
w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d,
                                   offset=0.3)
vt.plot.show(w)

# --- "standard" (univariate) colorize

# set a solid color
img0 = vt.plot.colorize(np.where(w > 0.5, 1, 0), [0, 255, 0, 255])

# set a solid and change alpha
img1 = vt.plot.colorize(np.where(w < 0.6, 1, 0), [255, 0, 0, 255],
                        alpha=0.5 * vt.op.remap(w))

# apply a colormap to all the map
img2 = vt.plot.colorize(w, 'nipy_spectral', desactivate_mask=True, alpha=0.8)

plt.figure()
plt.imshow(img2)
plt.imshow(img0)
plt.imshow(img1)

plt.show()
