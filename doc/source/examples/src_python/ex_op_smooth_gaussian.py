import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (4, 4)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
w += 0.2 * vt.noise.white2d(shape, seed)

vt.plot.show(w, cmap='gray', title='unfiltered')

w = vt.op.smooth_gaussian(w, sigma_radius=0.05)
vt.plot.show(w, cmap='gray', title='filtered')

plt.show()
