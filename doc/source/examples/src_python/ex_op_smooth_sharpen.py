import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.op.enforce_boundaries(w)

vt.plot.topo(w, title='original')

ws, smap = vt.op.smooth_sharpen(w, radius=0.1)

vt.plot.topo(ws, title='after sharpening')
vt.plot.show(smap, title='depot')

plt.show()
