import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.perlin2d(shape, res, seed)
vt.plot.show(vt.op.steps(w, 10))

plt.show()
