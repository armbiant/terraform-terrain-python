import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   seed=seed,
                                   noise_fct=vt.noise.perlin2d)
w = vt.op.enforce_boundaries(w)

vt.plot.topo(w, title='initial heightmap')

# define talus limit based on the maximum talus value, to avoid any
# grid dependence
talus = 0.2 * vt.op.gradient_talus(w).max()

# thermal erosion
wt, smap = vt.erosion.thermal(w, talus=talus, auto_bedrock=False)
vt.plot.topo(wt, title='thermal erosion (basic)')

# smooth landing option
wt, smap = vt.erosion.thermal(w,
                              talus=talus,
                              auto_bedrock=True,
                              smooth_landing=True)
vt.plot.topo(wt, title='thermal erosion (auto bedrock + smooth landing)')

plt.show()
