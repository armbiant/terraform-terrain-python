import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
w = vt.op.enforce_boundaries(w)

wb = vt.hmap.basalt_collonade(w, radius=0.05, noise=0.3, seed=-1)

vt.plot.topo(w, cmap=None)
vt.plot.topo(wb, cmap=None)

plt.show()
