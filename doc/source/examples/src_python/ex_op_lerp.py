import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
res1 = (4, 4)
res2 = (8, 8)
rest = (1, 1)
seed = 1

w1 = vt.noise.perlin2d(shape, res1, seed)
w2 = vt.noise.perlin2d(shape, res2, seed + 1)
t = vt.noise.perlin2d(shape, rest, seed + 2)

vt.plot.show(np.dstack((w1, w2, t)))

# linearly mix noise #1 and #2 based on 't'
w = vt.op.lerp(w1, w2, t)
vt.plot.show(w, title='mix')

plt.show()
