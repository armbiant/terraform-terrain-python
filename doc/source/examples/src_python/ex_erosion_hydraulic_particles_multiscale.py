import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1
res = (2, 2)

w = vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed)
vt.plot.topo(w, title='before erosion', cmap=None)

w = vt.erosion.hydraulic_particles_multiscale(w)
vt.plot.topo(w, title='default classes', cmap=None)

# user-defined classes
p_classes = {
    'large': {
        'radius': 5e-3,
        'p_inertia': 0.8,
        'p_capacity': 1,
        'particle_density': 0.1,
    },
    'small': {
        'radius': None,
        'p_capacity': 1,
        'particle_density': 0.1,
    }
}

w = vt.erosion.hydraulic_particles_multiscale(w, p_classes)
vt.plot.topo(w, title='user-defined classes', cmap=None)

plt.show()
