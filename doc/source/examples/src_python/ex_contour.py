import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
seed = 1

rng = np.random.default_rng(seed)

# random set of points
n = 10
x = 0.25 + 0.5 * rng.random(n)
y = 0.25 + 0.5 * rng.random(n)

# build a "path" (ordered points) using a nearest neighbor algorithm
x, y = vt.contour.nearest_neighbor_search_2d(x, y)

# close the contour
x = np.append(x, x[0])
y = np.append(y, y[0])

# fractalize
xf, yf = vt.contour.fractalize_2d(x, y, iterations=8, sigma=0.2)

# convert contour to a 0/1 mask
w = vt.contour.xy_to_map(xf, yf, shape=shape, filled=True)

plt.figure()
plt.plot(x, y, 'ko')
plt.plot(xf, yf, 'b-', lw=0.5)
plt.gca().axis('equal')

vt.plot.show(w, cmap='gray')

plt.show()
