Examples
========

Procedural texture
------------------

Primitives
^^^^^^^^^^

.. toctree::
    :glob:

    ex_noise*
    
Fractal
^^^^^^^

.. toctree::
    :glob:

    ex_fractal*
    
Heightmaps
----------

Generation and alteration
^^^^^^^^^^^^^^^^^^^^^^^^^

.. toctree::
    :glob:

    ex_hmap_*

Erosion
^^^^^^^

.. toctree::
    :glob:

    ex_erosion_*

Hydrology
^^^^^^^^^
   
.. toctree::
    :glob:

    ex_hydrology_*
   
Climate
^^^^^^^

.. toctree::
    :glob:

    ex_climate_*

Flora
^^^^^

.. toctree::
   :glob:

   ex_flora_*

Anthropic features
^^^^^^^^^^^^^^^^^^

.. toctree::
    :glob:

    ex_roads_*

Mesh and grid
-------------

.. toctree::
    :glob:

    ex_grid*

Contour
-------

.. toctree::
    :glob:

    ex_contour*
    
Mask
----

.. toctree::
    :glob:

    ex_mask*
    
Plots
-----

.. toctree::
    :glob:

    ex_plot_*
    
Rendering
---------

.. toctree::
    :glob:

    ex_render_*
    
Operators
---------

.. toctree::
    :glob:

    ex_op_base.rst
    
    
    
 
