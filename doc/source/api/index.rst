API Documentation
=================

Noise primitives (``vterrain.noise``)
-------------------------------------
.. automodapi:: vterrain.noise
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
   
Heightmaps (``vterrain.hmap``)
------------------------------
.. automodapi:: vterrain.hmap
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
     
Erosion (``vterrain.erosion``)
------------------------------
.. automodapi:: vterrain.erosion
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
      
Hydrology (``vterrain.hydrology``)
----------------------------------
.. automodapi:: vterrain.hydrology
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Climate (``vterrain.climate``)
------------------------------
.. automodapi:: vterrain.climate
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Flora (``vterrain.flora``)
--------------------------
.. automodapi:: vterrain.flora
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Roads (``vterrain.roads``)
------------------------------
.. automodapi:: vterrain.roads
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
   
Operators (``vterrain.op``)
---------------------------
.. automodapi:: vterrain.op
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Kernels (``vterrain.kernel``)
-----------------------------
.. automodapi:: vterrain.kernel
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Grids (``vterrain.grid``)
-------------------------
.. automodapi:: vterrain.grid
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:      

Mesh (``vterrain.mesh``)
------------------------
.. automodapi:: vterrain.mesh
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Contour (``vterrain.contour``)
------------------------------
.. automodapi:: vterrain.contour
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:      

Mask (``vterrain.mask``)
------------------------
.. automodapi:: vterrain.mask
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading: 
      
Plots (``vterrain.plot``)
-------------------------
.. automodapi:: vterrain.plot
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
      
Rendering (``vterrain.render``)
-------------------------------
.. automodapi:: vterrain.render
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:

Files I/O (``vterrain.fio``)
----------------------------
.. automodapi:: vterrain.fio
   :no-inheritance-diagram:
   :no-main-docstr:
   :no-heading:
