Tree spawning
=============

Parametrization
---------------

.. plot:: user_guide/flora/src_python/ug_flora_spawning_method.py
   :width: 80%
   :align: center

References
----------

- Gasch, C., Sotoca, J.M., Chover, M., Remolar, I., and
  Rebollo, C. 2022. Procedural modeling of plant ecosystems maximizing
  vegetation cover. Multimedia Tools and Applications 81, 12,
  16195–16217.

  
