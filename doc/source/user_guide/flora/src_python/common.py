import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1


def init_hmap():
    w = vt.noise.hybrid_multifractal2d(shape,
                                       res,
                                       seed=seed,
                                       noise_fct=vt.noise.perlin2d)
    w = vt.op.enforce_boundaries(w)
    return w
