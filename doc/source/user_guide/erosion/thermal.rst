Thermal erosion
===============

Model
-----



Parametrization
---------------


.. plot:: user_guide/erosion/src_python/ug_thermal_cone.py
   :width: 80%
   :align: center

.. plot:: user_guide/erosion/src_python/ug_thermal_talus.py
   :width: 80%
   :align: center

	   
References
----------
