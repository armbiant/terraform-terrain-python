Hydraulic erosion model (particle-based)
========================================

Model
-----

Particle motion
~~~~~~~~~~~~~~~

Rate of erosion, transportation, and deposition
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

- based on `Hjulström curve
  <https://en.wikipedia.org/wiki/Hjulstr%C3%B6m_curve/>`_

.. figure:: https://upload.wikimedia.org/wikipedia/commons/6/60/Hjulstr%C3%B6ms_diagram_en.PNG
   :figwidth: 60%
   :align: center
   :target: https://commons.wikimedia.org/wiki/File:Hjulstr%C3%B6ms_diagram_en.PNG
	 
   Hjulströms_diagram_sv.PNG: Karrockderivative work: Karrock, CC
   BY-SA 3.0, via Wikimedia Commons

Parametrization
---------------
   
Particle size
~~~~~~~~~~~~~

Pre-defined particle sizes:

- "small": highly erosive, no deposition, closely follows the landscape details,
  
- "medium": average particle,
  
- "large": large inertia (tends to go in a straight line) and a large
  foot-print.

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_size.py
   :width: 80%
   :align: center

Particle size and density (i.e. number of particles)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_influence_size_density.py
   :width: 80%
   :align: center
	   
Sediment capacity
~~~~~~~~~~~~~~~~~

Sediment capacity of the flow stream has a strong influence on the
overall erosion amplitude:

- small capacity means that the flow stream can only carry few
  sediments. Once full capacity has been reached, there cannot be any
  erosion,

- procedure tends to be unstable for large values of the sediment
  capacity (e.g. above 10 for 'medium' particles).

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_influence_capacity.py
   :width: 80%
   :align: center

Erosion and deposition coefficients
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Coefficient influence is strongly related to the sediment capacity.

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_influence_erosion_deposition.py
   :width: 80%
   :align: center
	   
Gravity
~~~~~~~

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_gravity.py
   :width: 80%
   :align: center
      
Bedrock
~~~~~~~

.. plot:: user_guide/erosion/src_python/ug_hydraulic_particle_bedrock.py
   :width: 80%
   :align: center

References
----------

1. Hjulström, F. 1935. Studies of the morphological activity of rivers
   as illustrated by the river fyris. Almqvist & Wiksells.

