import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap()

for radius in ['small', 'medium', 'large']:
    we = vt.erosion.hydraulic_particles(w, radius=radius)
    vt.plot.topo(we, title='radius: {}'.format(radius))

plt.show()
