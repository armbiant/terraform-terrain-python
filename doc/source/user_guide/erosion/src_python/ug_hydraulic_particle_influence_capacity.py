import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap()

radius = 'medium'

func = lambda p_erosion, p_capacity: vt.erosion.hydraulic_particles(
    w, radius=None, p_erosion=p_erosion, p_capacity=p_capacity)

# how the parameters vary
args_dict = {
    'p_erosion': [0.001, 0.01],
    'p_capacity': [0.1, 1, 5],
}

vt.plot.show_influence_matrix(func=func, args_dict=args_dict)

plt.show()
