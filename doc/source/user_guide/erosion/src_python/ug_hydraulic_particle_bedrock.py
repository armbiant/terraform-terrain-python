import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap(rocks=True)

we = vt.erosion.hydraulic_particles(w,
                                    z_bedrock=wr,
                                    radius='small',
                                    particle_density=0.5)
vt.plot.topo(we)

plt.show()
