import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap()

for gravity in [0.5, 1, 2]:
    we = vt.erosion.hydraulic_particles(w, radius='medium', gravity=gravity)
    vt.plot.topo(we, title='gravity: {}'.format(gravity))

plt.show()
