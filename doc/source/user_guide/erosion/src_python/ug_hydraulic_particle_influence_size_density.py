import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap()

func = lambda radius, particle_density: vt.erosion.hydraulic_particles(
    w, radius=radius, particle_density=particle_density)

# how the parameters vary
args_dict = {
    'radius': ['small', 'medium', 'large'],
    'particle_density': [0.2, 0.5, 1],
}

vt.plot.show_influence_matrix(func=func, args_dict=args_dict)

plt.show()
