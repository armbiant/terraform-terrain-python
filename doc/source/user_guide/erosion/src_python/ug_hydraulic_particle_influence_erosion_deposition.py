import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, wr = common.init_hmap()

radius = 'medium'

func = lambda p_erosion, p_deposition: vt.erosion.hydraulic_particles(
    w, radius='medium', p_erosion=p_erosion, p_deposition=p_deposition)

# how the parameters vary
args_dict = {
    'p_erosion': [0.001, 0.05],
    'p_deposition': [0.001, 0.05],
}

vt.plot.show_influence_matrix(func=func, args_dict=args_dict)

plt.show()
