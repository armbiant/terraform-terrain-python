import vterrain as vt

shape = (256, 256)
res = (2, 2)
seed = 1


def init_hmap(rocks=False):
    w = vt.noise.hybrid_multifractal2d(shape,
                                       res,
                                       seed=seed,
                                       noise_fct=vt.noise.perlin2d)
    w = vt.op.enforce_boundaries(w)

    wr = vt.op.steepen(w, scale=0.2, elevation_scaling=1)
    wr = vt.op.remap(wr, w.min() - 0.1 * w.ptp(), w.max())

    if rocks:
        wr += 0.2 * vt.hmap.rocks(w.shape, radius=0.1, density=0.3, gamma=0.5)

    return w, wr
