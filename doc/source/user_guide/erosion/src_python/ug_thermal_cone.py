import matplotlib.pyplot as plt
import numpy as np
#
import vterrain as vt

shape = (256, 256)
ic = int(shape[0] / 2 - 1)
jc = int(shape[1] / 2 - 1)

w = np.zeros(shape)
nc = 64
vt.op.add_array(w, ic, jc, vt.kernel.cone((nc, nc)))
w /= w.max()

talus = 0.2 * vt.op.gradient_talus(w).max()
we0, _ = vt.erosion.thermal(w, talus=talus, auto_bedrock=False)
we1, _ = vt.erosion.thermal(w, talus=talus, auto_bedrock=True)
we2, _ = vt.erosion.thermal(w,
                            talus=talus,
                            auto_bedrock=True,
                            smooth_landing=True)

vt.plot.diff(w, we0)
vt.plot.diff(w, we1)

plt.figure()
t = np.linspace(0, 1, shape[0])
plt.plot(t, w[:, jc], 'b-')
plt.plot(t, we0[:, jc], 'k-')
plt.plot(t, we1[:, jc], 'g--')
plt.plot(t, we2[:, jc], 'm--')
plt.gca().set_aspect('equal')

plt.show()
