import matplotlib.pyplot as plt
import numpy as np
#
import common
import vterrain as vt

w, _ = common.init_hmap()

for c in [0.2, 0.5]:
    talus = c * vt.op.gradient_talus(w).max()
    we, _ = vt.erosion.thermal(w, talus=talus, smooth_landing=True)
    vt.plot.topo(we, title='talus = {:.1f} * talus_max '.format(c))

plt.show()
