# Configuration file for the Sphinx documentation builder.
import sys
import os

# If extensions (or modules to document with autodoc) are in another
# directory, add these directories to sys.path here. If the directory
# is relative to the documentation root, use os.path.abspath to make
# it absolute, like shown here.
# sys.path.insert(0, os.path.abspath('../..'))

# -- Project information -----------------------------------------------------

project = u'tools'
copyright = u'2022, Otto Link'
author = u'Otto Link'

# -- General configuration ---------------------------------------------------

extensions = [
    'sphinx.ext.autodoc', 'sphinx.ext.autosummary',
    'sphinx_automodapi.automodapi', 'sphinx_exec_code',
    'matplotlib.sphinxext.plot_directive', 'sphinx.ext.viewcode'
]

autosummary_generate = True
automodapi_toctreedirnm = 'generated'

templates_path = ['_templates']
exclude_patterns = []

# The suffix(es) of source filenames.  You can specify multiple suffix
# as a list of string: source_suffix = ['.rst', '.md']
source_suffix = '.rst'

# The master toctree document
master_doc = 'index'

# -- Options for HTML output -------------------------------------------------

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
