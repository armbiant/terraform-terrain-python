import numpy as np
import os
import unittest
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TestOp(unittest.TestCase):

    def test_poisson_disk_sampler(self):
        shape = (4, 16)
        res = (1, 2)
        seed = 1

        w = vt.noise.perlin2d(shape=shape, res=res, seed=seed)
        w = vt.op.remap(w, vmin=2, vmax=10)

        x, y = vt.grid.poisson_disk_sampler(rmin=w, nsearch=30, seed=seed)

        xref = np.array([0.44096037, 0.60845785, 1.99365264, 1.00468121],
                        dtype=np.float32)
        yref = np.array([0.32237116, 6.98814296, 9.61027861, 14.01105683],
                        dtype=np.float32)

        np.testing.assert_array_almost_equal(x, xref, decimal=5)
        np.testing.assert_array_almost_equal(y, yref, decimal=5)
