import numpy as np
import os
import unittest
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TestOp(unittest.TestCase):

    def test_kernel_gaussian(self):
        shape = (6, 4)
        k = vt.kernel.gaussian(shape)

        k_ref = np.array([[0.00063812, 0.00914899, 0.00914899, 0.00063812],
                          [0.00434072, 0.06223469, 0.06223469, 0.00434072],
                          [0.01132116, 0.16231631, 0.16231631, 0.01132116],
                          [0.01132116, 0.16231631, 0.16231631, 0.01132116],
                          [0.00434072, 0.06223469, 0.06223469, 0.00434072],
                          [0.00063812, 0.00914899, 0.00914899, 0.00063812]],
                         dtype=np.float32)

        np.testing.assert_array_almost_equal(k, k_ref, decimal=5)

    def test_kernel_smooth_cosine(self):
        shape = (6, 4)
        k = vt.kernel.smooth_cosine(shape)

        k_ref = np.array([[0., 0., 0., 0.], [0., 0.06248688, 0.06248688, 0.],
                          [0., 0.18751312, 0.18751312, 0.],
                          [0., 0.18751312, 0.18751312, 0.],
                          [0., 0.06248688, 0.06248688, 0.], [0., 0., 0., 0.]],
                         dtype=np.float32)

        np.testing.assert_array_almost_equal(k, k_ref, decimal=5)

    def test_kernel_smooth_square(self):
        shape = (6, 4)
        k = vt.kernel.smooth_square(shape)

        k_ref = np.array([[0., 0., 0., 0.], [0., 0.06011647, 0.06011647, 0.],
                          [0., 0.18988353, 0.18988353, 0.],
                          [0., 0.18988353, 0.18988353, 0.],
                          [0., 0.06011647, 0.06011647, 0.], [0., 0., 0., 0.]],
                         dtype=np.float32)

        np.testing.assert_array_almost_equal(k, k_ref, decimal=5)
