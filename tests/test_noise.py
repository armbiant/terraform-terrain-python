import numpy as np
import os
import unittest
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TestNoise(unittest.TestCase):

    def test_diamond_square2d(self):
        shape = (8, 4)
        seed = 1

        w = vt.noise.diamond_square2d(shape, seed=seed, persistence=0.5)

        wref = np.asarray([[0.89930467, 1., 0.97705448, 0.80618052],
                           [0.66332016, 0.81165665, 0.90003233, 0.48689724],
                           [0.95675849, 0.60323266, 0.46310701, 0.18928525],
                           [0.88523892, 0.44487064, 0.45806503, 0.09038425],
                           [0.58358307, 0.63175052, 0.63990479, 0.46760542],
                           [0.55962804, 0.40810773, 0.19151189, 0.4848775],
                           [0., 0.3453616, 0.0939881, 0.34660604],
                           [0.28709677, 0.18293221, 0.29794133, 0.30186122]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_fault2d(self):
        shape = (8, 4)
        seed = 1

        w = vt.noise.fault2d(shape=shape,
                             seed=seed,
                             displacement=0.1,
                             maxit=100)

        wref = np.asarray(
            [[0.3, 0.2, 0.2, 0.2], [0.3, 0.2, 0.2, 0.5], [0.3, 0.3, 0.3, 0.],
             [0.3, 0.5, 0.3, 0.3], [0.3, 0.9, 0.4, 0.5], [0.3, 0.6, 1., 0.4],
             [0.3, 0.7, 0.6, 0.2], [0.3, 0.5, 0.2, 0.2]],
            dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

        x = np.linspace(0, shape[0] / max(shape), shape[0])
        y = np.linspace(0, shape[1] / max(shape), shape[1])
        w = vt.noise.fault2d_xy(x=x,
                                y=y,
                                seed=seed,
                                displacement=0.1,
                                maxit=100)
        w = vt.op.remap(w, vmin=0, vmax=1)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_fractal2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.fractal2d(shape=shape,
                               res=res,
                               seed=seed,
                               noise_fct=vt.noise.perlin2d)

        wref = np.asarray([[0.56335521, 0.9338058, 0.06288184, 0.56335521],
                           [0.03958936, 0.50520475, 0.08601792, 0.31662432],
                           [0.08340946, 0.53951643, 0., 0.52771039],
                           [0.41329674, 0.93742252, 0.36736932, 0.55713814],
                           [0.84534989, 1., 0.25515461, 0.3934599],
                           [0.74319142, 0.86974399, 0.63674393, 0.72241808],
                           [0.26607362, 0.75755059, 0.86906429, 0.97099469],
                           [0.56335521, 0.14435886, 0.91509089, 0.56335521]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_fractal2d_xy(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        x = np.linspace(0, shape[0] / max(shape), shape[0])
        y = np.linspace(0, shape[1] / max(shape), shape[1])
        w = vt.noise.fractal2d_xy(x=x,
                                  y=y,
                                  res=res,
                                  seed=seed,
                                  noise_fct_xy=vt.noise.perlin2d_xy)
        w = vt.op.remap(w, vmin=0, vmax=1)

        wref = np.asarray([[0.57502552, 0.94243761, 0.1030343, 0.57502552],
                           [0.06280601, 0.51687212, 0.08604662, 0.35175354],
                           [0.09095025, 0.51559809, 0., 0.53255894],
                           [0.42647207, 0.92095119, 0.36457153, 0.56152623],
                           [0.84110509, 1., 0.28592399, 0.43237167],
                           [0.73588803, 0.89176098, 0.65914358, 0.756576],
                           [0.30095652, 0.75645477, 0.90135284, 0.97004226],
                           [0.57502552, 0.18724653, 0.9136484, 0.57502552]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_gabor2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.gabor2d(shape,
                             width=0.1,
                             f0=1,
                             w0=30 / 180 * np.pi,
                             seed=seed)

        wref = np.asarray([[0.68594619, 0.37930183, 0.99412465, 0.50550553],
                           [0.24000673, 0.42155771, 0.74614122, 0.34378562],
                           [0.05289494, 0.72026871, 0.41987526, 0.44324298],
                           [0.32940429, 0.23291743, 0.0477099, 0.],
                           [0.41854749, 0.00922515, 0.33253054, 0.58674485],
                           [0.85025895, 0.60337524, 0.84826951, 0.8248942],
                           [0.70084421, 1., 0.19397553, 0.36181431],
                           [0.93142659, 0.91950547, 0.01288616, 0.45270313]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

        w = vt.noise.gabor2d(shape, width=0.1, f0=1, w0=None, seed=seed)

        wref = np.asarray([[0.60667449, 0.69811981, 0.90597993, 0.28584051],
                           [0.30709767, 0.59469135, 0.71345258, 0.28986386],
                           [0.37571918, 0.78053853, 0.48906774, 0.25467646],
                           [0.5698673, 0.42501695, 0.38980362, 0.13341756],
                           [0.38925543, 0.38949882, 0.60548354, 0.26450579],
                           [0.91918741, 0.45426773, 0.64629014, 0.43099214],
                           [0.7514373, 1., 0.54045957, 0.02324556],
                           [0.43502852, 0.32173673, 0.08188248, 0.]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_hybrid_multifractal2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.hybrid_multifractal2d(shape=shape,
                                           res=res,
                                           seed=seed,
                                           noise_fct=vt.noise.perlin2d)

        wref = np.asarray([[0.5751844, 0.9012143, 0.08799403, 0.5751844],
                           [0., 0.52305525, 0.11533396, 0.29458829],
                           [0.15949229, 0.66759039, 0.05995008, 0.53107417],
                           [0.42141941, 0.98235966, 0.38605974, 0.58655579],
                           [0.87132398, 1., 0.20523766, 0.41825607],
                           [0.77339504, 0.87263507, 0.56136299, 0.69109633],
                           [0.1777687, 0.77907222, 0.84551703, 0.98237346],
                           [0.5751844, 0.12029909, 0.92792527, 0.5751844]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_hybrid_multifractal2d_xy(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        x = np.linspace(0, shape[0] / max(shape), shape[0])
        y = np.linspace(0, shape[1] / max(shape), shape[1])
        w = vt.noise.hybrid_multifractal2d_xy(
            x=x, y=y, res=res, seed=seed, noise_fct_xy=vt.noise.perlin2d_xy)
        w = vt.op.remap(w, vmin=0, vmax=1)

        wref = np.asarray([[0.69074632, 0.94070233, 0.25619838, 0.69074632],
                           [0.16444342, 0.65067169, 0.03922953, 0.46599113],
                           [0.13629045, 0.62750008, 0., 0.55596722],
                           [0.49412896, 0.93872789, 0.36052683, 0.67152292],
                           [0.88616822, 1., 0.42312296, 0.6249423],
                           [0.81098108, 0.94360103, 0.71398894, 0.82152163],
                           [0.3701905, 0.79851817, 0.92607726, 0.9848647],
                           [0.69074632, 0.3387103, 0.93205782, 0.69074632]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_perlin2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.perlin2d(shape=shape, res=res, seed=seed)

        wref = np.asarray([[0.46897165, 0.31277425, 0.67100672, 0.46897165],
                           [0.05359169, 0.64975227, 0.97871466, 0.3679031],
                           [0., 0.98185914, 1., 0.36184251],
                           [0.354654, 0.8929213, 0.62751621, 0.44618377],
                           [0.53671522, 0.6781292, 0.36556485, 0.464518],
                           [0.44695005, 0.61693673, 0.32360774, 0.28891507],
                           [0.31359081, 0.62980991, 0.40609182, 0.23619784],
                           [0.46897165, 0.48372875, 0.2918948, 0.46897165]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

        x = np.linspace(0, shape[0] / max(shape), shape[0])
        y = np.linspace(0, shape[1] / max(shape), shape[1])
        w = vt.noise.perlin2d_xy(x=x, y=y, res=res, seed=seed)
        w = vt.op.remap(w, vmin=0, vmax=1)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_ridge2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.ridge2d(shape=shape,
                             res=res,
                             seed=seed,
                             noise_fct=vt.noise.perlin2d)
        wref = np.asarray([[0.93794331, 0.6255485, 0.65798657, 0.93794331],
                           [0.10718338, 0.70049547, 0.04257069, 0.7358062],
                           [0., 0.03628172, 0., 0.72368502],
                           [0.70930801, 0.21415741, 0.74496758, 0.89236753],
                           [0.92656956, 0.6437416, 0.73112971, 0.929036],
                           [0.89390009, 0.76612653, 0.64721547, 0.57783015],
                           [0.62718163, 0.74038018, 0.81218365, 0.47239568],
                           [0.93794331, 0.96745749, 0.5837896, 0.93794331]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_spot2d(self):
        shape = (8, 4)
        seed = 1

        w = vt.noise.spot2d(shape=shape, seed=seed, kernel=np.ones([2, 3]))

        wref = np.asarray([[0.80917399, 0.64206827, 0.87333841, 0.63516666],
                           [0.248251, 0.52267479, 0.55987112, 0.7376067],
                           [0.43324324, 0.46141269, 0.51003776, 0.53745861],
                           [0.29651127, 0.20595878, 0.31507393, 0.],
                           [0.48693726, 0.24691808, 0.40218649, 0.48915581],
                           [0.99081357, 0.89071519, 0.69399387, 1.],
                           [0.65959158, 0.94729139, 0.63144507, 0.38363232],
                           [0.85337025, 0.67834167, 0.84806881, 0.30780669]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_value2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.value2d(shape=shape, res=res, seed=seed)

        wref = np.asarray([[0.47407556, 0.6813082, 0.57148148, 0.51149578],
                           [0.06779004, 0.37042949, 0.10519177, 0.40066639],
                           [0., 0.26168643, 0.37532508, 0.36601728],
                           [0.10411691, 0.32250018, 0.84574243, 0.4287257],
                           [0.21675446, 0.51584391, 1., 0.60624414],
                           [0.24817745, 0.70238707, 0.77464244, 0.83035508],
                           [0.18230177, 0.64049535, 0.55920292, 0.94717101],
                           [0.00624554, 0.08408646, 0.76290974, 0.79907964]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)

    def test_worley2d(self):
        shape = (8, 4)
        res = (2, 4)
        seed = 1

        w = vt.noise.worley2d(shape=shape, res=res, seed=seed)

        wref = np.asarray([[0.59881436, 0.52080698, 0.05508677, 0.48404562],
                           [0.02021932, 0.50979883, 0.04407862, 0.47303747],
                           [0.09893951, 0.13791361, 0.37604751, 1.],
                           [0.54631221, 0.02799125, 0.26612515, 0.5102526],
                           [0.24619613, 0.57538412, 0.81351802, 0.18830109],
                           [0.4567207, 0.18936044, 0.52152067, 0.52366481],
                           [0.26736026, 0., 0.06219201, 0.89273652],
                           [0.25066694, 0.32431314, 0.26017859, 0.62747767]],
                          dtype=np.float32)

        np.testing.assert_array_almost_equal(w, wref, decimal=5)
