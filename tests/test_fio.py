import numpy as np
import os
import shutil
import tempfile
import unittest
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TestIo(unittest.TestCase):

    def test_save_load(self):
        shape = (4, 4)
        w = np.zeros(shape, dtype=np.float32)
        w[2, 3] = 1

        dpath = tempfile.mkdtemp()

        for extension in ['.pgm', '.raw', '.png']:
            fname = os.path.join(dpath, 'tmp' + extension)
            vt.fio.write(w, fname)
            wload = vt.op.remap(vt.fio.load(fname), vmin=0, vmax=1)
            np.testing.assert_array_almost_equal(w, wload, decimal=5)

        shutil.rmtree(dpath)
