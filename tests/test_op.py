import numpy as np
import os
import unittest
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TestOp(unittest.TestCase):

    def test_clamp(self):
        x = np.array([[-1, 0, 2], [np.nan, 0, 1]], dtype=np.float32)
        xref = np.array([[0, 0, 1], [np.nan, 0, 1]], dtype=np.float32)
        x = vt.op.clamp(x, vmin=0, vmax=1)

        np.testing.assert_array_equal(x, xref)

    def test_dijsktra(self):
        shape = (16, 8)
        res = (2, 4)
        seed = 1

        w = vt.noise.perlin2d(shape=shape, res=res, seed=seed)
        path = vt.op.dijkstra(w,
                              s=(0, 0),
                              e=(shape[0] - 1, shape[1] - 1),
                              distance_exponent=0.5)

        path_ref = np.array(
            [[1, 1], [2, 2], [3, 2], [4, 2], [5, 2], [6, 2], [7, 2], [8, 2],
             [9, 2], [10, 3], [11, 4], [12, 5], [13, 6], [14, 6], [15, 7]],
            dtype=int)
        np.testing.assert_array_equal(path, path_ref)

    def test_gradient(self):
        z = np.array([[1, 2, 3], [-1, -2, -3]])
        x, y = np.meshgrid(np.linspace(0, 2, z.shape[0]),
                           np.linspace(0, 4, z.shape[1]),
                           indexing='ij')

        # without underlying mesh
        dzx, dzy = vt.op.gradient(z)

        dzx_ref = np.array([[-2, -4, -6], [-2, -4, -6]], dtype=np.float32)
        dzy_ref = np.array([[1, 1, 1], [-1, -1, -1]], dtype=np.float32)

        np.testing.assert_array_equal(dzx, dzx_ref)
        np.testing.assert_array_equal(dzy, dzy_ref)

        # with underlying mesh
        dzx, dzy = vt.op.gradient(z, x, y)

        dzx_ref = np.array([[-1, -2, -3], [-1, -2, -3]], dtype=np.float32)
        dzy_ref = np.array([[0.5, 0.5, 0.5], [-0.5, -0.5, -0.5]],
                           dtype=np.float32)

        np.testing.assert_array_equal(dzx, dzx_ref)
        np.testing.assert_array_equal(dzy, dzy_ref)

        # norm
        dzn = vt.op.gradient_norm(z)

        dzn_ref = np.array(
            [[2.236068, 4.123106, 6.082763], [2.236068, 4.123106, 6.082763]],
            dtype=np.float32)

        np.testing.assert_array_almost_equal(dzn, dzn_ref, decimal=5)

        # angle
        dza = vt.op.gradient_angle(z)

        dza_ref = np.array([[2.67794504, 2.89661399, 2.97644398],
                            [-2.67794504, -2.89661399, -2.97644398]],
                           dtype=np.float32)

        np.testing.assert_array_almost_equal(dza, dza_ref, decimal=5)

    def test_local_maxima(self):
        shape = (101, 101)
        x, y = np.meshgrid(np.linspace(0, 1, shape[0]),
                           np.linspace(0, 1, shape[1]),
                           indexing='ij')
        z = np.exp(-((x-0.1)**2+(y-0.2)**2)/0.1**2) \
            + 0.5*np.exp(-((x-0.6)**2+(y-0.8)**2)/0.1**2)

        idx = vt.op.local_maxima(z)
        idx_ref = np.array([[10, 20], [60, 80]], dtype=int)
        np.testing.assert_array_equal(idx, idx_ref)

    def test_lerp(self):
        x1 = np.array([[0, 0, 0], [0, 0, 0]], dtype=np.float32)
        x2 = np.array([[1, 2, 3], [-1, -2, -3]], dtype=np.float32)
        t = np.array([[0.5, 0, 1], [0.5, 1, 0]], dtype=np.float32)
        xref = np.array([[0.5, 0, 3], [-0.5, -2, 0]], dtype=np.float32)
        x = vt.op.lerp(x1, x2, t)

        np.testing.assert_array_equal(x, xref)

    def test_remap(self):
        x = np.array([[-1, 0, 3], [np.nan, 0, 1]], dtype=np.float32)
        xref = np.array([[0, 0.25, 1], [np.nan, 0.25, 0.5]], dtype=np.float32)
        x = vt.op.remap(x, vmin=0, vmax=1)

        np.testing.assert_array_equal(x, xref)

    def test_harden(self):
        x = np.array([[-1, 0, 2], [np.nan, 0, 1]], dtype=np.float32)
        xref = np.array([[-1, -1, 2], [np.nan, -1, 2]], dtype=np.float32)
        x = vt.op.harden(x, gain=1e5)

        np.testing.assert_array_equal(x, xref)

    def test_smooth_gaussian(self):
        x = np.array([[1, 2, 3], [-1, -2, -3]])
        xref = np.array([[0, 1, 1], [0, -1, -1]])
        x = vt.op.smooth_gaussian(x, sigma=0.5)
        np.testing.assert_array_equal(x, xref)

    def test_warp2d(self):
        x = np.array([[1, 2, 3], [-1, -2, -3]])
        xref = np.array([[1, 2, 3], [-1, -2, -4]])
        x = vt.op.warp2d(x, x, x, scale=0.1)
        np.testing.assert_array_equal(x, xref)


if __name__ == '__main__':
    unittest.main()
