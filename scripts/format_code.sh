#!/bin/bash

# python
DIRS="src_python tests doc scripts"

for i in ${DIRS}; do
    yapf -vv --in-place --recursive "$i/."
done

# clean-up
find . -type f -name 'yapf*.py' -exec rm -f {} +

# Fortran
DIRS="lib"

for f in `ls ${DIRS}/*.f90`; do
    echo 'formatting: '${f}
    fprettify -i 3 ${f}
done

