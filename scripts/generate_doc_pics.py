import matplotlib.pyplot as plt
import numpy as np
import os
#
import vterrain as vt

PICS_PATH = 'doc/pics'

# --- primitives

shape = (256, 256)
res = (4, 4)
seed = 1

op_dict = {
    'perlin2d':
    vt.noise.perlin2d(shape, res, seed=seed),
    'perlin2d_exp':
    vt.noise.perlin2d(shape, res, seed=seed, mu=0.2),
    'value2d':
    vt.noise.value2d(shape, res, seed=seed),
    'worley2d':
    vt.noise.worley2d(shape, res, seed=seed, distance_exponent=0.5),
    'fault2d':
    vt.noise.fault2d(shape, seed=seed),
    'gabor2d':
    vt.noise.gabor2d(shape, width=0.1, f0=10, w0=30 / 180 * np.pi, seed=seed),
    'diamond_square2d':
    vt.noise.diamond_square2d(shape, seed=seed, persistence=0.7),
    'ridge2d':
    vt.noise.ridge2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed),
    'fractal2d':
    vt.noise.fractal2d(shape,
                       res,
                       noise_fct=vt.noise.worley2d,
                       distance_exponent=1,
                       seed=seed),
    'hybrid_multifractal2d':
    vt.noise.hybrid_multifractal2d(shape,
                                   res,
                                   noise_fct=vt.noise.perlin2d,
                                   seed=seed),
    'spot2d':
    vt.noise.spot2d(shape,
                    kernel=np.ones((10, 10)),
                    seed=seed,
                    sparse=True,
                    density=0.05),
}

for k, v in op_dict.items():
    vt.fio.write(v, os.path.join(PICS_PATH, 'noise_' + k + '.png'))

# --- alterations

z = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)
plt.imsave(os.path.join(PICS_PATH, 'hmap_alter_before.png'),
           z,
           cmap='terrain',
           vmin=0,
           vmax=2)

idx = vt.op.local_maxima(z, prefilter=True)[:2]
z = vt.hmap.alter(z, idx=idx, new_elevations=[2, 1.5])
plt.imsave(os.path.join(PICS_PATH, 'hmap_alter_after.png'),
           z,
           cmap='terrain',
           vmin=0,
           vmax=2)

# - thinplate
rng = np.random.default_rng(seed)

n = 20
x = rng.random(n)
y = rng.random(n)
z = rng.random(n)

x = np.append(x, [0.4, 0.7])
y = np.append(y, [0.3, 0.5])
z = np.append(z, [4, 3])

zt = vt.hmap.generate_thinplate(shape, x, y, z)
plt.imsave(os.path.join(PICS_PATH, 'hmap_alter_thinplate.png'),
           zt,
           cmap='terrain',
           vmin=0,
           vmax=4)

# --- depression filling

z = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)
z = vt.hmap.depression_filling(z)
plt.imsave(os.path.join(PICS_PATH, 'hmap_depression_filling.png'),
           z,
           cmap='terrain',
           vmin=0,
           vmax=1)

# --- erosion(s)

# thermal
z = vt.noise.fractal2d(shape, res, noise_fct=vt.noise.perlin2d, seed=seed)
plt.imsave(os.path.join(PICS_PATH, 'erosion_before.png'),
           z,
           cmap='terrain',
           vmin=0,
           vmax=1)

ze = vt.erosion.thermal(z, talus=0.015)
plt.imsave(os.path.join(PICS_PATH, 'erosion_thermal.png'),
           ze,
           cmap='terrain',
           vmin=0,
           vmax=1)

# hydraulic
zf = vt.erosion.hydraulic_flow_stream(z)
plt.imsave(os.path.join(PICS_PATH, 'erosion_hydraulic_flow_stream.png'),
           zf,
           cmap='terrain',
           vmin=0,
           vmax=1)

zm = vt.erosion.hydraulic_musgrave(z,
                                   water_level=0.001,
                                   iterations=200,
                                   tau=1e-2)
zm = vt.erosion.thermal(zm, talus=0.04)

plt.imsave(os.path.join(PICS_PATH, 'erosion_hydraulic_musgrave.png'),
           zm,
           cmap='terrain',
           vmin=0,
           vmax=1)

zp = vt.erosion.hydraulic_particles(z, particle_density=0.3)
plt.imsave(os.path.join(PICS_PATH, 'erosion_hydraulic_particles.png'),
           zp,
           cmap='terrain',
           vmin=0,
           vmax=1)

# hydrology
smap = vt.hydrology.stream_map(z)
plt.imsave(os.path.join(PICS_PATH, 'erosion_hydrology_stream_map.png'), smap)
