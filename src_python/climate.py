"""Climate generation.
"""

import earthpy.spatial as es
import numpy as np
import os
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def precipitation_map(z,
                      wind_angle=0.,
                      soil_moisture_map=None,
                      particle_density=0.1,
                      p_inertia=0.1,
                      k_soil_evap=0.1,
                      cloud_radius=0.1,
                      seed=-1,
                      iterations=500):
    """
    Compute a precipation map with particle-based simulation.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    wind_angle : float or ndarray, optional
        Wind angle in radian, can be defined locally. Default is 0.
    soil_moisture_map : ndarray, optional
        Soil moisture map (quantity of water) as an ndarray between 0 and 1.
        Default is 'None', leading to a uniform moisture map equals to 0.5.
    particle_density : float, optional
        Particle density: the number of particles simulated equals the
        particle density multiplied by the heightmap size = shape[0]*shape[1].
        Default is 0.1.
    p_inertia : float, optional
        Particle direction inertia. Default is 0.1.
    k_soil_evap : float, optional
        Soil moisture evaporation rate (and transfer to the particle). Default
        is 0.05.
    cloud_radius : float, optional
        Could radius. If set in ]0, 1], this parameter corresponds to the
        rain kernel radius, adjusted to the grid shape. Default is 0.1.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    iterations : int, optional
        Maximum number of iterations for each particle simulation. Default
        is 500.

    Returns
    -------
    out : ndarray
        Moisture map between 0 and 1.

    """

    if not (isinstance(wind_angle, np.ndarray)):
        wind_angle *= np.ones(z.shape)

    if soil_moisture_map is None:
        soil_moisture_map = 0.5 * np.ones(z.shape)

    # clouds are represented by a smooth cosine kernel centered on the
    # particle
    nr = int(np.ceil(cloud_radius * max(z.shape)))
    kernel = vt.kernel.smooth_cosine((2 * nr + 1, 2 * nr + 1))

    # define core solver parameters based on 'less' grid resolution
    # dependent python wrapper parameters
    n_particles = particle_density * z.shape[0] * z.shape[1]

    pmap = vterrain_core.precipation_map_particles(
        z=z,
        soil_moisture_map=soil_moisture_map,
        wind_angle=wind_angle,
        iterations=iterations,
        n_particles=n_particles,
        p_inertia=p_inertia,
        k_soil_evap=k_soil_evap,
        seed=seed,
        kernel=kernel)

    return vt.op.remap(pmap, vmin=0, vmax=1)


@vt.logger.show_input(log)
def snowfall_map(z,
                 lowest_elevation,
                 elevation_transition_ratio=0.05,
                 talus=0.015,
                 sun_exposure_influence=0,
                 sigma_radius=0):
    """
    Compute a snowfall map based on various geometric features of the
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    lowest_elevation : float
        Snow occurence lowest elevation.
    elevation_transition_ratio : float, optional
        Elevation transition between snow and no snow, normalized by
        heightmap peak-to-peak amplitude. Default is 0.05.
    talus : float or ndarray, optional
        Talus limits, value between 0 (flat) and 1 (sloped).
        Can be define locally. Default value is 0.015.
    sun_exposure_influence : float, optional
        Influence coefficient of the sun exposure on snow melting (0: no
        influence, 1: full influence). Default is 0.
    sigma_radius : float, optional
        Final Gaussian filter half-width with respect to the domain size.
        Default is 0.

    Returns
    -------
    out : ndarray
        Snowfall map between 0 and 1.

    """

    # elevation transition
    zc = lowest_elevation
    zt = zc - elevation_transition_ratio * z.ptp()

    mask = np.ones(z.shape)
    mask[np.where(z < zt)] = 0

    idx = np.where((z > zt) & (z < zc))
    mask[idx] = (z[idx] - zt)**2 / (zc - zt)**2

    # talus
    mask[np.where(vt.op.gradient_talus(z) > talus)] = 0

    # sun exposure
    if sun_exposure_influence > 0:
        mask *= sun_exposure_influence * (
            1 - vt.climate.solar_irradiance_map(z)**0.5)

    if sigma_radius:
        mask = vt.op.smooth_gaussian(mask, sigma_radius)

    return vt.op.remap(mask, vmin=0, vmax=1)


@vt.logger.show_input(log)
def solar_irradiance_map(z,
                         angles=[200, 270, 330],
                         altitudes=[1, 1, 1],
                         weights=[1, 4, 1]):
    """
    Compute solar irradiance of a given heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    angles : float (list), optional
        Angular positions in degrees of the sun used to calculate irradiance
        (south is 270 degrees).
    altitudes : float (list), optional
        Angular altitudes in degrees of the sun used to calculate irradiance
    weights : float (list), optional
        Weight contribution of each angular position to the irradiance (morning
        and evening lights are for instance generally weaker).

    Returns
    -------
    out : ndarray
        Solar irradiance map between 0 (in the shadow) and 1 (in the sun).

    """

    smap = np.zeros(z.shape)
    for a, alt, w in zip(angles, altitudes, weights):
        smap += w * es.hillshade(z, azimuth=a, altitude=alt)

    return vt.op.remap(smap, vmin=0, vmax=1)
