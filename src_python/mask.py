"""Functions for countour.
"""

import numpy as np
import os
import scipy
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def from_angle(z,
               x=None,
               y=None,
               sigma_radius=0,
               hard_mask=True,
               angle_thresholds=[80, 100]):
    """
    Define a mask based on the elevation surface angle in degrees
    (between -180 and 180 degrees).

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    x : ndarray, optional
        'x' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    y : ndarray, optional
        'y' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    sigma_radius : float, optional
        Radius of the Gaussian filter kernel (between 0 and 1, the domain
        is assumed to be a unit-square). Default is 0.
    hard_mask : bool, optional
        Define wether the mask values are continuous (angle values in
        degree) or quantized (0 or 1 if within the angle_thresholds interval).
        Default is True.
    angle_thresholds : (float, float), optional
        Angle interval in degree where the mask is positive. Used only for
        hard masks.

    Returns
    -------
    out : ndarray
        Mask.

    """

    if sigma_radius:
        scipy.ndimage.gaussian_filter(z,
                                      sigma=sigma_radius * max(z.shape),
                                      output=z)

    alpha = vt.op.gradient_angle(z, x, y) / np.pi * 180

    if hard_mask:
        alpha = np.where(
            (alpha >= angle_thresholds[0]) & (alpha >= angle_thresholds[1]), 1,
            0)

    return alpha


@vt.logger.show_input(log)
def from_convexity(w, sigma_radius=0.05, epsilon_threshold=0.01):
    """
    Define a mask from heightmap convexity.
    
    Parameters
    ----------
    w : ndarray
        Input heightmap as a 2-dimensional array.
    sigma_radius : float, optional
        Gaussian filter half-width with respect to the domain size.
        Default is 0.05.
    epsilon_threshold : float, optional
        Below this threshold value (normalized by peak-to-peak Gaussian
        curvature amplitude), the Gaussian curvature is assumed to be zero.
        Default is 0.01.
    
    Returns
    -------
    mask : ndarray, 2d
        Convexity index in {-1, 0, 1}. '-1' for concave, '0' for flat
        and '1' for convex.
        
    Notes
    -----
    Set 'epsilon_threshold' to 0 to get a binary output 'convex' or 'concave'.
        
    """

    _, mask = vt.op.gaussian_curvature(w, sigma_radius=sigma_radius)

    eps = epsilon_threshold * mask.ptp()
    mask[np.where(mask >= 0)] = 1
    mask[np.where(mask < 0)] = -1
    mask[np.where(np.abs(mask) < eps)] = 0

    return -mask
