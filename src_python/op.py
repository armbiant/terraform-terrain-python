"""Generic operators.
"""
import numpy as np
import os
import scipy
import skimage
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


def add_array(a, i, j, b):
    """
    Add to an ndarray 'a' of shape (M, M) an ndarray 'b' of shape (N,
    N), centered on the indices (i, j).

    Parameters
    ----------
    a : ndarray (M, M)
        Input array #1.
    i : int
        Index i (for array #1) where array #2 is added.
    j : int
        Index j (for array #1) where array #2 is added.
    b : ndarray (N, N)
        Input array #2.

    Returns
    -------
    out : ndarray (M, M)
        Sum array.

    """

    nk = b.shape[0]

    # truncate kernel to make it fit into heightmap array
    nk0 = int(np.floor(nk / 2))  # left
    nk1 = nk - nk0  # right

    ik0 = max(0, nk0 - i)
    jk0 = max(0, nk0 - j)
    ik1 = min(nk, nk - (i + nk1 - a.shape[0]))
    jk1 = min(nk, nk - (j + nk1 - a.shape[1]))

    # where it goes in the heigtmap
    i0 = max(i - nk0, 0)
    j0 = max(j - nk0, 0)
    i1 = min(i + nk1, a.shape[0])
    j1 = min(j + nk1, a.shape[1])

    a[i0:i1, j0:j1] += b[ik0:ik1, jk0:jk1]

    return a


@vt.logger.show_input(log)
def clamp(x, vmin, vmax):
    """
    Clamp array values between a minimum and a maximum, ignoring
    any NaNs.

    Parameters
    ----------
    x : ndarray
        Input array.
    vmin : float, optional
        Low clamping value of array. Default is 0.
    vmax : float, optional
        High clamping value of array. Default is 1.

    Returns
    -------
    out : ndarray
        The clamped array.

    """

    return np.minimum(vmax * np.ones(x.shape),
                      np.maximum(vmin * np.ones(x.shape), x))


@vt.logger.show_input(log)
def detect_border(z):
    """
    Detect borders for 2d mask defined by constant values.

    Parameters
    ----------
    z : ndarray, 2d
        Input heightmap.

    Results
    -------
    out : ndarray, 2d
        Border mask (0 or 1).
    """

    zx = z - np.roll(z, -1, axis=0)
    zy = z - np.roll(z, -1, axis=1)
    mask = zx + zy
    mask[-1, :] = 0
    mask[:, -1] = 0
    return np.where(np.abs(mask) > 1e-10, 1.0, 0.0)


@vt.logger.show_input(log)
def detect_ridge_valley(z,
                        binary_output=False,
                        binary_threshold=0.05,
                        size=10):
    """
    Detect ridges and valleys of a given heightmap.
    
    Parameters
    ----------
    z : ndarray, 2d
        Input heightmap.
    binary_output : bool, optional
        Determine wether the output is continuous or binary (1 for ridges and
        valleys and 0 elsewhere). Default is False.
    binary_threshold : float, optional
        Threshold value defining ridge or valley regions when binary_output is
        set to True. Default is 0.1.
    size : int, optional
        Radius of the maximum filter use for when binary_output is set to
        True. Default is 10.
        
    Returns
    -------
    out : ndarray, 2d
        Ridges, normalized between 0 and 1.
    out : ndarray, 2d
        Valleys, normalized between 0 and 1.
    
    """

    r = skimage.filters.frangi(z, black_ridges=False, mode='reflect')
    v = skimage.filters.frangi(z, black_ridges=True, mode='reflect')

    r = (r - r.min()) / r.ptp()
    v = (v - v.min()) / v.ptp()

    if binary_output:
        idx = np.where(r > binary_threshold)
        r[idx] = r[idx] / scipy.ndimage.maximum_filter(r[idx], size=size)
        r = np.where(r > 1 - binary_threshold, 1.0, 0.0)

        idx = np.where(v > binary_threshold)
        v[idx] = v[idx] / scipy.ndimage.maximum_filter(v[idx], size=size)
        v = np.where(v > 1 - binary_threshold, 1.0, 0.0)

    return r, v


@vt.logger.show_input(log)
def dijkstra(w, s, e, distance_exponent=0.5):
    """Find the lowest elevation path between two points in a 2d-array.
    
    Parameters
    ----------
    w : ndarray
        Input array ("elevation" map).
    s : (int, int)
        Starting point pair of indices.
    e : (int, int)
        Ending point pair of indices.
    distance_exponent : float, optional
        Exponent of the distance calculation between two points. Increasing
        the "distance exponent" of the cost function increases the cost of
        elevation gaps: path then tends to stay at the same elevation if
        possible (i.e. reduce the overall cumulative elevation gain). Default
        is 0.5.

    Returns
    -------
    out : ndarray
        Shortest path indices, integers, shape (n, 2).

    References
    ----------
    - Dijkstra, E.W. 1971. A short introduction to the art of
      programming.
    - see https://math.stackexchange.com/questions/3088292/finding-lowest-elevation-path-between-two-points

    """

    path = vterrain_core.dijkstra(w=vt.op.remap(w),
                                  ist=s[0],
                                  jst=s[1],
                                  ie=e[0],
                                  je=e[1],
                                  distance_exponent=distance_exponent)
    n = int(np.round(len(path[np.where(path > 0)]) / 2))
    return path[n - 1::-1]


@vt.logger.show_input(log)
def enforce_boundaries(z, values=0, xc=0.4, yc=0.4):
    """
    Enforce values at the boundaries of the array.

    Parameters
    ----------
    z : ndarray
        Input array.
    values : float, or [float]*4 or [1d array]*4
        Values at the boundaries. Can be a float: same value for boundaries,
        a list of four floats for the values at east, west, south and north
        boundaries, or a list of 1d-vector defining the boundary values for
        each cell. Default is 0.
    xc : float, optional
        Transition ratio in the 'x' direction. Default is 0.4.
    yc : float, optional
        Transition ratio in the 'y' direction. Default is 0.4.

    Results
    -------
    out : ndarray
        Modified array.

    """

    nx, ny = z.shape[0], z.shape[1]

    # define border value vectors
    if type(values) != list:
        v_west = values * np.ones(ny)
        v_east = values * np.ones(ny)
        v_south = values * np.ones(nx)
        v_north = values * np.ones(nx)

    else:
        if len(values) == 4:
            v_west = values[0] * np.ones(ny)
            v_east = values[1] * np.ones(ny)
            v_south = values[2] * np.ones(nx)
            v_north = values[3] * np.ones(nx)

        else:
            v_west = values[0]
            v_east = values[1]
            v_south = values[2]
            v_north = values[3]

    # compute distance to the boundaries for each cell
    x, y = np.meshgrid(np.linspace(0, 1, nx),
                       np.linspace(0, 1, ny),
                       indexing='ij')

    # 'x' boundaries
    r = np.where(x < xc, 0.5 + 0.5 * np.cos(np.pi * x / xc), 0.0)
    z = r * v_west[None, :] + (1 - r) * z

    r = np.flipud(r)
    z = r * v_east[None, :] + (1 - r) * z

    # 'y' boundaries
    r = np.where(y < yc, 0.5 + 0.5 * np.cos(np.pi * y / yc), 0.0)
    z = r * v_south[:, None] + (1 - r) * z

    r = np.fliplr(r)
    z = r * v_north[:, None] + (1 - r) * z

    return z


@vt.logger.show_input(log)
def equalize(x, local=False, size=(32, 32)):
    """
    Equalize amplitudes of a 2d-array.
    
    Parameters
    ----------
    x : ndarray
        Input mask.
    local : bool, optional
        'Local' equalization. Default is False.
    size : (int, int), optional
        Kernel size for local equalization. Default is (32, 32).

    Returns
    -------
    out : ndarray
        Equalized array.

    """

    if local:
        y = skimage.filters.rank.equalize(
            x, footprint=vt.kernel.smooth_cosine(size))
        y = vt.op.remap(y, w.min(), w.max())
    else:
        y = skimage.exposure.equalize_hist(x)
    return x


def extract_subarray(a, i, j, shape_out):
    """
    Extract from an ndarray 'a' of shape (M, M) an ndarray 'b' of
    shape (P, Q), centered on the indices (i, j).

    Parameters
    ----------
    a : ndarray (M, M)
        Input array #1.
    i : int
        Index i (for array #1) where array #2 is extracted.
    j : int
        Index j (for array #1) where array #2 is extracted.
    b : ndarray (P, Q)
        Extracted array shape.

    Returns
    -------
    out : ndarray (P, Q)
        Extracted array.

    """

    nkx = shape_out[0]
    nky = shape_out[1]

    # truncate kernel to make it fit into heightmap array
    nkx0 = int(np.floor(nkx / 2))  # left
    nkx1 = nkx - nkx0  # right

    nky0 = int(np.floor(nky / 2))  # bottom
    nky1 = nky - nky0  # top

    # extracted region
    ilist = np.arange(i - nkx0, i + nkx1)
    ilist = np.minimum(np.maximum(ilist, 0), a.shape[0])

    jlist = np.arange(j - nky0, j + nky1)
    jlist = np.minimum(np.maximum(ilist, 0), a.shape[1])

    ij = np.meshgrid(ilist, jlist)

    return a[ij]


@vt.logger.show_input(log)
def fractalize_mask_contours(mask,
                             discretization_density=0.05,
                             iterations=2,
                             sigma=0.2,
                             mu=0):
    """
    Modify contours of a mask by adding fractal noise.

    Parameters
    ----------
    mask : ndarray
        Input mask.
    discretization_density : float, optional
        'Azimuthal' discretization density, in [0, 1]. Increase this parameter
        to capture more details of the mask contours, but at the cost of having
        more control points in the end. Default is 0.05.
    iterations : int
        Number of iterations (e.g. subdivision + random displacement) applied
        to the original contour. Default is 2.
    sigma : float, optional
        Half-width of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.2.
    mu : float, optional
        Mean of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.

    Returns
    -------
    out : ndarray
        Mask with noised contours.

    See also
    --------
    - vterrain.contour.map_to_xy
    - vterrain.contour.fractalize_2d
    
    """

    xc_list, yc_list = vt.contour.map_to_xy(mask, discretization_density)

    mask_out = np.zeros(mask.shape)
    for xc, yc in zip(xc_list, yc_list):
        xf, yf = vt.contour.fractalize_2d(xc,
                                          yc,
                                          iterations=iterations,
                                          sigma=sigma,
                                          mu=mu)
        contour_map = vt.contour.xy_to_map(
            xf,
            yf,
            shape=mask.shape,
            filled=True,
            extent=[0, mask.shape[0] - 1, 0, mask.shape[1] - 1])
        mask_out = np.maximum(mask_out, contour_map)

    return mask_out


@vt.logger.show_input(log)
def gamma_correction(x, gamma=None):
    """
    Apply gamma correction to the input array.

    Parameters
    ----------
    x : ndarray
        Input mask.
    gamma : float or None, optional
        Gamma correction factor. Default is None, gamma is chosen automatically.

    Returns
    -------
    out : ndarray
        Corrected array.
    """

    xmin = x.min()
    xmax = x.max()
    xn = vt.op.remap(x)

    if gamma is None:
        gamma = -0.301 / np.log10(np.mean(xn))

    return vt.op.remap(xn**gamma, vmin=xmin, vmax=xmax)


@vt.logger.show_input(log)
def gaussian_curvature(w, sigma_radius=0.05):
    """
    Compute Gaussian and mean curvatures of the given heightmap.
    
    Parameters
    ----------
    w : ndarray
        Input heightmap as a 2-dimensional array.
    sigma_radius : float, optional
        Gaussian filter half-width with respect to the domain size.
        Default is 0.05.
    
    Returns
    -------
    k : ndarray, 2d
        Gaussian curvature 'K1*K2' array.
    h : ndarray, 2d
        Mean curvature '(K1+K2)/2' array.

    References
    ----------
    - Kurita, T. and Boulanger, P. 1992. Computation of surface curvature from
      range images using geometrically intrinsic weights. Proc. of IAPR
      Workshop on Machine Vision Applications (MVA’92), 389–392.
    """

    # prefiltering
    if sigma_radius:
        sigma = sigma_radius * max(w.shape)
        scipy.ndimage.gaussian_filter(w, sigma=sigma, output=w)

    wx, wy = np.gradient(w)
    wxx, wxy = np.gradient(wx)
    _, wyy = np.gradient(wy)

    # Gaussian curvature = K1 * K2
    k = (wxx * wyy - (wxy**2)) / (1 + (wx**2) + (wy**2))**2
    # mean curvature = (K1 + K2)/2
    h = (wxx * (1 + wy**2) - 2 * wxy * wx * wy + wyy *
         (1 + wx**2)) / 2 / (1 + wx**2 + wy**2)**1.5

    return k, h


@vt.logger.show_input(log)
def gradient(z, x=None, y=None):
    """
    Compute the gradient of a 2-dimensional field.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    x : ndarray, optional
        'x' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    y : ndarray, optional
        'y' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.

    Returns
    -------
    out : ndarray, ndarray
        Gradient arrays in both direction: dz/dx and dz/dy.

    """

    if x is not None:
        return np.gradient(z, axis=0)/np.gradient(x, axis=0), \
               np.gradient(z, axis=1)/np.gradient(y, axis=1)
    else:
        return np.gradient(z, axis=0), np.gradient(z, axis=1)


@vt.logger.show_input(log)
def gradient_angle(z, x=None, y=None):
    """
    Compute the angle of the gradient of a 2-dimensional field.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    x : ndarray, optional
        'x' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    y : ndarray, optional
        'y' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.

    Returns
    -------
    out : ndarray
        The angle (in radian) of the gradient.

    """

    dzx, dzy = gradient(z, x, y)
    return np.arctan2(dzy, dzx)


@vt.logger.show_input(log)
def gradient_norm(z, x=None, y=None):
    """
    Compute the gradient norm of a 2-dimensional field.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional
    x : ndarray, optional
        'x' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    y : ndarray, optional
        'y' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.

    Returns
    -------
    out : ndarray
        Gradient norm.

    """

    dzx, dzy = gradient(z, x, y)
    return (dzx**2 + dzy**2)**0.5


@vt.logger.show_input(log)
def gradient_slope(z, x=None, y=None):
    """
    Compute the gradient slope (in degrees) of a 2-dimensional field.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional
    x : ndarray, optional
        'x' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.
    y : ndarray, optional
        'y' coordinate values, as a meshgrid ('ij' indexing). Default
        is none.

    Returns
    -------
    out : ndarray
        Gradient slope in degrees.

    """

    return np.arctan(gradient_norm(z, x, y)) / np.pi * 180


@vt.logger.show_input(log)
def gradient_talus(z, normalized=False):
    """
    Compute the "talus" slope (smallest elevation gap between neighboring
    cells) of a 2-dimensional field.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional
    normalized : bool, optional
        Normlalized output by peak-to-peak elevation amplitude. Default
        is False.

    Returns
    -------
    out : ndarray
        Gradient talus.

    """

    talus = vterrain_core.compute_talus(z)

    if normalized:
        talus /= z.ptp()

    return talus


@vt.logger.show_input(log)
def harden(x, gain):
    """
    'Harden' array values by applied a hyperbolic tangent function to
    the values.

    Parameters
    ----------
    x : ndarray
        Input array
    gain : float or ndarray
        Hardening intensity: 1=fairly linear, 10=very sharp transitions.

    Returns
    -------
    out : ndarray
        Hardened array.

    """

    xmin = np.nanmin(x)
    xmax = np.nanmax(x)
    w = np.tanh((2 * (x - xmin) / (xmax - xmin) - 1) * gain)
    w = (w + 1) / 2 * (xmax - xmin) + xmin
    return w


@vt.logger.show_input(log)
def lerp(x1, x2, t):
    """
    Returns the result of linearly interpolating between input x1 and
    input x2 by input t. The value of input t is expected to be in the
    range [0, 1].

    Parameters
    ----------
    x1 : ndarray
        Input array #1.
    x2 : ndarray
        Input array #2.
    t : ndarray
        Input array.

    Returns
    -------
    out : ndarray
        Interpolated array: when t=0, the return value is x1 and when
        t=1, the return value is x2.

    """

    return x1 * (1 - t) + x2 * t


@vt.logger.show_input(log)
def local_maxima(x, min_distance=None, prefilter=False):
    """
    Return the coordinates of local peaks based on skimage
    'peak_local_max'.

    Parameters
    ----------
    x : ndarray
        Input array.
    min_distance : int, optional
        Minimum distance between two local extrema (peaks at smaller
        distances are discarded). Default is None, then the min_distance
        is taken to be 10th of the array shape.
    prefilter : boolean, optional
        Apply or not, a median filter to the array before searching for the
        extrema. Can be useful for arrays with broadband content to
        superfluous extrema detection. Median filter size is
        (min_distance, min_distance). Default is False.

    Returns
    -------
    out : ndarray
        Indices of the maxima, array of shape (N, 2) where N is the
        number of maxima.

    """

    if min_distance is None:
        min_distance = int(min(x.shape) / 10)

    if prefilter:
        x = scipy.ndimage.median_filter(x, size=(min_distance, min_distance))

    idx = skimage.feature.peak_local_max(x, min_distance=min_distance)

    return np.array(idx, dtype=int)


@vt.logger.show_input(log)
def plateau(x, plateau_size=0.1, gain=2):
    """
    Gives a "plateau"-like shape to the array values.

    Parameters
    ----------
    x : ndarray
        Input array
    plateau_size : float, optional
        Plateau radius (domain is supposed to be unit-square). Default is 0.1.
    gain : float or ndarray
        Sharpening intensity: 1=fairly linear, 10=very sharp transitions.

    Returns
    -------
    out : ndarray
        Transformed array.

    """

    ns = int(plateau_size * max(x.shape))

    # local height is "normalized" locally using an height value
    # locally averaged
    xmin = scipy.ndimage.minimum_filter(x, size=(ns, ns))
    xmax = scipy.ndimage.maximum_filter(x, size=(ns, ns))

    # this part is the same as 'op.harden' below
    w = np.tanh((2 * (x - xmin) / (xmax - xmin) - 1) * gain)
    w = (w + 1) / 2 * (xmax - xmin) + xmin

    return w


@vt.logger.show_input(log)
def redistribute_values(w, pdf, seed=-1, **kwargs):
    """
    Redistribute values of an ndarray according to a given probability
    density function of these values, without changing the overall 'shape'
    of the corresponding heightmap.
    
    Parameters
    ----------
    w : ndarray, 2d
        Input array.
    pdf : str or ndarray, 1d, float
        Probability density function. Pre-defined pdf can be used (and their
        default parameters can be modified): 'pyramid'
        (Kunlun), 'pyramid_r' (Alps), 'diamond' (Rockies), 'hourglass' (Himalayas).
        The pdf itself can be provided as a 1d array.
    extent : tuple of 1 floats
        Extent of the resulting array (xmin, xmax). Default is [0, 1].
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.
        
    Returns
    -------
    out : ndarray, 2d, float
        Arrays with modified values.
    
    References
    ----------
    - Elsen, P.R. and Tingley, M.W. 2015. Global mountain topography and the
      fate of montane species under climate change. Nature Climate Change 5,
      772–776.
    
    """

    wmin = w.min()
    wmax = w.max()

    # pre-defined distributions
    if type(pdf) == str:

        x = np.linspace(0, 1, 65536)

        if pdf == 'pyramid':  # Kunlun
            pdf = x

        elif pdf == 'pyramid_r':  # Alps
            pdf = 1 - x

        elif pdf == 'diamond':  # Rockies
            if 'xc' not in kwargs.keys():
                xc = 0.5
            else:
                xc = kwargs['xc']

            pdf = np.where(x < xc, x / xc, (1 - x) / (1 - xc))

        elif pdf == 'hourglass':  # Himalaya
            params = {
                'x1': 0.25,
                'x2': 0.75,
                'peak_ratio': 0.5,
                'peak_width': 0.1,
            }
            for k in params.keys():
                if k in kwargs.keys():
                    params[k] = kwargs[k]

            pdf = params['peak_ratio'] * np.exp(
                -(x - params['x1'])**2 / params['peak_width']**2 / 2) + np.exp(
                    -(x - params['x2'])**2 / params['peak_width']**2 / 2)

    # generate new values
    wnew = vt.grid.random_grid1d_pdf(w.shape[0] * w.shape[1],
                                     pdf,
                                     preserve_extrema=True)
    wnew = vt.op.remap(wnew, vmin=wmin, vmax=wmax).reshape(w.shape)

    # do the mapping
    idx_new = np.unravel_index(np.argsort(wnew, axis=None), w.shape)
    idx_old = np.unravel_index(np.argsort(w, axis=None), w.shape)

    # mapped array
    m = np.zeros(w.shape)
    m[idx_old] = wnew[idx_new]

    return m


@vt.logger.show_input(log)
def remap(x, vmin=0, vmax=1):
    """
    Rescale an array between given minimum and maximum values, ignoring
    any NaNs.

    Parameters
    ----------
    x : ndarray
        Input array
    vmin : float, optional
        New minimum value of array. Default is 0.
    vmax : float, optional
        New maximum value of array. Default is 1.

    Returns
    -------
    out : ndarray
        Rescaled array.

    """

    xmin = np.nanmin(x)
    xmax = np.nanmax(x)

    if xmin != xmax:
        return (x - xmin) / (xmax - xmin) * (vmax - vmin) + vmin
    else:
        return vmax * np.ones(x.shape)


@vt.logger.show_input(log)
def remap_local(x, radius, vmin=0, vmax=1):
    """
    Locally rescale an array between given minimum and maximum values.

    Parameters
    ----------
    x : ndarray
        Input array
    radius : narray
        'Local' radius (in [0, 1]) with respect to the domain size.
    vmin : float, optional
        New minimum value of array. Default is 0.
    vmax : float, optional
        New maximum value of array. Default is 1.

    Returns
    -------
    out : ndarray
        Rescaled array.

    """

    ir = int(radius * max(x.shape))

    xmin = scipy.ndimage.minimum_filter(x, size=(ir, ir))
    xmax = scipy.ndimage.maximum_filter(x, size=(ir, ir))

    if np.array_equal(xmin, xmax):
        return vmax * np.ones(x.shape)
    else:
        dx = xmax - xmin
        invx = np.where(dx > 1e-6, 1 / dx, 0)
        return (x - xmin) * invx * (vmax - vmin) + vmin


@vt.logger.show_input(log)
def reshape_interp(w, shape):
    """
    Reshape a 2d array using interpolation to fill missing values.
    
    Parameters
    ----------
    w : ndarray, 2d
        Input array
    shape : (int, int)
        New shape.
        
    Returns
    -------
    out, ndarray, 2d
        Array with new shape.
        
    """

    fitp = scipy.interpolate.RectBivariateSpline(np.linspace(0, 1, w.shape[0]),
                                                 np.linspace(0, 1, w.shape[1]),
                                                 w,
                                                 bbox=[0, 1, 0, 1],
                                                 kx=3,
                                                 ky=3)
    return fitp(np.linspace(0, 1, shape[0]), np.linspace(0, 1, shape[1]))


@vt.logger.show_input(log)
def rugosity(x, radius=0):
    """
    Compute local rugosity based on the skewness value.
    
    Parameters
    ----------
    x : ndarray
        Input array.
    radius : float, optional
        Averaging window radius. Default is 0, leading to a 2x2 pixels kernel.
    
    Returns
    -------
    out : ndarray
        Local rugosity.
    
    """

    # define averaging kernel
    ir = max(2, int(radius * max(x.shape)))
    k = np.ones((ir, ir)) / ir**2

    x_avg = scipy.ndimage.convolve(x, k, mode='reflect')
    x_std = np.sqrt(scipy.ndimage.convolve(x**2, k, mode='reflect') - x_avg**2)
    x_skw = (scipy.ndimage.convolve(x**3, k, mode='reflect') -
             x_avg**3) / x_std**3

    x_skw = np.where(x_std > 1e-6 * x.ptp(), x_skw, 0)

    return x_skw


@vt.logger.show_input(log)
def sharpen(x, ratio=1, kernel='cross'):
    """
    Sharpen a 2d-array.
    
    Parameters
    ----------
    x : ndarray, 2d
        Input array.
    ratio : float, optional
        Output ratio between sharpened (1) and no-sharpened array (0).
        Default is 1.
    kernel : string or ndarray, optional
        If set to 'cross' or 'square', the kernel is a (3, 3) ndarray with
        proper coefficients. 'kernel' can also be set directly as an ndarray
        (as a smoothing kernel, not a sharpening one).
        Default is 'cross'.
    
    Results
    -------
    out : ndarray, 2d
        Resulting array.
        
    """

    if isinstance(kernel, np.ndarray):
        # turn the smoothing kernel into a sharpening kernel
        ic = int((kernel.shape[0] - 1) / 2)
        jc = int((kernel.shape[1] - 1) / 2)
        kernel = -kernel
        kernel[ic, jc] += 2

    else:
        if kernel == 'cross':
            kernel = np.array([[0, -1, 0], [-1, 5, -1], [0, -1, 0]],
                              dtype=float)

        elif kernel == 'square':
            kernel = np.array([[-1, -1, -1], [-1, 9, -1], [-1, -1, -1]],
                              dtype=float)

    xs = scipy.ndimage.convolve(x, kernel, mode='reflect')

    return ratio * xs + (1 - ratio) * x


@vt.logger.show_input(log)
def smooth_cos(x, radius=0.05):
    """
    Smooth 2d-array using a smooth cosine kernel.

    Parameters
    ----------
    x : ndarray, 2d
        Input array
    radius : float, optional
        Kernel radius with respect to the domain size. Default is 0.05.

    Returns
    -------
    out : ndarray, 2d
         Smoothed array.

    """
    ir = int(radius * max(x.shape))
    k = vt.kernel.smooth_cosine((ir, ir))
    return scipy.signal.fftconvolve(x, k, mode='same')


@vt.logger.show_input(log)
def smooth_gaussian(x, sigma_radius=0.05, mode='reflect'):
    """
    Smooth 2d-array using a Gaussian filter.

    Parameters
    ----------
    x : ndarray, 2d
        Input array
    sigma_radius : float, optional
        Gaussian half-width with respect to the domain size. Default is 0.05.
    mode : str, optional
        Boundary mode. Default is 'reflect'.

    Returns
    -------
    out : ndarray, 2d
         Smoothed array.

    """
    sigma = sigma_radius * max(x.shape)
    return scipy.ndimage.gaussian_filter(x, sigma=sigma, mode=mode)


@vt.logger.show_input(log)
def smooth_laplace(x, sigma=0.1, iterations=3):
    """
    Smooth 2d-array using a Laplace low-pass filter.

    Parameters
    ----------
    x : ndarray, 2d
        Input array
    sigma : float, optional
        Filtering intensity in [0, 1]. Default is 0.1.
    iterations : int, optional
        Filtering iterations. Default is 3.

    Returns
    -------
    out : ndarray, 2d
         Smoothed array.

    """

    kernel = np.array([[0, -1, 0], [-1, 4, -1], [0, -1, 0]], dtype=float)

    for _ in range(iterations):
        lx = scipy.signal.convolve2d(x, kernel, mode='same', boundary='symm')
        x -= sigma * lx

    return x


@vt.logger.show_input(log)
def smooth_sharpen(w,
                   radius=0.1,
                   kernel=None,
                   rescale=True,
                   sharpen=True,
                   sharpen_iterations=1):
    """
    Combine smoothing and sharpening.

    Parameters
    ----------
    w : ndarray, 2d
        Input array.
    radius : float, optional
        Kernel radius with respect to the domain size. Default is 0.1.
    kernel : ndarray, optional
        Smoothing kernel. Default is a cone.
    rescale : bool, optional
        Heightmap rescaling to ensure (approx.) mass conservation. Default is True.
    sharpen : bool, optional
        Activate sharpening. Default is True.
    sharpen_iterations : int, optional
        Sharpening iterations. Default is 1.
    
    Returns
    -------
    out : ndarray, 2d
         Smoothed array.
    out : ndarray, 2d
         'Sediment' thickness array.

    """

    ir = int(radius * max(w.shape))

    if kernel is None:
        kernel = vt.kernel.cone((ir, ir))

    w0 = np.copy(w)

    # --- smoothing phase
    w = scipy.ndimage.convolve(w, kernel, mode='reflect')

    smap = np.maximum(w - w0, np.zeros(w.shape))
    stot = np.sum(smap)
    wtot = np.sum(w)

    if rescale:
        coeff_norm = (wtot - stot) / wtot
    else:
        coeff_norm = 1.0

    # --- sharpening phase
    if sharpen:
        for _ in range(sharpen_iterations):
            w0 = vt.op.sharpen(w0, kernel=vt.kernel.smooth_cosine((11, 11)))

    smap = np.maximum(w - w0 * coeff_norm, np.zeros(w.shape))
    w = np.maximum(w, w0 * coeff_norm)

    return w, smap


@vt.logger.show_input(log)
def spectral_filter(w, kc=0.1, graded_filter=True, filtering='lp'):
    """
    Spectral filtering of a 2d-array.

    Parameters
    ----------
    w : ndarray, 2d
        Input array.
    kc : float or (float, float), optional
        Wavenumber cut-off, in [0, 1]. If two values are provided, a band-pass
        filter is applied. The default is 0.1.
    graded_filter : bool, optional
        Smooth cut-off to avoid artifacts like ripples. The default is True.
    filtering : str, optional
        Filtering type 'high-pass' (hp) or 'low-pass' (lp). The default
        is 'lp'.

    Returns
    -------
    out : ndarray
        Filtered array.

    """

    iw = np.fft.rfft2(w)

    i, j = np.meshgrid(np.linspace(0, iw.shape[0] - 1, iw.shape[0]),
                       np.linspace(0, iw.shape[1] - 1, iw.shape[1]),
                       indexing='ij')

    if type(kc) == list:
        # band-pass filter
        n1 = int(kc[0] * max(iw.shape) / 2)
        n2 = int(kc[1] * max(iw.shape) / 2)
        nc = n2
        r2 = i**2 + j**2
        mask = np.where((r2 <= n2**2) & (r2 > n1**2), 1.0, 0.0)
        mask += np.flipud(mask)

    else:
        # low-pass and high-pass filter
        nc = int(kc * max(iw.shape) / 2)
        mask = np.where(i**2 + j**2 <= nc**2, 1.0, 0.0)
        mask += np.flipud(mask)

        if filtering == 'hp':
            # high-pass filter (default is low-pass)
            mask = 1 - mask

    if graded_filter:
        sigma = nc / 10
        mask = scipy.ndimage.gaussian_filter(mask, sigma=sigma)

    return np.fft.irfft2(mask * iw)


@vt.logger.show_input(log)
def stamp_kernel(x, kernel, density=0.01, seed=-1):
    """
    Stamp a kernel base on a input intensity mask.
    
    Parameters
    ----------
    x : ndarray
        Stamping intensity (amplitude).
    kernel : ndarray
        Stamping kernel.
    density : float, optional
        Samping density in [0, 1]. Default is 0.01.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray, 2-dimensional
        Resulting array after stamping.
    
    """

    # number of rotations
    NROT = 4
    mask = np.round(
        NROT *
        vt.noise.white2d(x.shape, seed=seed, sparse=True, density=density))
    mask = mask.astype(int)

    x_out = np.zeros(x.shape)
    for i in range(1, NROT + 1):
        xtmp = np.where(mask == i, 1.0, 0.0) * x

        if i < NROT:
            k = np.rot90(kernel, k=i)
        else:
            k = kernel

        x_out += scipy.signal.fftconvolve(xtmp, k, mode='same')

    return vt.op.remap(x_out, x.min(), x.max())


@vt.logger.show_input(log)
def steepen(x,
            scale=0.05,
            elevation_scaling=0,
            sigma_radius=0.05,
            reverse=False):
    """
    Steepen (or flatten) an elevation map.

    Parameters
    ----------
    x : ndarray, 2d
        Input array
    scale : float, optional
        Steepening scale, in [0, 1]. Default is 0.05.
    elevation_scaling : float, optional
        Scaling of the steepening with relative elevation, in [0, 1]. Default
        is 0 (no scaling).
    sigma_radius : float, optional
        Gaussian filter half-width with respect to the domain size, used to smooth
        gradient calculation. Default is 0.05.
    reverse : bool, optional
        Reverse steepening = flattening. Default is False (i.e. steepening).

    Returns
    -------
    out : ndarray, 2d
         Steepened array.

    """

    # filter array before computing gradients
    if sigma_radius:
        xf = np.zeros(x.shape)
        scipy.ndimage.gaussian_filter(x,
                                      sigma=sigma_radius * max(x.shape),
                                      output=xf)
    else:
        xf = x

    dx, dy = vt.op.gradient(xf)

    if reverse:
        # "flattening"
        dx = -dx
        dy = -dy

    if elevation_scaling:
        zscale = elevation_scaling * vt.op.remap(x, vmin=0, vmax=1)
    else:
        zscale = 1

    return vt.op.warp2d(x, wx=dx * zscale, wy=dy * zscale, scale=scale)


@vt.logger.show_input(log)
def steps(x, nsteps):
    """
    Quantify values of ndarray to reduced them to 'nsteps'.
    
    Parameters
    ----------
    x : ndarray
        Input array
    nsteps : integer
        Number of steps.

    Returns
    -------
    out : ndarray
        Stepped array.

    """

    return np.round(x * nsteps) / nsteps


@vt.logger.show_input(log)
def subsample(x, subsampling):
    """
    Subsample a 2d-array.

    Parameters
    ----------
    x : ndarray
        Input array.
    subsampling : int
        Keep every "subsampling" value.

    Returns
    -------
    out : ndarray
        Subsampled array.

    """

    y = scipy.ndimage.maximum_filter(x,
                                     size=(subsampling, subsampling),
                                     mode='reflect')
    return y[::subsampling, ::subsampling]


@vt.logger.show_input(log)
def truncate(x, vmin, vmax, reverse=True):
    """
    Truncate array values outside the range [vmin, vmax] by replacing
    those values by NaNs.

    Parameters
    ----------
    x : ndarray
        Input array.
    vmin : float
        Low bound of array.
    vmax : float
        High bound of array.
    reverse : bool, optional
        If set to True, values inside [vmin, vmax] are replaced by NaNs
        (instead of the oppposit. Default is False.

    Returns
    -------
    out : ndarray
        The truncated array.

    """

    x[np.where(x < vmin)] = np.nan
    x[np.where(x > vmax)] = np.nan
    return x


@vt.logger.show_input(log)
def warp2d(x, wx, wy=None, scale=0.1):
    """
    Warp (translate) values of ndarray.
    
    Parameters
    ----------
    x : ndarray
        Input array.
    wx : ndarray
        Translation following 'x' direction, array is normalized
        between 0 and 1 (= domain size).
    wy : ndarray, optional
        Translation following 'y' direction. Default is None, leading
        to wy = wx.
    scale : float, optional
        Translation scale between 0 and 1 (= domain size). Default is 0.1.

    Returns
    -------
    out : ndarray
        Warped array.

    """

    # translation wrapper...
    def func(x, dx, dy):
        i = int(x[0])
        j = int(x[1])
        return x[0] - dx[i, j], x[1] - dy[i, j]

    if wy is None:
        wy = wx

    wx_min = wx.min()
    wx_max = wx.max()
    wx_ptp = wx.ptp()

    if wx_min != wx_max:
        wdx = np.nan_to_num(
            scale * remap(wx, wx_min / wx_ptp, wx_max / wx_ptp) * max(x.shape))
    else:
        wdx = np.ones(x.shape) * max(x.shape)

    wy_min = wy.min()
    wy_max = wy.max()
    wy_ptp = wy.ptp()

    if wy_min != wy_max:
        wdy = np.nan_to_num(
            scale * remap(wy, wy_min / wy_ptp, wy_max / wy_ptp) * max(x.shape))
    else:
        wdy = np.ones(x.shape) * max(x.shape)

    xt = scipy.ndimage.geometric_transform(input=x,
                                           mapping=func,
                                           extra_arguments=(wdx, wdy),
                                           mode='reflect',
                                           order=5)
    return xt
