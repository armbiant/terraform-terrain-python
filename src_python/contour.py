"""Functions for countour.
"""

import numpy as np
import os
import scipy
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


def nearest_neighbor_search_2d(x, y, i_start=None):
    """
    Nearest neighbor search algorithm in 2d.
    
    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    istart : int, optional
        Index of the starting point. Default is None, the point with the
        largest radius is used as a starting point.
    """

    if i_start is None:
        i_start = np.argmax(x**2 + y**2)

    xp = [x[i_start]]
    yp = [y[i_start]]

    x = np.delete(x, i_start)
    y = np.delete(y, i_start)

    while len(x) > 0:
        r2 = (x - xp[-1])**2 + (y - yp[-1])**2
        i = np.argmin(r2)
        xp.append(x[i])
        yp.append(y[i])

        x = np.delete(x, i)
        y = np.delete(y, i)

    x = np.array(xp)
    y = np.array(yp)

    return np.array(xp), np.array(yp)


def divide_2d(x, y, division=2):
    """
    Subdivise a contour defined in a 2d-plane by a series of nodes (x, y).
    
    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    division : float, optional
        Subdivision. Default is 2.
        
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the divided contour points.
    out : ndarray, 1d, float
        'y' location of the divided contour points.
    out : ndarray, 1d, int
        Indices points corresponding to the input points.
    """

    nx = x.shape[0]

    distance = np.zeros(nx - 1)
    for i in range(nx - 1):
        distance[i] = ((x[i + 1] - x[i])**2 + (y[i + 1] - y[i])**2)**0.5
    divide_step = distance[np.where(distance > 0)].min() / division

    xd = []
    yd = []
    # index of original contour points
    idx_input_nodes = [0]
    for i in range(nx - 1):
        alpha = np.arctan2(y[i + 1] - y[i], x[i + 1] - x[i])

        t = np.linspace(0,
                        distance[i],
                        int(np.floor(distance[i] / divide_step)),
                        endpoint=False)
        xd += (x[i] + t * np.cos(alpha)).tolist()
        yd += (y[i] + t * np.sin(alpha)).tolist()

        idx_input_nodes.append(idx_input_nodes[-1] + len(t))

    xd.append(x[-1])
    yd.append(y[-1])

    return np.array(xd), np.array(yd), np.array(idx_input_nodes)


def fractalize_segments_2d(x, y, iterations, sigma, mu):
    """
    'Fractalize' a series of segments defined in a 2d-plane by subdividing the
    contour and adding a random Gaussian displacement to the new points.
    
    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    iterations : int
        Number of iterations (e.g. subdivision + random displacement) applied
        to the original contour. Default is 8.
    sigma : float, optional
        Half-width of the random Gaussian displacement, normalized by the
        distance between points.
    mu : float, optional
        Mean of the random Gaussian displacement, normalized by the
        distance between points.
    
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the divided contour points.
    out : ndarray, 1d, float
        'y' location of the divided contour points.
    """

    for _ in range(iterations):
        nx = x.shape[0]

        xnew = np.empty(2 * nx - 1)
        ynew = np.empty(2 * nx - 1)

        xnew[::2] = x
        ynew[::2] = y

        amp = np.random.normal(mu, sigma, nx - 1)

        for i in range(nx - 1):
            d = (y[i + 1] - y[i]) / (x[i + 1] - x[i])
            dx = -d
            dy = 1
            norm = (dx**2 + dy**2)**0.5
            dx = dx / norm
            dy = dy / norm

            distance = ((x[i + 1] - x[i])**2 + (y[i + 1] - y[i])**2)**0.5

            xnew[2 * i] = x[i]
            ynew[2 * i] = y[i]

            xnew[2 * i + 1] = (x[i] + x[i + 1]) / 2 + distance * amp[i] * dx
            ynew[2 * i + 1] = (y[i] + y[i + 1]) / 2 + distance * amp[i] * dy

        x = xnew
        y = ynew

    return x, y


def fractalize_2d(x,
                  y,
                  iterations=8,
                  sigma=0.3,
                  mu=0,
                  interpolated_output=False):
    """
    'Fractalize' a contour defined in a 2d-plane by subdividing the contour and
    adding a random Gaussian displacement to the new points.
    
    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    iterations : int
        Number of iterations (e.g. subdivision + random displacement) applied
        to the original contour. Default is 8.
    sigma : float, optional
        Half-width of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.2.
    mu : float, optional
        Mean of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.
    interpolated_output : bool, optional
        Re-interpolate x and y to get a constant distance between points.
        Default is False (no interpolation).
    
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the divided contour points.
    out : ndarray, 1d, float
        'y' location of the divided contour points.
    """

    # 1st pass - adjust number of iteration to get a fairly constant distance
    # between each points
    _, _, idx = divide_2d(x, y, division=1)

    xnew = []
    ynew = []
    for i in range(x.shape[0] - 1):
        nit = int(np.floor((idx[i + 1] - idx[i])**0.5))
        xd, yd = fractalize_segments_2d(x[i:i + 2],
                                        y[i:i + 2],
                                        iterations=nit,
                                        sigma=sigma,
                                        mu=mu)
        xnew += xd[:-1].tolist()
        ynew += yd[:-1].tolist()

    xnew.append(x[-1])
    ynew.append(y[-1])

    x = np.asarray(xnew)
    y = np.asarray(ynew)

    # 2nd pass
    for it in range(iterations - 1):
        x, y = fractalize_segments_2d(x, y, iterations=1, sigma=sigma, mu=mu)

    # if requested, re-interpolate x and y to get a constant distance
    # between points
    if interpolated_output:
        nx = x.shape[0]

        # cumulative distance
        d = np.zeros(nx)
        for i in range(1, nx):
            d[i] = d[i - 1] + (x[i] - x[i - 1])**2 + (y[i] - y[i - 1])**2
        delta_min = np.diff(d).min()
        ditp = np.linspace(0, d.max(), int(1 / delta_min))

        fitp = scipy.interpolate.interp1d(d, x)
        x = fitp(ditp)

        fitp = scipy.interpolate.interp1d(d, y)
        y = fitp(ditp)

    return x, y


@vt.logger.show_input(log)
def map_to_xy(mask, discretization_density=0.05):
    """
    Convert a mask to a contour (= list of points).

    Parameters
    ----------
    mask : ndarray
        Mask array.
    discretization_density : float, optional
        'Azimuthal' discretization density, in [0, 1]. Increase this parameter
        to capture more details of the mask contours, but at the cost of having
        more control points in the end.

    Returns
    -------
    xc_list : list of 1d ndarray, float
        'x' coordinates of the contours. Each contour is separated.
    yc_list : list of 1d ndarray, float
        'y' coordinates of the contours. Each contour is separated.

    """

    mask_label, nlabels = scipy.ndimage.label(mask)
    xc_list = []
    yc_list = []

    if nlabels == 1:
        log.warning('could not find any shape')

    else:
        # (the 1st label corresponds to the background, just skip it)
        for v in np.unique(mask_label)[1:]:
            xy = scipy.ndimage.center_of_mass(mask_label, mask_label, v)

            mask_v = np.where(mask_label == v, 1, 0)

            # reconstruct a simplified contour defined as a series of
            # xy coordinates
            area = np.sum(mask_v)
            radius = (area / np.pi)**0.5
            circonf = 2 * np.pi * radius

            npt = int(circonf * discretization_density)

            # if a surface (at least a triangle) cannot be built from
            # the contour, just skip it
            if npt > 2:
                theta = np.linspace(1, 2 * np.pi, npt)

                xc = []
                yc = []

                for th in theta:
                    r = np.arange(1, 2 * radius, 1)
                    i = (xy[0] + r * np.cos(th)).astype(int)
                    j = (xy[1] + r * np.sin(th)).astype(int)

                    # keep indices within the array bounds
                    i = np.minimum(np.maximum(0, i), mask_v.shape[0] - 1)
                    j = np.minimum(np.maximum(0, j), mask_v.shape[1] - 1)

                    first_zeros = np.where(mask_v[i, j] == 0)
                    if first_zeros[0].shape[0]:
                        idx = max(0, first_zeros[0][0] - 1)
                    else:
                        idx = -1

                    xc.append(i[idx])
                    yc.append(j[idx])

                # close contour
                xc.append(xc[0])
                yc.append(yc[0])

                xc_list.append(np.asarray(xc))
                yc_list.append(np.asarray(yc))

    return xc_list, yc_list


def xy_to_map(x, y, shape, filled=True, filling_value=1, extent=[0, 1, 0, 1]):
    """
    Contour to a 2d map a contour defined in a 2d-plane by a series of
    nodes (x, y).
    
    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    shape : (int, int)
        Output map array shape.
    filled : bool, optional
        Default is True.
    filling_value : float, optional
        Value used to fill the contour. Default is 1.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 2d, float
        Resulting 2d array.
    """

    w = np.zeros(shape)

    # remove points outside domain
    idx = np.where((x >= extent[0]) & (x <= extent[1]) \
                 & (y >= extent[2]) & (y <= extent[3]))
    x = x[idx]
    y = y[idx]

    if len(x) > 1:
        # convert to indices
        xs = (x - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1)
        ys = (y - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1)

        # subdivise to avoid "holes" in the contour
        division = np.ceil(
            max(np.abs(np.diff(xs)).max(),
                np.abs(np.diff(ys)).max()))
        xs, ys, _ = divide_2d(xs, ys, division=division)

        # fill map
        w[xs.astype(int), ys.astype(int)] = filling_value

        if filled:
            w[scipy.ndimage.binary_fill_holes(w)] = filling_value

    return w
