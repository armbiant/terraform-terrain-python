"""Heightmap generation and tweaking.
"""

import copy
import numpy as np
import os
import scipy
import tps
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def alter(z,
          idx,
          new_elevations,
          kernel=None,
          method='adaptive_kernel',
          kernel_scaling=1,
          kernel_shape=None,
          **kwargs):
    """
    Point-wise alteration: locally enforce a new elevation value while
    maintaining the 'shape' of the heightmap.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    idx : ndarray
        List of (i,j) indices where the elevation is to be altered.
    new_elevations : ndarray or float
        List of elevation values that are to be enforced at (i,j) locations,
        1-d array, same length at idx.
    kernel : ndarray of function, optional
        Kernel function: from a given 'shape', returns and ndarray of shape
        'shape' with kernel values. Default is a smooth cosine
        (see ``vt.kernel.smooth_cosine``).
    method: str, optional
        Method used, can be 'fixed_kernel' (can yield repetitive artifacts)
        or 'adaptive_kernel' (better blending). Default is 'adaptive_kernel'.
    kernel_scaling : float, optional
        Kernel shape scaling with alteration intensity (should be > 0, the
        greater the larger the alterations footprint). Default is 1.
    kernel_shape : (int, int), optional
        Kernel shape for the 'fixed_kernel' method. Default: set to 1/4th of
        the heightmap shape.
    kwargs, optional
        Any additional input arguments that will be passed to the
        kernel function.

    Returns
    -------
    out : ndarray
        Altered array.

    """

    if type(idx) is list:
        idx = np.array(idx, dtype=int).reshape(len(idx), 2)

    if type(new_elevations) is not np.ndarray:
        new_elevations *= np.ones(idx.shape[0])

    if kernel is None:
        kernel = vt.kernel.smooth_cosine

    if method == 'fixed_kernel':
        # --- convolution-based

        if kernel_shape is None:
            n = max(1, int(max(z.shape) / 4))
            kernel_shape = (n, n)

        # build input for convolution product
        w = np.zeros(z.shape, dtype=np.float32)
        w[idx[:, 0], idx[:, 1]] = new_elevations - z[idx[:, 0], idx[:, 1]]
        wmin = w.min()
        wmax = w.max()

        # convolve and rescale output
        w = scipy.signal.fftconvolve(w, kernel(shape=kernel_shape, **kwargs))
        w = vt.op.remap(w, wmin, wmax)

        return z + w

    elif method == 'adaptive_kernel':
        # --- 'sum'-based

        zptp = z.ptp()
        nz = max(z.shape)
        w_positive = np.zeros(z.shape)
        w_negative = np.zeros(z.shape)
        for i, j, elv in zip(idx[:, 0], idx[:, 1], new_elevations):

            # shape factor
            sf = np.zeros(z.shape)

            if abs(z[i, j]) > 0:

                # kernel size based on aleration intensity, but still keep
                # things under control...
                nk = int(abs(elv - z[i, j]) / zptp * nz * kernel_scaling)
                nk = max(2, min(nz, nk))

                # compute shape factor by summing (the kernel is varying,
                # convolution product is not possible)
                k = kernel(shape=(nk, nk), **kwargs)
                k = vt.op.remap(k, vmin=0, vmax=1)

                # add kernel
                sf = vt.op.add_array(sf, i, j, k)
                sf = sf * z / abs(z[i, j])

            else:
                pass

            if elv - z[i, j] > 0:
                w_positive = np.maximum(w_positive, (elv - z[i, j]) * sf)
            elif elv - z[i, j] < 0:
                w_negative = np.minimum(w_negative, (elv - z[i, j]) * sf)

        return z + w_positive + w_negative


@vt.logger.show_input(log)
def basalt_collonade(z, radius, noise=0.3, seed=-1):
    """
    Transform heightmap into basalt collonades.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    radius : float
        Hexagon radius (assuming the domain is a unit square).
    noise : float, optional
        Noise ratio in [0, 1].
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray
        Transformed heightmap.

    """

    w = np.copy(z)

    extent = [-2 * radius, 1 + 2 * radius, -2 * radius, 1 + 2 * radius]

    xn, yn = vt.grid.random_grid_jittered(radius,
                                          radius,
                                          scale=noise,
                                          staggered=True,
                                          seed=seed,
                                          extent=extent)

    mask = vt.noise.voronoi2d(w.shape, xn, yn, cell_filling=True)

    for v in np.unique(mask):
        idx = np.where(mask == v)
        w[idx] = np.median(w[idx])

    return w


@vt.logger.show_input(log)
def depression_filling(z, iterations=1000, eps=1e-3):
    '''
    Fill the depressions of the heightmap.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    iterations : int
        Maximum number of iterations. Default is 1000.
    eps : float
        Slope of the 'epsilon-descending path'. Default is 1e-3.

    Returns
    -------
    out : ndarray
        Altered array.

    References
    ----------
    - Planchon, O. and Darboux, F. 2002. A fast, simple and versatile
      algorithm to fill the depressions of digital elevation
      models. CATENA 46, 159–176.

    '''

    return vterrain_core.depression_filling(z=z,
                                            iterations=iterations,
                                            eps=eps)


@vt.logger.show_input(log)
def dig_crater(z,
               ic,
               jc,
               radius,
               depth,
               dig_kernel_ratio=0.5,
               inner_radius_ratio=0.5,
               noise_ratio=0.5,
               dig_kernel=None,
               seed=-1):
    """
    Dig a crater into a given heightmap.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    ic : int
        'i' index of the crater center.
    jc : int
        'j' index of the crater center.
    radius : float
        Crater radius (domain is supposed to be unit-square).
    depth : float
        Crater depth (approximative), with respect to the crater borders.
    dig_kernel_ratio : float, optional
        Size ratio between the crater radius and the radius of the kernel used
        for digging. Default is 0.5.
    inner_radius_ratio : float, optional
        Ratio between the crater outer radius (border) and its innner radius
        (flat inner region). Default is 0.5.
    noise_ratio : float, optional
        Noise amplitude ratio. Default is 0.5.
    dig_kernel : ndarray, optional
        Digging kernel. If set to None, a smooth disk kernel is used. Default
        is None.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).

    Returns
    -------
    out : ndarray
        Heightmap with crater.

    """

    # define crater region and digging kernels
    ir = int(radius * max(z.shape))
    ik = int(dig_kernel_ratio * ir)

    cr_kernel = vt.kernel.smooth_disk((2 * ir + 1, 2 * ir + 1),
                                      radius=inner_radius_ratio)
    cr_kernel /= cr_kernel.max()

    if dig_kernel is None:
        dig_kernel = vt.kernel.biweight((ik, ik))

    dig_kernel = dig_kernel / np.max(dig_kernel)

    # min and max elevation in the crater region (before crater carving)
    zmin = ((1 - cr_kernel) *
            z[ic - ir:ic + ir + 1, jc - ir:jc + ir + 1]).max()
    zmax = z[ic - ir:ic + ir + 1, jc - ir:jc + ir + 1].max()

    # digging amplitude to get to the bottom of the crater
    amp = (zmax - zmin) + depth

    # convolution weights
    if seed > 0:
        rng = np.random.default_rng(seed)
        noise = rng.random((2 * ir + 1, 2 * ir + 1))
    else:
        noise = np.random.rand(2 * ir + 1, 2 * ir + 1)

    w = np.zeros(z.shape)
    w[ic - ir:ic + ir + 1, jc - ir:jc + ir +
      1] = cr_kernel * (1 - noise_ratio + noise_ratio * noise)

    # digging amplitude map
    w = scipy.ndimage.convolve(w, dig_kernel)

    return z - amp * w / w.max()


@vt.logger.show_input(log)
def generate_plate_pressure_map(motion_vector_x,
                                motion_vector_y,
                                plate_transition_width=0.1):
    """
    Generate tectonic plate pressure map based on plate motion vectors.

    Parameters
    ----------
    motion_vector_x : ndarray
        Input map as a 2-dimensional array: 'x'-component of plate motion.
    motion_vector_y : ndarray
        Input map as a 2-dimensional array: 'y'-component of plate motion.
    plate_transition_width : float, optional
        Half width of the transition between two plates, between [0, 1] (set
        to 1 corrsponds to the whole domain). Default is 0.1.

    Returns
    -------
    out : ndarray
        Pressure map, positive values.
    out : ndarray
        Pressure map, negative values.

    """

    nsize = max(3, int(plate_transition_width * max(motion_vector_x.shape)))

    # simplified 1d-kernel
    t = np.linspace(-1, 1, nsize)
    k = t * np.exp(-t**2 / 0.5**2 / 2)
    k = k / np.sum(k)

    w = scipy.signal.fftconvolve(motion_vector_x,
                                 k.reshape((nsize, 1)),
                                 mode='same')
    w += scipy.signal.fftconvolve(motion_vector_y,
                                  k.reshape((1, nsize)),
                                  mode='same')

    w_pos = copy.deepcopy(w)
    w_neg = copy.deepcopy(w)

    w_pos[np.where(w < 0)] = 0
    w_neg[np.where(w > 0)] = 0

    # normalize fields but while keeping the amplitude ratio between
    # positive and negative
    if w_pos.max() > -w_neg.min():
        vmax_pos = 1
        vmax_neg = -w_neg.min() / w_pos.max()
    else:
        vmax_pos = -w_pos.max() / w_neg.min()
        vmax_neg = 1

    w_pos = vt.op.remap(w_pos, vmin=0, vmax=vmax_pos)
    w_neg = vt.op.remap(w_neg, vmin=-vmax_neg, vmax=0)

    return w_pos, w_neg


@vt.logger.show_input(log)
def generate_ridge(shape,
                   x,
                   y,
                   extent=[0, 1, 0, 1],
                   fractal_iterations=0,
                   fractal_sigma=0.2,
                   fractal_mu=0,
                   kernel=None,
                   radius=0.02):
    """
    Generate a ridge based on a set of reference points (x, y). The nodes
    are connected using a minimal spanning tree.

    Parameters
    ----------
    shape : tuple of integers
        Shape of the resulting nd array. The array representes a domain
        bounded by the intervals x in [0, 1] and y in [0, 1].
    x : ndarray 1-dimensional
        "x" location of the reference points.
    y : ndarray 1-dimensional
        "y" location of the reference points.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
    fractal_iterations : int
        Number of iterations (e.g. subdivision + random displacement) applied
        to the original contour. Default is 0.
    fractal_sigma : float, optional
        Half-width of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.2.
    fractal_mu : float, optional
        Mean of the random Gaussian displacement, normalized by the
        distance between points. Default is 0.
    kernel : optional
        Smoothing kernel, can be None, 'gaussian' or a user-defined kernel as
        an ndarray. Default is None.
    radius : float, optional
        Kernel radius (assuming the domain is a unit square) for the Gaussian
        smoothing. Default is 0.02.

    Returns
    -------
    out : ndarray
        Interpolated heightmap.

    """

    nodes = np.dstack((x, y))[0]
    tri = scipy.spatial.Delaunay(nodes)

    adj = np.zeros((len(nodes), len(nodes)))
    for idx in tri.simplices:
        for i in range(2):
            adj[idx[i], idx[i + 1]] = np.sum(
                (nodes[idx[i + 1], :] - nodes[idx[i], :])**2)
    adj = np.triu(np.maximum(adj, adj.T))

    csr = scipy.sparse.csgraph.minimum_spanning_tree(adj)
    pe, qe, _ = scipy.sparse.find(csr)

    w = np.zeros(shape)
    adjacency_matrix = np.zeros((len(x), len(y)))
    for p, q in zip(pe, qe):
        xs = np.asarray([nodes[p, 0], nodes[q, 0]])
        ys = np.asarray([nodes[p, 1], nodes[q, 1]])

        xs, ys = vt.contour.fractalize_segments_2d(
            xs,
            ys,
            iterations=fractal_iterations,
            sigma=fractal_sigma,
            mu=fractal_mu)

        w = np.maximum(w, vt.contour.xy_to_map(xs, ys, shape, extent=extent))

        adjacency_matrix[p, q] = ((xs[1] - xs[0])**2 + (ys[1] - ys[0])**2)**0.5

    # transform to upper triangular matrix
    adjacency_matrix = np.triu(np.maximum(adj, adj.T))

    # kernel convolution
    if not (kernel is None):
        if kernel == 'gaussian':
            w = scipy.ndimage.gaussian_filter(w,
                                              sigma=radius * max(shape),
                                              output=w)
        else:
            w = scipy.signal.fftconvolve(w, kernel, mode='same')

    return vt.op.remap(w, vmin=0, vmax=1), np.dstack((pe, qe))[0]


@vt.logger.show_input(log)
def generate_thinplate(shape, x, y, z):
    """
    Generate an heightmap based on a set of reference points (x, y, z) by
    solving the thin plate spline interpolation.

    Parameters
    ----------
    shape : tuple of integers
        Shape of the resulting nd array. The array representes a domain
        bounded by the intervals x in [0, 1] and y in [0, 1].
    x : ndarray 1-dimensional
        "x" location of the reference points.
    y : ndarray 1-dimensional
        "y" location of the reference points.
    z : ndarray 1-dimensional
        "z" elevation of the reference points.

    Returns
    -------
    out : ndarray
        Interpolated heightmap.

    """

    # array tweaking to fit 'tps' module requirements
    xy = np.dstack((x, y))[0]
    z = z[:, np.newaxis]

    itp = tps.ThinPlateSpline(alpha=0)
    itp.fit(xy, z)

    xg, yg = np.meshgrid(np.linspace(0, 1, shape[0]),
                         np.linspace(0, 1, shape[1]),
                         indexing='ij')
    xyg = np.dstack((xg.ravel(), yg.ravel()))[0]
    itp.transform(xyg).reshape(shape)

    return itp.transform(xyg).reshape(shape)


@vt.logger.show_input(log)
def outcrops(shape,
             radius=0.2,
             density=0.2,
             gamma=0.2,
             cracks_depth=0.1,
             cracks_width=0.01,
             cracks_slope=0.01,
             cracks_noise=0.2,
             mask=None,
             seed=-1):
    """
    Create outcrops heightmap.

    Parameters
    ----------
    shape : tuple of integers
        Shape of the resulting nd array.
    radius : float, optional
        Outcrop typical scale. Default is 0.2.
    density : float, optional
        Outcrop spatial density, in [0, 1]. Default is 0.2.
    gamma : float, optional
        Gamma correction factor. Default is 0.2.
    cracks_depth : float, optional
        Crack depth. Default is 0.1.
    cracks_width : float, optional
        Crack width. Default is 0.01.
    cracks_slope : float, optional
        Crack slope. Default is 0.01.
    cracks_noise : float, optional
        Crack displacement noise. Default is 0.2.
    mask : ndarray or function
        Mask defining the outcrops silhouette. Can be an ndarray or a noise function
        with parameters 'shape', 'res' and 'seed. Default is None (=> Worley based factal).
        Use 'lambda' to use functions with different parameters.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray
        Outcrop elevation map, in [0, 1].   

    """

    # rock base rugosity
    wc = vt.noise.diamond_square2d(shape, seed=seed)
    wc -= vt.op.smooth_gaussian(wc, sigma_radius=2 * radius)
    wc = vt.op.remap(wc, 0, 1)

    # rock shapes
    nr = int(1 / radius)

    if seed > 0: seed += 1
    wc *= vt.noise.fractal2d(shape=shape,
                             res=(nr, nr),
                             seed=seed,
                             noise_fct=vt.noise.worley2d)

    # add cracks
    if seed > 0: seed += 1
    cracks = 1 - vt.noise.cracks(shape=shape,
                                 res=2 * nr**2,
                                 width=cracks_width,
                                 transition_radius=cracks_slope,
                                 noise_displacement=cracks_noise,
                                 seed=seed)
    wc -= cracks_depth * cracks

    # mask
    if seed > 0: seed += 1
    if mask is None:
        mask = vt.noise.fractal2d(shape, (nr, nr),
                                  seed=seed,
                                  noise_fct=vt.noise.worley2d)

    if callable(mask):
        mask = mask(shape=shape, res=(nr, nr), seed=seed)

    vmin = np.percentile(mask.ravel(), 100 * (1 - density))
    mask = vt.op.clamp(mask, vmin=vmin, vmax=1)
    mask = vt.op.remap(mask)

    # give a 'bumpy' look to the transitions
    mask = vt.op.gamma_correction(mask, gamma=gamma)
    wc *= mask

    # normalized maximum (or minimum) to 1
    norm = max(abs(wc.max()), abs(wc.min()))
    wc /= norm

    return wc


@vt.logger.show_input(log)
def rocks(shape, radius=0.1, density=0.2, gamma=0.5, seed=-1):
    """
    Generate a 'rocky' heightmap.

    Parameters
    ----------
    shape : tuple of integers
        Shape of the resulting nd array.
    radius : float, optional
        Outcrop typical scale. Default is 0.2.
    density : float, optional
        Outcrop spatial density, in [0, 1]. Default is 0.2.
    gamma : float, optional
        Gamma correction factor (> 1 for sharp rocks, < 1 for round boulder-like
        rocks). Default is 0.5.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray
        Rock elevation map, in [0, 1].   

    """

    w = np.zeros(shape)

    nr = int(1 / radius)
    w = vt.noise.fractal2d(shape=shape,
                           res=(nr, nr),
                           seed=seed,
                           noise_fct=vt.noise.perlin2d)

    vmin = np.percentile(w.ravel(), 100 * (1 - density))
    w = vt.op.clamp(w, vmin=vmin, vmax=1)
    w = vt.op.remap(w)

    w = vt.op.gamma_correction(w, gamma=gamma)

    return w


@vt.logger.show_input(log)
def stratify(z,
             n_layers=4,
             gamma=0.5,
             noise_z=0.1,
             noise_gamma=0.1,
             seed=-1,
             skip_first_layer=True,
             post_filtering=None):
    """
    Stratify the heightmap by creating a series of layers with a specified shape.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    n_layers : int or (int, int), optional
        Number of layers. If two values are provided as a list, the number of layers
        is randomly chosen within this interval. Default is 4.
    noise_z : float, optional
        Noise ratio, in [0, 0.5], applied to the layer elevations. Default is 0.1.
    noise_gamma : float, optional
        Noise gamma applied to the gamma correction. Default is 0.1.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    skip_first_layer : bool, optional
        Do not apply gamma correction to the first layer (to preserve base ground
        shape). Default is True.
    post_filtering : bool, optional
        Apply Gaussian smoothing as a post-processing step to remove gradient
        discontinuities at the bottom of convexe strates. Default is None: if the
        input array is not 2D => no filtering, if gamma < 1 => filtering of the convexe
        strates, if gamma > 1 => no filtering of the concave strates.

    Results
    -------
    out : ndarray
        Stratified heightmap.

    """

    if seed < 0:
        rng = np.random.default_rng()
    else:
        rng = np.random.default_rng(seed)

    if type(n_layers) == list:
        n_layers = n_layers[0] + int(
            (n_layers[1] - n_layers[0]) * rng.random())

    # define layer elevations
    hs = np.linspace(z.min(), z.max(), n_layers + 1)
    dh = np.diff(hs).min()
    hs[1:-1] += noise_z * dh * (2 * rng.random(n_layers - 1) - 1)

    # create strates
    zs = np.zeros(z.shape)
    is_first_layer = True

    for i in range(len(hs) - 1):
        h1, h2 = hs[i:i + 2]

        idx = np.where((z >= h1) & (z <= h2))
        if len(idx[0]):

            if is_first_layer and skip_first_layer:
                # skip this layer if requested
                is_first_layer = False
                zs[idx] = z[idx]

            else:
                g = gamma * (1 + noise_gamma * 2 * (rng.random() - 1))
                g = max(1e-2, g)
                zs[idx] = vt.op.gamma_correction(z[idx], gamma=g)

    if len(z.shape) < 2:
        post_filtering = False

    else:
        if post_filtering is None:
            if gamma > 1:
                post_filtering = False
            else:
                post_filtering = True

    if post_filtering:
        zs = scipy.ndimage.gaussian_filter(zs, sigma=0.5)

    return zs, hs


@vt.logger.show_input(log)
def stratify_multiscale(z,
                        layers_param=None,
                        partition=(4, 4),
                        transition_radius=0.05,
                        post_filtering=True,
                        seed=-1):
    """
    Multiscale stratification by a series of strates and sub-strates, and so on.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    layers_param : list of dict
        Parameters of each layers as a list of dictionnaries (see arguments of
        vt.hmap.stratify). Default is a 3-layer model.
    partition : None, (int, int) or ndarray, optional
        Domain partitioning: stratification is applied separately to each partition
        and random parameters may be different for each partition. If set to None:
        no partition, if set to a tuple of integers: a cell-based noise is used to
        partition the domain with wavenumbers provided by the tuple of integers. A
        label array can be provided (partitions are defined by constant values).
        Default is (4, 4).
    transition_radius : float, optional
        Tansition radius between partitions, with respect to the domain size.
        Default is 0.05.
    post_filtering : bool, optional
        Apply Gaussian smoothing as a post-processing step to remove gradient
        discontinuities. Default is True.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    Results
    -------
    out : ndarray
        Stratified heightmap.

    """

    if layers_param is None:
        layers_param = [
            {
                'n_layers': 3,
                'gamma': 0.4,
                'skip_first_layer': True,
            },
            {
                'n_layers': [1, 4],
                'gamma': 0.7,
                'noise_z': 0.4,
                'skip_first_layer': False,
            },
            {
                'n_layers': [1, 4],
                'gamma': 0.5,
                'skip_first_layer': False,
            },
        ]

    zs = np.copy(z)

    # --- recursive wrapper
    def recursive_strat(zs, hs, i_layer, layers_param, seed):
        if i_layer < len(layers_param):
            hs = np.array(hs)

            for i in range(len(hs) - 1):
                if hs[i + 1] > hs[i]:
                    if seed > 0:
                        seed += 1

                    idx = np.where((zs >= hs[i]) & (zs <= hs[i + 1]))
                    zs[idx], sub_hs = vt.hmap.stratify(zs[idx],
                                                       seed=seed,
                                                       post_filtering=False,
                                                       **layers_param[i_layer])
                    zs = recursive_strat(zs, sub_hs, i_layer + 1, layers_param,
                                         seed)

        return zs

    # --- partitioning
    if partition is None:
        zs = recursive_strat(z, [z.min(), z.max()], 0, layers_param, seed)

    else:
        if type(partition) == tuple:
            mask_regions = vt.noise.worley2d(z.shape,
                                             partition,
                                             seed=seed,
                                             cell_filling=True)

        else:
            mask_regions = partition

        zs = np.zeros(z.shape)
        for v in np.unique(mask_regions):
            mask = np.where(mask_regions == v, 1.0, 0.0)

            if transition_radius is not None:
                mask = vt.op.smooth_gaussian(mask,
                                             sigma_radius=transition_radius)

            idx = np.where(mask > 1e-10)
            if len(idx[0]):
                zs[idx] += mask[idx] * recursive_strat(
                    z[idx], [z.min(), z.max()], 0, layers_param, seed)

    if post_filtering:
        zs = scipy.ndimage.gaussian_filter(zs, sigma=0.5)

    return zs


@vt.logger.show_input(log)
def viscous_flow(z,
                 h,
                 iterations=100,
                 gnu=0.01,
                 source_amp=1e-3,
                 dt=1,
                 tol=1e-4,
                 coarse_reshaping=None):
    """
    Viscous flow (lava, glacier) simulation on a given heightmap.

    Parameters
    ----------
    z : ndarray
        Input heightmap as a 2-dimensional array.
    h : ndarray
        Initial flow thickness (source distribution is 'source_amp' is not zero).
    iterations : int, optional
        Number of iterations. Default is 100.
    gnu : float, optional
        Ratio gravity / viscosity. Default is 0.01.
    source_amp : float, optional
        Source amplitude. Default is 0.001.
    dt : float, optional
        Initial time step (adaptive time stepping is used). Default is 1.
    tol : float, optional
        Adaptive time stepping tolerance. Default is 1e-4.
    coarse_reshaping : none or (int, int)
        Compute initial condition on a coarser grid (shape provided). Default is None.

    Returns
    -------
    out : ndarray
        Heightmap + flow thickness.
    out : ndarray
        Flow thickness.

    """

    # work on a smoothed heightmap to limit instabilities
    zs = np.copy(z)
    zs = vt.op.smooth_laplace(zs, sigma=0.2, iterations=10)

    # --- initial conditions on a coarse solver
    if coarse_reshaping is not None:
        log.debug('solution init on coarse solver')

        zr = vt.op.reshape_interp(zs, coarse_reshaping)
        hr = vt.op.reshape_interp(h, coarse_reshaping)

        hr = vterrain_core.viscous_flow(z=zr,
                                        h=hr,
                                        gnu=gnu,
                                        source_amp=source_amp,
                                        dt=dt,
                                        tol=tol,
                                        iterations=iterations)

        # project back to original resolution
        h = vt.op.reshape_interp(hr, h.shape)

        # just a few more iterations at fine resolution to clean-up
        # the solution
        iterations = 1000

    # --- standard solver
    h = vterrain_core.viscous_flow(z=zs,
                                   h=h,
                                   gnu=gnu,
                                   source_amp=source_amp,
                                   dt=dt,
                                   tol=tol,
                                   iterations=iterations)

    return z + h, h


def angle_to_talus(angle, shape, extent=[0, 1, 0, 1]):
    """
    Convert a slope angle (in degrees) to a 'talus' value, i.e. the elevation
    difference between two heightmap cells.

    Parameters
    ----------
    angle : float or ndarray
        Slope angle in degree.
    shape : (int, int)
        Heightmap array shape.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax).
        Default is [0, 1, 0, 1].

    Returns
    -------
    talus : float or ndarray
        Talus value(s).

    """

    # the talus is computed for each direction but we keep the
    # smallest one
    delta = min((extent[1] - extent[0]) / shape[0],
                (extent[3] - extent[2]) / shape[1])
    return np.tan(angle / 180 * np.pi) * delta
