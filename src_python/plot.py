import earthpy.spatial as es
import lic
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


def colorize(mask, color, alpha=1, desactivate_mask=False, flip=False):
    """
    Generate an image based on a mask and a color.
    
    Parameters
    ----------
    mask : ndarray, size (M, N)
        Colorizarion mask. No colorization when 'mask' <= 0.
    color : (R, G, B, A) or str (colormap name)
        Color in [0, 255] or a matplotlib colormap name.
    alpha : float or ndarray, optional
        Color alpha (overide the alpha channel of 'color'), can be locally
        defined as an array. Default is 1.
    desactivate_mask : bool, optional
        Desactivate masking. Can be usefull when 'color' is a colormap in
        order to plot all the image disregarding the value of the mask.
        Default is False.
    flip : bool
        Transpose the data. Default is False.
        
    Returns
    -------
    img : ndarray, size (M, N, 4)
        Output image with 4 channels (in [0, 255]).
    
    """

    # the output
    img = np.zeros((*mask.shape, 4))

    # create the colormap or a 'fake' one for solid color
    if type(color) == str:
        cmap = lambda x: 255 * matplotlib.cm.get_cmap(color)(x)
    else:
        cmap = lambda x: color * np.ones((*x.shape, 4))

    # alpha channel
    alpha *= 255
    if not (isinstance(alpha, np.ndarray)):
        alpha = alpha * np.ones(mask.shape)

    # the mask
    if not (desactivate_mask):
        idx = np.where(mask > 0)
    else:
        idx = np.where(mask != np.nan)
    ni = len(idx[0])

    # treat each channel separetely
    for ic in range(4):
        idx_i = (*idx, ic * np.ones(ni, dtype=np.int64))

        if isinstance(alpha, np.ndarray) and ic == 3:
            img[idx_i] = alpha[idx]
        else:
            img[idx_i] = cmap(mask)[idx_i]

    img = np.rot90(img)

    if flip:
        img.transpose(1, 0, 2)

    return img.astype(int)


def colorize_bivariate(x1, x2, colors, normalize_input=True, flip=False):
    """
    Generate an image of size (M, N) based on a pair 'x1', 'x2' of ndarrays
    of the same size. Colorization is based on 'x1' and 'x2', used as input
    for the bivariate colormap 'colors'.
    
    Parameters
    ----------
    x1 : ndarray, size (M, N)
        Colorizarion parameter #1.
    x2 : ndarray, size (M, N)
        Colorizarion parameter #2.
    colors : ndarray, size (P, Q, 4)
        Bivariate colormap: axes 0 corresponds to parameter #1 and axes 1 to
        parameters 2. The output color is defined by interpolating (x1, x2) on
        this color array.
    normalize_input : bool, optional
        Normalize input arrays between 0 and 1. Default is True.
    flip : bool
        Transpose the data. Default is False.
        
    Returns
    -------
    img : ndarray, size (M, N, 4)
        Output image, same size as x1 and x2, with 4 channels (in [0, 255]).
    
    """

    shape = x1.shape

    if normalize_input:
        x1 = (x1 - x1.min()) / (x1.max() - x1.min())
        x2 = (x2 - x2.min()) / (x2.max() - x2.min())

    img = np.zeros((*shape, 4))

    # interpolate each channel separately
    cx = np.linspace(0, 1, colors.shape[0])
    cy = np.linspace(0, 1, colors.shape[1])

    for ic in range(4):
        fitp = scipy.interpolate.RegularGridInterpolator((cx, cy), colors[:, :,
                                                                          ic])
        img[:, :, ic] = fitp((x1, x2))

    img = np.rot90(img)

    if flip:
        img.transpose(1, 0, 2)

    return img.astype(int)


def colormap_bivariate(name):
    """
    Generate pre-defined bivariate colormap arrays.

    Parameters
    ----------
    name : str
        Colormap name.

    Returns
    -------
    colors : ndarray, size (shape, 4)
        Bivariate colormap array.

    """

    if name == 'gray':
        xyc = np.array([
            [0, 0, 0, 0, 0, 255],  # black
            [1, 0, 255, 255, 255, 255],  # white
            [0, 1, 0, 0, 0, 255],  # black
            [1, 1, 0, 0, 0, 255],  # black
        ])

    elif name == 'DEM' or name == 'dem':
        xyc = np.array([
            [0, 0, 144, 2, 111, 255],
            [0.10, 0, 0, 0, 255, 255],
            [0.30, 0, 0, 255, 255, 255],
            [0.50, 0, 0, 255, 0, 255],
            [0.70, 0, 255, 255, 0, 255],
            [0.90, 0, 255, 0, 0, 255],
            [1, 0, 106, 15, 15, 255],
            [0, 1, 119, 136, 153, 255],
            [1, 1, 0, 0, 0, 255],
        ])

    elif name == 'terrain':
        xyc = np.array([
            [0, 0, 13, 206, 104, 255],  # green
            [0.33, 0, 229, 249, 147, 255],  # sand
            [0.66, 0, 134, 100, 87, 255],  # brown
            [1, 0, 255, 255, 255, 255],  # white
            [0, 0.2, 13, 206, 104, 255],  # green
            [0.33, 0.2, 229, 249, 147, 255],  # sand
            [0.66, 0.2, 134, 100, 87, 255],  # brown
            [1, 0.2, 255, 255, 255, 255],  # white
            [0, 1, 119, 136, 153, 255],  # light slate
            [1, 1, 47, 79, 79, 255],  # dark slate
        ])

    return colors_to_regulargrid(xyc)


def colors_to_regulargrid(xyc, shape=(8, 8)):
    """
    Generate a bivariate colormap defined on a regular grid by interpolating
    a set of colors provided at some coordinates.
    
    Parameters
    ----------
    xyc : ndarray, size (N, 6)
        Contains coordinates 'xy' (in [0, 1]) and color values (in [0, 255]),
        of the form [x, y, R, G, B, A].
    shape : (int, int)
        Output shape. Default is (8, 8).

    Returns
    -------
    colors : ndarray, size (shape, 4)
        Bivariate colormap array.

    """

    colors = np.zeros((*shape, 4))

    x, y = np.meshgrid(np.linspace(0, 1, shape[0]),
                       np.linspace(0, 1, shape[1]),
                       indexing='ij')

    # each channel is treated separately
    for ic in range(4):
        colors[:, :, ic] = scipy.interpolate.griddata(points=xyc[:, [0, 1]],
                                                      values=xyc[:, 2 + ic],
                                                      xi=(x, y),
                                                      method='linear')

    return vt.op.clamp(colors, vmin=0, vmax=255).astype(int)


def diff(z1,
         z2,
         cmap_arrays='jet',
         cmap_diff='jet',
         axis='off',
         origin='lower',
         flip=True):
    """
    Plot a comparison a two 2-dimensional arrays. Three subplots are
    produced: plot of the 1st array z1, plot of 2nd array z2, plot of the
    difference z1-z2.

    Parameters
    ----------
    z1 : ndarray
        Input array #1, 2-dimensional.
    z2 : ndarray
        Input array #2, 2-dimensional.
    cmap_arrays : string
        Colormap for the array plots. Default is 'jet'.
    cmap_diff : string
        Colormap for the difference plot. Default is 'jet'.
    origin : string
        See imshow: place the [0, 0] index of the array in the upper left
        or lower left corner of the Axes. Default is 'lower'.
    axis : string
        Define weather the axis are plotted. Default is 'off'.
    flip : bool
        Transpose the data to get an 'ij' similar to a xy plot. Default
        is True.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure (three subplots).

    """

    if flip:
        z1 = z1.T
        z2 = z2.T

    # switch between horizontal or vertical layout according
    # to underlying 2d array shape
    if np.argmax(z1.shape[0:2]) == 0:
        fig, axs = plt.subplots(1, 3)
    else:
        fig, axs = plt.subplots(3, 1)

    vmin = min(np.nanmin(z1), np.nanmin(z2))
    vmax = max(np.nanmax(z1), np.nanmax(z2))
    axs[0].imshow(z1, cmap=cmap_arrays, origin=origin, vmin=vmin, vmax=vmax)
    axs[1].imshow(z2, cmap=cmap_arrays, origin=origin, vmin=vmin, vmax=vmax)

    if z1.dtype == 'bool':
        axs[2].imshow(np.logical_xor(z1, z2), cmap=cmap_diff, origin=origin)
    else:
        axs[2].imshow(z1 - z2, cmap=cmap_diff, origin=origin)

    for i in range(3):
        axs[i].axis(axis)

    return fig, axs


def histograms(z, bins=20, cmap='gray_r', title=None, fname=None):
    """
    Plot histograms of the elevation and gradient norm. Three plots are
    produced: elevation histogram base on 'z', gradient norm histogram
    and a 2d-histogram crossing elevation and gradient norm.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    bins : int, optional
        Number of equal-width bins for the histograms. Default is 20.
    cmap : string, optional
        Colormap for the 2d central histogram. Default is 'gray_r'.
    title : string, optional
        Figure title. Default is None.
    fname : string; optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    # gradient norm
    dx, dy = np.gradient(z)
    g = (dx**2 + dy**2)**0.5

    # flatten arrays because this is a pure statistical analysis
    z = z.ravel()
    g = g.ravel()

    # fig/subplot layout
    fig = plt.figure()

    if not (title is None):
        fig.suptitle(title)

    NCOL = 3
    axs = NCOL * [None]
    axs[0] = plt.subplot2grid((NCOL, NCOL), (1, 0),
                              colspan=NCOL - 1,
                              rowspan=NCOL - 1)
    axs[1] = plt.subplot2grid((NCOL, NCOL), (0, 0), colspan=NCOL - 1)
    axs[2] = plt.subplot2grid((NCOL, NCOL), (1, NCOL - 1), rowspan=NCOL - 1)

    # heatmap 2d-histogram
    h, bz, bg = np.histogram2d(z, g, bins=(bins, bins))
    extent = [min(bz), max(bz), min(bg), max(bg)]

    axs[0].imshow(h.T, origin='lower', aspect='auto', extent=extent, cmap=cmap)
    axs[0].set_xlabel('elevation')
    axs[0].set_ylabel('gradient norm')

    # elevation histogram
    pdf = scipy.stats.gaussian_kde(z)
    t = np.linspace(min(bz), max(bz), 100)

    axs[1].hist(z,
                bins=bz,
                facecolor='k',
                alpha=0.3,
                edgecolor=None,
                density=True)
    axs[1].plot(t, pdf(t), 'k-', lw=1.5)

    axs[1].get_xaxis().set_visible(False)
    axs[1].get_yaxis().set_visible(False)
    axs[1].set_xlim([min(bz), max(bz)])

    # gradient norm histogram
    pdf = scipy.stats.gaussian_kde(g)
    t = np.linspace(min(bg), max(bg), 100)

    axs[2].hist(g,
                bins=bg,
                facecolor='k',
                alpha=0.3,
                orientation='horizontal',
                edgecolor=None,
                density=True)
    axs[2].plot(pdf(t), t, 'k-', lw=1.5)

    axs[2].get_xaxis().set_visible(False)
    axs[2].get_yaxis().set_visible(False)
    axs[2].set_ylim([min(bg), max(bg)])

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def blic(u,
         v,
         z=None,
         length=20,
         cmap='gray',
         origin='lower',
         axis='off',
         flip=True,
         title=None,
         fname=None,
         vmin=None,
         vmax=None,
         extent=None):
    """Plot the visualization of vector data using Line Integral Convolution (LIC).

    Parameters
    ----------
    u : ndarray
        Input array, arrow 'x' direction.
    v : ndarray
        Input array, arrow 'y' direction.
    z : ndarray, optional
        Input array, background data to generate the 'bump'. Default is None.
    cmap : string, optional
        Colormap for the plot. Default is 'gray'.
    origin : string, optional
        See imshow: place the [0, 0] index of the array in the upper left
        or lower left corner of the Axes. Default is 'lower'.
    axis : string, optional
        Define weather the axis are plotted. Default is 'off'.
    flip : bool, optional
        Transpose the data to get an 'ij' similar to a xy plot. Default
        is True.
    title : string, optional
        Figure title. Default is None.
    fname : string, optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).
    vmin, vmax : float, optional
        Colormap range. By default, the colormap covers the complete value
        range of the supplied data.
    extent : tuple of 4 floats, optional
        Extent of the resulting map (xmin, xmax, ymin, ymax). Default is None.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    References
    ----------
    - Sanna, A. and Montrucchio, B. 2000. Adding a scalar value to 2D
      vector field visualization: the BLIC (Bumped LIC). Eurographics
      2000 - Short Presentations, Eurographics Association.

    """

    if flip:
        u = u.T
        v = v.T
        if z is not None:
            z = z.T

    lic_img = lic.lic(u, v, length=length)

    if z is not None:
        hs = es.hillshade(z, azimuth=270, altitude=1)
        lic_img *= vt.op.remap(hs, vmin=0, vmax=1)

    fig = plt.figure()
    axs = plt.gca()
    plt.imshow(lic_img,
               cmap=cmap,
               origin=origin,
               vmin=vmin,
               vmax=vmax,
               extent=extent)
    axs.set_aspect('equal')
    plt.axis(axis)

    if not (title is None):
        fig.suptitle(title)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def network(x,
            y,
            adjacency_matrix,
            labels=None,
            axis='off',
            title=None,
            fname=None):
    """Plot the visualization of a network.

    Parameters
    ----------
    x : ndarray, 1d
        Node 'x' locations.
    y : ndarray, 1d
        Node 'y' locations.
    adjacency_matrix : ndarray, 2d
        Graph adjacency matrix.
    labels : list, optional
        Nodes label, by default the indice of the node is used. Default is None.
    axis : string, optional
        Define weather the axis are plotted. Default is 'off'.
    title : string, optional
        Figure title. Default is None.
    fname : string, optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    if labels is None:
        labels = range(len(x))

    fig = plt.figure()
    axs = plt.gca()

    for x_, y_, s_ in zip(x, y, labels):
        plt.plot(x_, y_, 'k.')
        plt.text(x_,
                 y_,
                 str(s_),
                 color='k',
                 fontsize=10,
                 ha='center',
                 va='center',
                 bbox={
                     'boxstyle': 'round',
                     'ec': (0.5, 0.5, 1.0),
                     'fc': (0.8, 0.8, 1.0),
                 })

    ps, qs = np.where(adjacency_matrix != 0)[:]

    for p, q in zip(ps, qs):
        xe = [x[p], x[q]]
        ye = [y[p], y[q]]

        if adjacency_matrix[p, q] > 0:
            color = 'k'
            lw = 1.5
        else:
            color = 'b'
            lw = 0.25

        plt.plot(xe, ye, ls='-', lw=lw, color=color)
        plt.text(np.mean(xe),
                 np.mean(ye),
                 str(adjacency_matrix[p, q]),
                 color=color,
                 fontsize=6,
                 ha='center',
                 va='center',
                 bbox={
                     'boxstyle': 'round',
                     'ec': (1, 1, 1),
                     'fc': (1, 1, 1),
                 })

    axs.set_aspect('equal')
    plt.axis(axis)

    if not (title is None):
        fig.suptitle(title)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def quiver(u,
           v,
           scale=0.5,
           nskip=None,
           z=None,
           cmap='jet',
           origin='lower',
           axis='off',
           flip=True,
           title=None,
           fname=None,
           vmin=None,
           vmax=None):
    """
    Plot a 2-dimensional quiver plot.

    Parameters
    ----------
    u : ndarray
        Input array, arrow 'x' direction.
    v : ndarray
        Input array, arrow 'y' direction.
    
    z : ndarray, optional
        Input array, background data to plot.
    cmap : string, optional
        Colormap for the plot. Default is 'jet'.
    scale : float, optional
        Arrow scaling with respect to figure width. Default is 0.5.
    nskip : int, float
        Plot only every 'nskip' data to avoid a cluttered figure.
        By default, 'nskip' is set to 1/32th of the original shape.
    origin : string, optional
        See imshow: place the [0, 0] index of the array in the upper left
        or lower left corner of the Axes. Default is 'lower'.
    axis : string, optional
        Define weather the axis are plotted. Default is 'off'.
    flip : bool, optional
        Transpose the data to get an 'ij' similar to a xy plot. Default
        is True.
    title : string, optional
        Figure title. Default is None.
    fname : string, optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).
    vmin, vmax : float, optional
        Colormap range. By default, the colormap covers the complete value
        range of the supplied data.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    xmax = max(u.shape) / u.shape[0]
    ymax = max(u.shape) / u.shape[1]

    fig = plt.figure()
    axs = plt.gca()

    if z is not None:
        if flip:
            z = z.T
            extent = [0, ymax, 0, xmax]
        else:
            extent = [0, xmax, 0, ymax]
        plt.imshow(z,
                   cmap=cmap,
                   origin=origin,
                   vmin=vmin,
                   vmax=vmax,
                   extent=extent)

    if nskip is None:
        nskip = int(max(u.shape) / 32)

    u = u[::nskip, ::nskip]
    v = v[::nskip, ::nskip]

    x, y = np.meshgrid(np.linspace(0, xmax, u.shape[0]),
                       np.linspace(0, ymax, u.shape[1]))

    plt.quiver(y, x, u, v, scale_units='width', angles='xy', scale=scale)

    axs.set_aspect('equal')
    plt.axis(axis)

    if not (title is None):
        fig.suptitle(title)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def set_equal_aspect_ratio_3daxes(axs, xmin, xmax, ymin, ymax, zmin, zmax):
    """Set equal aspect ratio for 3D axes.

    Parameters
    ----------
    axs : pyplot axes
        Pyplot axes

    """

    max_range = np.array([xmax - xmin, ymax - ymin, zmax - zmin]).max() / 2.0
    mid_x = (xmax + xmin) * 0.5
    mid_y = (ymax + ymin) * 0.5
    mid_z = (zmax + zmin) * 0.5
    axs.set_xlim(mid_x - max_range, mid_x + max_range)
    axs.set_ylim(mid_y - max_range, mid_y + max_range)
    axs.set_zlim(mid_z - max_range, mid_z + max_range)


def show_influence_matrix(func, args_dict, cmap='jet'):
    """
    Plot influence matrix of the two parameters on a heightmap (see example...).

    Parameters
    ----------
    func : function
        Must take two parameters and output a 2d ndarray.
    args_dict : dictionnary
        Dictionnary based on parameter names and values to be computed.
    cmap : str, optional
        Colormap. Default is 'jet'.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    pname1, pname2 = args_dict.keys()
    values1 = args_dict[pname1]
    values2 = args_dict[pname2]

    fig, axs = plt.subplots(len(values2), len(values1))
    fig.suptitle('{}, {}'.format(pname1, pname2))

    for i, p1 in enumerate(values1):
        for j, p2 in enumerate(values2):
            args = {
                pname1: p1,
                pname2: p2,
            }
            w = func(**args)

            axs[j, i].imshow(w.T, origin='lower', cmap=cmap)
            axs[j, i].axis('off')
            axs[j, i].set_title('({}, {})'.format(p1, p2))

    return fig, axs


def spectrum(z, cmap='gray', axis='off', fname=None):
    """
    Plot the spectrum a two 2-dimensional array.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    cmap : string
        Colormap for the plot. Default is 'gray'.
    axis : string
        Define weather the axis are plotted. Default is 'off'.
    fname : string
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    sz = np.abs(np.fft.fftshift(np.fft.fft2(z - z.mean())))
    sz = np.log10(1 + sz)

    fig = plt.figure()
    axs = plt.gca()
    plt.imshow(sz, cmap=cmap, origin='lower')
    axs.set_aspect('equal')
    plt.axis(axis)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def show(z,
         cmap='jet',
         origin='lower',
         axis='off',
         flip=True,
         title=None,
         fname=None,
         vmin=None,
         vmax=None,
         extent=None):
    """
    Plot a 2- or 3-dimensional ndarray array as one or multiple
    colormaps. Three-dimensional array is assumed to have a shape
    similar to an image, namely (N, M, P), with P much smaller than N
    and M.

    Parameters
    ----------
    z : ndarray
        Input array.
    cmap : string, optional
        Colormap for the plot. Default is 'jet'.
    origin : string, optional
        See imshow: place the [0, 0] index of the array in the upper left
        or lower left corner of the Axes. Default is 'lower'.
    axis : string, optional
        Define weather the axis are plotted. Default is 'off'.
    flip : bool, optional
        Transpose the data to get an 'ij' similar to a xy plot. Default
        is True.
    title : string, optional
        Figure title. Default is None.
    fname : string, optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).
    vmin, vmax : float, optional
        Colormap range. By default, the colormap covers the complete value
        range of the supplied data.
    extent : tuple of 4 floats, optional
        Extent of the resulting map (xmin, xmax, ymin, ymax). Default is None.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    if len(z.shape) == 2:  # 2-D
        if flip:
            z = z.T

        fig = plt.figure()
        axs = plt.gca()
        plt.imshow(z,
                   cmap=cmap,
                   origin=origin,
                   vmin=vmin,
                   vmax=vmax,
                   extent=extent)
        axs.set_aspect('equal')
        plt.axis(axis)

    if len(z.shape) == 3:  # 3-D
        if z.shape[2] < 16:

            # switch between horizontal or vertical layout according
            # to underlying 2d array shape
            if np.argmax(z.shape[0:2]) == 0:
                fig, axs = plt.subplots(z.shape[2], 1)
            else:
                fig, axs = plt.subplots(1, z.shape[2])

            for i in range(z.shape[2]):
                if flip:
                    zt = z[:, :, i].T
                else:
                    zt = z[:, :, i]
                axs[i].imshow(zt,
                              cmap=cmap,
                              origin=origin,
                              vmin=vmin,
                              vmax=vmax,
                              extent=extent)
                axs[i].axis(axis)

        else:
            log.warning(
                'shape {} is not compatible with this plotting procedure'.
                format(z.shape))

    if not (title is None):
        fig.suptitle(title)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def show3d(z, cmap='jet', axis='off', title=None, fname=None):
    """
    Surface 3D plot a 2-dimensional ndarray.

    Parameters
    ----------
    z : ndarray
        Input array.
    cmap : string
        Colormap for the plot. Default is 'nipy_spectral'.
    axis : string
        Define weather the axis are plotted. Default is 'off'.
    title : string, optional
        Figure title. Default is None.
    fname : string
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    fig, axs = plt.subplots(subplot_kw={'projection': '3d'})

    x, y = np.meshgrid(np.linspace(0, 1, z.shape[0]),
                       np.linspace(0, 1, z.shape[1]),
                       indexing='ij')

    axs.plot_surface(x, y, 0.2 * z, cmap='terrain')
    set_equal_aspect_ratio_3daxes(axs, x.min(), x.max(), y.min(), y.max(),
                                  z.min(), z.max())
    plt.axis(axis)

    if not (title is None):
        fig.suptitle(title)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs


def topo(z,
         cmap=None,
         axis='off',
         origin='lower',
         flip=True,
         title=None,
         fname=None,
         vmin=None,
         vmax=None,
         extent=None):
    """
    Plot a 2-dimensional array with a "digital-elevation model"
    style, including hillshade plot and colormap based on elevation.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    cmap : string, optional
        Colormap for the elevation plot. If set to 'None', there is no
        elevation colormap overlay. Default is None.
    origin : string, optional
        See imshow: place the [0, 0] index of the array in the upper left
        or lower left corner of the Axes. Default is 'lower'.
    axis : string, optional
        Define weather the axis are plotted. Default is 'off'.
    flip : bool, optional
        Transpose the data to get an 'ij' similar to a xy plot. Default
        is True.
    title : string, optional
        Figure title. Default is None.        
    fname : string; optional
        If defined, figure is saved as fname. Default is 'none' (no file
        saved).
    vmin, vmax : float, optional
        Colormap range. By default, the colormap covers the complete value
        range of the supplied data.
    extent : tuple of 4 floats, optional
        Extent of the resulting map (xmin, xmax, ymin, ymax). Default is None.

    Returns
    -------
    fig : figure handler
        Handler of the resulting figure.
    axs : axes handler
        Handler of the resulting axes of the figure.

    """

    if flip:
        z = z.T

    alpha = 1

    fig = plt.figure()
    axs = plt.gca()

    if not (title is None):
        fig.suptitle(title)
    if not (cmap is None):
        plt.imshow(z,
                   cmap=cmap,
                   origin=origin,
                   vmin=vmin,
                   vmax=vmax,
                   extent=extent)
        alpha = 0.5
    plt.imshow(es.hillshade(z, azimuth=210, altitude=30),
               cmap='bone',
               origin=origin,
               alpha=alpha,
               extent=extent)
    axs.set_aspect('equal')
    plt.axis(axis)

    if fname:
        fig.savefig(fname, bbox_inches='tight', pad_inches=0)

    return fig, axs
