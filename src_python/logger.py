import functools
import logging
import numpy as np
#
import vterrain as vt


class Formatter(logging.Formatter):

    def format(self, record):
        if record.levelno == logging.INFO:
            self._style._fmt = '%(message)s'
            color = ''
        else:
            color = {
                logging.WARNING: 33,
                logging.ERROR: 31,
                logging.FATAL: 31,
                logging.DEBUG: 36
            }.get(record.levelno, 0)

        self._style._fmt = f'%(name)-{0}s:%(lineno)-{0}s - %(funcName)-{0}s - \033[{color}m%(levelname)-7s\033[0m : %(message)s'.format(
            len(record.name) + 5)

        return super().format(record)


def create(name):
    level = vt.settings.LOGGER_LEVEL

    # get a logger
    logger = logging.getLogger(name)
    logger.setLevel(level)

    # remove handlers
    for hdl in logger.handlers:
        logger.removeHandler(hdl)

    # display log to console
    console_log = logging.StreamHandler()
    console_log.setLevel(level)
    console_log.setFormatter(Formatter())
    logger.addHandler(console_log)

    if vt.settings.LOGGER_FILE:
        fh = logging.FileHandler(vt.settings.LOGGER_FILE)
        fh.setLevel(logging.DEBUG)
        fh.setFormatter(Formatter())
        logger.addHandler(fh)

    logger.propagate = 0

    return logger


def show_input(log):

    def inner_decorator(func):
        argnames = func.__code__.co_varnames[:func.__code__.co_argcount]
        func_name = func.__name__
        module_name = func.__module__
        fullname = '{}.{}'.format(module_name, func_name)

        def to_string(v):
            if type(v) is not np.ndarray:
                return str(v)
            else:
                return str(type(v)) + ' [{}, {}, {:.3e}, {:.3e}]'.format(
                    v.dtype, v.shape, v.min(), v.max())

        @functools.wraps(func)
        def wrapped(*args, **kwargs):
            if vt.settings.LOGGER_SHOW_FUNCTION_ENTERING:
                log.debug('entering {}...'.format(fullname))

            if vt.settings.LOGGER_SHOW_FUNCTION_INPUT:
                out = '{}: '.format(fullname)
                for a, v in zip(argnames, args[:len(argnames)]):
                    out += '{} = {}, '.format(a, to_string(v))
                out += ' kwargs = {'
                for k, v in kwargs.items():
                    out += '{}: {}, '.format(k, to_string(v))
                out += '}'
                log.debug(out)

            # actual function call
            return func(*args, **kwargs)

        return wrapped

    return inner_decorator
