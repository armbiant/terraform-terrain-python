"""Noise generators.
"""

import copy
import numpy as np
import os
import scipy
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def cracks(shape,
           res,
           width=0.02,
           transition_radius=0.01,
           kernel=None,
           noise_displacement=0,
           seed=-1):
    """
    Generate cracks.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Noise wavenumber in x and y directions.
    width : float, optional
        Crack bottom width (in [0, 1]) with respect to the domain size.
        Default is 0.02.
    transition_radius : float, optional
        Transition radius crack/no-crack (in [0, 1]) with respect to the domain size.
        Default is 0.01.
    kernel : None or ndarray
        Kernel used to dig the cracks. If 'None' a cone-shape kernel is used. An
        ndarray can be provided. Default is None.
    noise_displacement : float, optional
        Noise added to the crack locations, in [0, 1]. Default is 0.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1

    Returns
    -------
    out : ndarray, 2-dimensional
        Crack noise, amplitude normalized between 0 and 1.

    """

    if type(res) == int:
        xn, yn = vt.grid.random_grid(res, seed=seed)
        w = voronoi2d(shape, xn, yn, distance_exponent=1, cell_filling=True)

        # redefine resolution for the displacement noise
        res = (int(res**0.5), int(res**0.5))

    else:
        w = vt.noise.worley2d(shape, res, seed=seed, cell_filling=True)

    w = vt.op.detect_border(w)

    # widen
    nc = int(width * max(shape))
    if nc:
        w = scipy.ndimage.grey_dilation(w, size=(nc, nc))

    # add noise
    if noise_displacement:
        if seed < 0:
            seed1 = seed
            seed2 = seed
        else:
            seed1, seed2 = seed + 1, seed + 2

        dx = vt.noise.fractal2d(shape,
                                res,
                                noise_fct=vt.noise.worley2d,
                                seed=seed1)
        dy = vt.noise.fractal2d(shape,
                                res,
                                noise_fct=vt.noise.worley2d,
                                seed=seed2)
        w = vt.op.warp2d(w, wx=dx, wy=dy, scale=noise_displacement)
        w = vt.op.harden(w, gain=10)

    # dig cracks
    if kernel is None:
        nk = max(3, int(transition_radius * max(shape)))
        kernel = vt.kernel.cone((nk, nk))

    w = scipy.signal.fftconvolve(w, kernel, mode='same')

    return vt.op.remap(w, vmin=1, vmax=0)


@vt.logger.show_input(log)
def diamond_square2d(shape, seed=-1, persistence=0.5):
    """
    Generate a 2d-dimensional noise based on the diamond-square algorithm.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    persistence : float, optional
        'Persistence' is a multiplier that determines how quickly
        the amplitude diminishes for each successive diamond-square
        iteration: choose 'persistence' close to 0 for a smooth noise,
        and close 1 for a rougher noise texture. Default is 0.5.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1

    Returns
    -------
    out : ndarray, 2-dimensional
        'Diamond-square' noise, amplitude normalized between 0 and 1.

    Notes
    -----
    The algorithm is originaly designed for a square array with a power-of-2
    shape.

    References
    ----------
    - Computer rendering of stochastFournier, A., Fussell, D., and
      Carpenter, L. 1982. Computer rendering of stochastic
      models. Communications of the ACM 25, 371–384.

    """

    # work on a (larger) square power-of-2 array as required by the
    # diamond-square algorithm
    nmax = max(shape)
    n2p = 1 if nmax == 0 else 2**(nmax - 1).bit_length()

    w = vterrain_core.diamond_square2d(nx=n2p,
                                       persistence=persistence,
                                       seed=seed)

    return vt.op.remap(w[:shape[0], :shape[1]], vmin=0, vmax=1)


@vt.logger.show_input(log)
def fault2d(shape, seed=-1, displacement=0.1, maxit=1000):
    """
    Generate 2-dimensional 'fault' noise, on a ndarray with a given
    shape.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    displacement : float, optional
        Random step displacement amplitude. Default is 0.1.
    maxit : integer, optional
        Maximum number iterations, i.e. random displacements. Default
        is 1000.

    Returns
    -------
    out : ndarray, 2-dimensional
        'Fault' noise, amplitude normalized between 0 and 1.

    References
    ----------
    - Galin, E., Guérin, E., Peytavie, A., et al. 2019. A Review of
      Digital Terrain Modeling. Computer Graphics Forum 38, 553–577.

    """

    x = np.linspace(0, shape[0] / max(shape), shape[0])
    y = np.linspace(0, shape[1] / max(shape), shape[1])

    w = vterrain_core.fault2d_xy(x=x,
                                 y=y,
                                 seed=seed,
                                 displacement=displacement,
                                 maxit=maxit)
    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def fault2d_xy(x, y, seed=-1, displacement=0.1, maxit=1000):
    """
    Generate 2-dimensional 'fault' noise, on a given (x,y) grid.

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    displacement : float, optional
        Random step displacement amplitude. Default is 0.1.
    maxit : integer, optional
        Maximum number iterations, i.e. random displacements. Default
        is 1000.

    Returns
    -------
    out : ndarray, 2-dimensional
        'Fault' noise (amplitude not normalized).
    """

    return vterrain_core.fault2d_xy(x=x,
                                    y=y,
                                    seed=seed,
                                    displacement=displacement,
                                    maxit=maxit)


@vt.logger.show_input(log)
def fractal2d(shape,
              res,
              noise_fct,
              octaves=8,
              persistence=0.5,
              lacunarity=2,
              seed=-1,
              **kwargs):
    """
    Generate 2-dimensional fractal noise, on a ndarray with a given
    shape, for a given base noise function (eg. Perlin, Worley...).

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Fundamental noise wavenumber in x and y directions.
    noise_fct : function
        Noise function, must be 2-dimensional and must have
        (shape, res, seed) as input parameters.
    octaves : int, optional
        Number of octaves. Default is 8.
    persistence : float, optional
        'Persistence' is a multiplier that determines how quickly
        the amplitude diminishes for each successive octave: choose
        'persistence' close to 0 for a smooth noise, and close 1 for
        a rougher noise texture. Default is 0.5.
    lacunarity : float, optional
        Defines the wavenumber ratio between each octaves. Default is 2.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    kwargs, optional
        Any additional input arguments that will be passed to the
        noise function.

    Returns
    -------
    out : ndarray, 2-dimensional
        Factal noise, amplitude normalized between 0 and 1.

    """

    w = np.zeros(shape)
    s = seed
    for k in range(octaves):
        if s < 0:
            s = seed
        else:
            s += 1

        nf = int(lacunarity**k)
        res_h = (nf * res[0], nf * res[1])

        if res_h[0] > int(shape[0] / 2) or res_h[1] > int(shape[1] / 2):
            # if the resolution is getting too high compared to the
            # grid resolution, just stops
            log.debug('resolution is too high, octave loop interrupted '
                      'at octave #{}'.format(k + 1))
            break

        w += persistence**k * noise_fct(
            shape=shape, res=res_h, seed=s, **kwargs)

    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def fractal2d_xy(x,
                 y,
                 res,
                 noise_fct_xy,
                 octaves=8,
                 persistence=0.5,
                 lacunarity=2,
                 seed=-1,
                 **kwargs):
    """
    Generate 2-dimensional fractal noise, on a ndarray, on a given (x,y)
    grid, for a given base noise function (eg. Perlin, Worley...).

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    res : (int, int)
        Fundamental noise wavenumber in x and y directions.
    noise_fct_xy : function
        Noise function, must be 2-dimensional and must have
        (x, y, res, seed) as input parameters.
    octaves : int, optional
        Number of octaves. Default is 8.
    persistence : float, optional
        'Persistence' is a multiplier that determines how quickly
        the amplitude diminishes for each successive octave: choose
        'persistence' close to 0 for a smooth noise, and close 1 for
        a rougher noise texture. Default is 0.5.
    lacunarity : float, optional
        Defines the wavenumber ratio between each octaves. Default is 2.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    kwargs, optional
        Any additional input arguments that will be passed to the
        noise function.

    Returns
    -------
    out : ndarray, 2-dimensional
        Factal noise (amplitude not normalized).

    """

    w = np.zeros((x.shape[0], y.shape[0]))
    s = seed
    for k in range(octaves):
        if s < 0:
            s = seed
        else:
            s += 1

        nf = int(lacunarity**k)
        res_h = (nf * res[0], nf * res[1])
        w += persistence**k * noise_fct_xy(
            x=x, y=y, res=res_h, seed=s, **kwargs)

    return w


@vt.logger.show_input(log)
def gabor2d(shape, width, f0, w0, seed=-1, sparse=False, density=0.1):
    """
    Generate 2-dimensional Gabor noise on a ndarray with a given shape.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    width : float
        Kernel width.
    f0 : float
        Kernel wavenumber.
    w0 : float
        Kernel 'angle' in radian. If set to None, the angle is chosen
        randomly for each kernel evaluation to produce isotropic Gabor
        noise.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    sparse : boolean, optional
        If set to True, the weight field automatically generated is sparse.
        Default is True.
    density : float, optional
        Density of the sparse matrix (see ``scipy.sparse.random``). Default
        is 0.1.

    Returns
    -------
    out : ndarray, 2-dimensional
        Gabor noise, amplitude normalized between 0 and 1.

    References
    ----------
    - Lagae, A., Lefebvre, S., Drettakis, G., and Dutré, P. 2009. Procedural
      noise using sparse Gabor convolution. ACM SIGGRAPH 2009 papers on - SIGGRAPH
      09, ACM Press.

    """

    weight = white2d(shape, seed=seed, sparse=sparse, density=density)

    if w0 is not None:
        kernel = vt.kernel.gabor_width(width=width,
                                       f0=f0,
                                       w0=w0,
                                       reference_shape=shape)
        w = scipy.signal.fftconvolve(weight, kernel, mode='same')

    else:
        w = np.zeros(shape)

        # random kernel angle
        if seed > 0:
            seed += 1
        w0 = 2 * np.pi * white2d(shape, seed=seed, sparse=False)

        # fake-convolution
        iw, jw = np.where(weight > 0)
        for i, j in zip(iw, jw):
            kernel = vt.kernel.gabor_width(width=width,
                                           f0=f0,
                                           w0=w0[i, j],
                                           reference_shape=shape)
            w = vt.op.add_array(w, i, j, weight[i, j] * kernel)

    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def hybrid_multifractal2d(shape,
                          res,
                          noise_fct,
                          octaves=8,
                          persistence=0.9,
                          lacunarity=2,
                          offset=0.1,
                          seed=-1,
                          **kwargs):
    """
    Generate 2-dimensional hybrid multifractal fractal noise
    (Musgrave's noise), on a ndarray with a given shape, for a given
    base noise function (eg. Perlin, Worley...).

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Fundamental noise wavenumber in x and y directions.
    noise_fct : function
        Noise function, must be 2-dimensional and must have
        (shape, res, seed) as input parameters.
    octaves : int, optional
        Number of octaves. Default is 8.
    persistence : float, optional
        'Persistence' is a multiplier that determines how quickly
        the amplitude diminishes for each successive octave: choose
        'persistence' close to 0 for a smooth noise, and close 1 for
        a rougher noise texture. Default is 0.9.
    lacunarity : float, optional
        Defines the wavenumber ratio between each octaves. Default is 2.
    offset : float, optional
        Offset applied to the noise function to move its range to [0, 2].
        For most noise functionq provided in this package, 1 is a proper
        choise. Default is 0.3.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    kwargs, optional
        Any additional input arguments that will be passed to the
        noise function.

    Returns
    -------
    out : ndarray, 2-dimensional
        Factal noise, amplitude normalized between 0 and 1.

    References
    ----------
    - Musgrave, F.K., Kolb, C.E., and Mace, R.S. 1989. The synthesis and
      rendering of eroded fractal terrains. ACM SIGGRAPH Computer Graphics
      23, 41–50.

    """

    w = np.zeros(shape)
    weight = np.ones(shape)
    s = seed
    for k in range(octaves):
        if s < 0:
            s = seed
        else:
            s += 1

        nf = int(lacunarity**k)
        res_h = (nf * res[0], nf * res[1])

        if res_h[0] > int(shape[0] / 2) or res_h[1] > int(shape[1] / 2):
            # if the resolution is getting too high compared to the
            # grid resolution, just stops
            log.debug('resolution is too high, octave loop interrupted '
                      'at octave #{}'.format(k + 1))
            break

        w_h = persistence**k * (
            noise_fct(shape=shape, res=res_h, seed=s, **kwargs) + offset)

        w += weight * w_h

        # update and clamp the weight to avoid divergence
        weight *= w_h
        weight[np.where(np.abs(weight) > 1)] = 1

    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def hybrid_multifractal2d_xy(x,
                             y,
                             res,
                             noise_fct_xy,
                             octaves=8,
                             persistence=0.9,
                             lacunarity=2,
                             offset=0.3,
                             seed=-1,
                             **kwargs):
    """
    Generate 2-dimensional hybrid multifractal fractal noise
    (Musgrave's noise), on a ndarray, on a given (x,y) grid, for a
    given base noise function (eg. Perlin, Worley...).

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    res : (int, int)
        Fundamental noise wavenumber in x and y directions.
    noise_fct : function
        Noise function, must be 2-dimensional and must have
        (shape, res, seed) as input parameters.
    octaves : int, optional
        Number of octaves. Default is 8.
    persistence : float, optional
        'Persistence' is a multiplier that determines how quickly
        the amplitude diminishes for each successive octave: choose
        'persistence' close to 0 for a smooth noise, and close 1 for
        a rougher noise texture. Default is 0.5.
    lacunarity : float, optional
        Defines the wavenumber ratio between each octaves. Default is 2.
    offset : float, optional
        Offset applied to the noise function to move its range to [0, 2].
        For most noise functionq provided in this package, 1 is a proper
        choise. Default is 1.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    kwargs, optional
        Any additional input arguments that will be passed to the
        noise function.

    Returns
    -------
    out : ndarray, 2-dimensional
        Factal noise (amplitude not normalized).

    References
    ----------
    - Musgrave, F.K., Kolb, C.E., and Mace, R.S. 1989. The synthesis and
      rendering of eroded fractal terrains. ACM SIGGRAPH Computer Graphics
      23, 41–50.

    """

    shape = (x.shape[0], y.shape[0])
    w = np.zeros(shape)
    weight = np.ones(shape)
    s = seed
    for k in range(octaves):
        if s < 0:
            s = seed
        else:
            s += 1

        nf = int(lacunarity**k)
        res_h = (nf * res[0], nf * res[1])
        w_h = persistence**k * (
            noise_fct_xy(x=x, y=y, res=res_h, seed=s, **kwargs) + offset)

        w += weight * w_h

        # update and clamp the weight to avoid divergence
        weight *= w_h
        weight[np.where(np.abs(weight) > 1)] = 1

    return w


@vt.logger.show_input(log)
def perlin2d(shape, res, seed=-1, mu=None):
    """
    Generate 2-dimensional Perlin's noise, on a ndarray with a given
    shape.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Perlin's noise wavenumber in x and y directions.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1

    Returns
    -------
    out : ndarray, 2-dimensional
        Perlin's noise, amplitude normalized between 0 and 1.

    References
    ----------
    - Perlin, K. 1985. An image synthesizer. ACM SIGGRAPH Computer
      Graphics 19, 287–296.

    """

    x = np.linspace(0, shape[0] / max(shape), shape[0])
    y = np.linspace(0, shape[1] / max(shape), shape[1])

    if mu is None:
        mu = 0

    w = vterrain_core.perlin2d_xy(x=x, y=y, res=res, seed=seed, mu=mu)
    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def perlin2d_xy(x, y, res, seed=-1, mu=None):
    """
    Generate 2-dimensional Perlin's noise, on a given (x,y) grid.

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    res : (int, int)
        Perlin's noise wavenumber in x and y directions.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.
    mu : float, optional
        Exponential rate, for exponentially distributed gradients: for
        positive values results in fewer 'large' gradients and negative
        values does the opposite. Not activated by default (mu=0.2 can
        be used as a starting point). Default is None.

    Returns
    -------
    out : ndarray, 2-dimensional
        Perlin's noise (amplitude not normalized).

    References
    ----------
    - Perlin, K. 1985. An image synthesizer. ACM SIGGRAPH Computer
      Graphics 19, 287–296.
    - Parberry, I. 2015. Modeling Real-World Terrain with Exponentially
      Distributed Noise. Journal of Computer Graphics Techniques (JCGT) 4, 1–9.
    """

    if mu is None:
        mu = 0

    return vterrain_core.perlin2d_xy(x=x, y=y, res=res, seed=seed, mu=mu)


@vt.logger.show_input(log)
def ridge2d(shape, res, noise_fct, seed=-1, **kwargs):
    """
    Generate 2-dimensional "ridge" noise, on a ndarray with a given
    shape, for a given base noise function (eg. Perlin, Worley...).

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Perlin's noise wavenumber in x and y directions.
    noise_fct : function
        Noise function, must be 2-dimensional and must have
        (shape, res, seed) as input parameters.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    kwargs, optional
        Any additional input arguments that will be passed to the
        noise function.

    Returns
    -------
    out : ndarray, 2-dimensional
        'Ridge' noise, amplitude normalized between 0 and 1.

    """

    # applying the absolute value is going to double the wavenumbers,
    # simply produce a field with reduced wavenumbers in the first
    # place with the primitive to cope with that
    res_r = (max(1, int(res[0] / 2)), max(1, int(res[1] / 2)))

    w = noise_fct(shape=shape, res=res, seed=seed, **kwargs)
    w = 1 - np.abs(vt.op.remap(w, vmin=-1, vmax=1))
    return w


@vt.logger.show_input(log)
def spot2d(shape, kernel, seed=-1, weight=None, sparse=False, density=0.1):
    """
    Generate 2-dimensional "spot" noise (random placement of a small
    pattern, the spot, over a surface), on a ndarray with a given
    shape, for a given base kernel.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    kernel : ndarray
        Convolution kernel.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    weight : ndarray, optional
        Weight field convolved with the kernel. If not defined, the weight
        is generated automatically: white noise if sparse is set to False,
        and random sparse noise if sparse is set to True. Default is None.
    sparse : boolean, optional
        If set to True, the weight field automatically generated is sparse.
        Default is False.
    density : float, optional
        Density of the sparse matrix (see scipy.sparse.random). Default is
        0.1.

    Returns
    -------
    out : ndarray, 2-dimensional
        'Spot' noise, amplitude normalized between 0 and 1.

    References
    ----------
    - van Wijk, J.J. 1991. Spot noise texture synthesis for data
      visualization. Proceedings of the 18th annual conference on Computer
      graphics and interactive techniques - SIGGRAPH 91, ACM Press.

    """

    # generate weight filed is not provided
    if weight is None:
        weight = white2d(shape, seed=seed, sparse=sparse, density=density)

    w = scipy.signal.fftconvolve(weight, kernel, mode='same')
    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def random_amplitude_gradient_pdf(shape, res, pdf, scale=10, seed=-1):
    """
    Generate 2d noise based on a (2d) probability function of the
    couple (amplitude, gradient norm).

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Noise wavenumber in x and y directions.
    pdf : ndarray, 2d
        Probability density function of the couple (amplitude, gradient norm).
    scale : float, optional
        Gradient scaling. Default is 10.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1

    Returns
    -------
    out : ndarray, 2-dimensional
        Resulting noise, amplitude normalized between 0 and 1.

    """

    # generate (amplitude, gradient) samples
    a, d = vt.grid.random_grid_pdf(n=res[0] * res[1], pdf=pdf, seed=seed)
    a = a.reshape(res)
    d = d.reshape(res)

    # gradient orientation is randomly chosen
    theta = 2 * np.pi * vt.noise.white2d(res, seed=seed)

    # gradient are imposed using additional nodes
    dx = 1 / 6 / (res[0] - 1)
    dy = 1 / 6 / (res[1] - 1)

    # amplitude on the new grid
    amp_g = np.empty((3 * res[0], 3 * res[1]))
    for i in range(3):
        for j in range(3):
            amp_g[i::3, j::3] = a

    ct = np.cos(theta)
    st = np.sin(theta)

    amp_g[2::3, 1::3] = amp_g[2::3, 1::3] + scale * dx * d * ct
    amp_g[0::3, 1::3] = amp_g[0::3, 1::3] - scale * dx * d * ct

    amp_g[1::3, 2::3] = amp_g[1::3, 2::3] + scale * dy * d * st
    amp_g[1::3, 0::3] = amp_g[1::3, 0::3] - scale * dy * d * st

    amp_g[2::3,
          2::3] = amp_g[2::3, 2::3] + scale * d * (dx * ct + dy * st) * 0.707
    amp_g[0::3,
          0::3] = amp_g[0::3, 0::3] - scale * d * (dx * ct + dy * st) * 0.707

    amp_g[0::3,
          2::3] = amp_g[0::3, 2::3] + scale * d * (-dx * ct + dy * st) * 0.707
    amp_g[2::3,
          0::3] = amp_g[2::3, 0::3] - scale * d * (dx * ct - dy * st) * 0.707

    # grid coordinates
    xa = np.empty((3 * res[0]))
    ya = np.empty((3 * res[1]))

    xa[0::3] = np.linspace(0, 1, res[0]) - dx
    xa[1::3] = np.linspace(0, 1, res[0])
    xa[2::3] = np.linspace(0, 1, res[0]) + dx

    ya[0::3] = np.linspace(0, 1, res[1]) - dy
    ya[1::3] = np.linspace(0, 1, res[1])
    ya[2::3] = np.linspace(0, 1, res[1]) + dy

    # eventually interpolate
    fitp = scipy.interpolate.RectBivariateSpline(xa, ya, amp_g, kx=3, ky=3)

    xw = np.linspace(0, 1, shape[0])
    yw = np.linspace(0, 1, shape[1])

    return vt.op.remap(fitp(xw, yw))


@vt.logger.show_input(log)
def value2d(shape, res, seed=-1):
    """
    Generate 2-dimensional "value" noise on a ndarray with a given
    shape.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Approximate noise wavenumber in x and y directions.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1

    Returns
    -------
    out : ndarray, 2-dimensional
        'Value' noise, amplitude normalized between 0 and 1.

    """

    # lattice white noise
    nxv = 2 * res[0] + 1
    nyv = 2 * res[1] + 1

    if seed > 0:
        rng = np.random.default_rng(seed)
        values = rng.random((nxv, nyv))
    else:
        values = np.random.rand(nxv, nyv)

    # build interpolation function
    xv = np.linspace(0, 1, nxv)
    yv = np.linspace(0, 1, nyv)
    fitp = scipy.interpolate.RectBivariateSpline(xv,
                                                 yv,
                                                 values,
                                                 bbox=[0, 1, 0, 1])

    # interpolate array with requested shape
    x = np.linspace(0, 1, shape[0])
    y = np.linspace(0, 1, shape[1])
    return vt.op.remap(fitp(x, y), vmin=0, vmax=1)


@vt.logger.show_input(log)
def voronoi2d(shape, xn, yn, distance_exponent=1, cell_filling=False):
    """
    Generate Voronoi distance map as a 2d array.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    xn : ndarray, 1d
        'x' coordinate values of the nodes in [0, 1].
    yn : ndarray, 1d
        'y' coordinate values of the nodes in [0, 1].
    distance_exponent : float, optional
        Exponent applied to the distance computation: set
        distance_exponent=0.5 to get and Euclidian distance. Default is 1.
    cell_filling : boolean, optional
        If set to True, the cells are filled with consecutive constant
        values.

    Returns
    -------
    out : ndarray, 2-dimensional
        Voronoi distance.

    """

    x = np.linspace(0, shape[0] / max(shape), shape[0])
    y = np.linspace(0, shape[1] / max(shape), shape[1])

    ws = vterrain_core.voronoi2d_xy(x,
                                    y,
                                    xn,
                                    yn,
                                    distance_exponent=distance_exponent,
                                    cell_filling=cell_filling)

    if not (cell_filling):
        ws = vt.op.remap(ws, vmin=0, vmax=1)

    return ws


@vt.logger.show_input(log)
def voronoi2d_xy(x, y, xn, yn, distance_exponent=1, cell_filling=False):
    """
    Generate Voronoi distance map as a 2d array.

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    xn : ndarray, 1d
        'x' coordinate values of the nodes in [0, 1].
    yn : ndarray, 1d
        'y' coordinate values of the nodes in [0, 1].
    distance_exponent : float, optional
        Exponent applied to the distance computation: set
        distance_exponent=0.5 to get and Euclidian distance. Default is 1.
    cell_filling : boolean, optional
        If set to True, the cells are filled with consecutive constant
        values.

    Returns
    -------
    out : ndarray, 2-dimensional
        Voronoi distance.

    """

    vterrain_core.voronoi2d_xy(x,
                               y,
                               xn,
                               yn,
                               distance_exponent=distance_exponent,
                               cell_filling=bool(cell_filling))


@vt.logger.show_input(log)
def worley2d(shape, res, seed=-1, distance_exponent=1, cell_filling=False):
    """
    Generate 2-dimensional Worley's noise ("cellular noise"), on a
    ndarray with a given shape.

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    res : (int, int)
        Perlin's noise wavenumber in x and y directions.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    distance_exponent : float, optional
        Exponent applied to the distance computation: set
        distance_exponent=0.5 to get and Euclidian distance. Default is 1.
    cell_filling : boolean, optional
        If set to True, the cells are filled with a constant value.
        Values are consecutive integers.

    Returns
    -------
    out : ndarray, 2-dimensional
        Worley's noise, amplitude normalized between 0 and 1.

    References
    ----------
    - Worley, S. 1996. A cellular texture basis function. Proceedings of
      the 23rd annual conference on Computer graphics and interactive
      techniques - SIGGRAPH 96, ACM Press.

    """

    x = np.linspace(0, shape[0] / max(shape), shape[0])
    y = np.linspace(0, shape[1] / max(shape), shape[1])

    w = vterrain_core.worley2d_xy(x=x,
                                  y=y,
                                  res=res,
                                  seed=seed,
                                  distance_exponent=distance_exponent,
                                  cell_filling=bool(cell_filling))
    return vt.op.remap(w, vmin=0, vmax=1)


@vt.logger.show_input(log)
def white2d(shape, seed=-1, sparse=False, density=0.1):
    """
    Generate 2-dimensional white noise, on a ndarray with a given
    shape (can be sparse).

    Parameters
    ----------
    shape : (int, int)
        Resulting ndarray shape.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    sparse : boolean, optional
        If set to True, the weight field automatically generated is sparse.
        Default is False.
    density : float, optional
        Density of the sparse matrix (see ``scipy.sparse.random``). Default is
        0.1.

    Returns
    -------
    out : ndarray, 2-dimensional
        White noise, amplitudes in [0, 1].

    """

    if sparse:
        # random sparse matrix
        if seed > 0:
            rng = np.random.default_rng(seed)
        else:
            rng = None
        w = scipy.sparse.random(shape[0],
                                shape[1],
                                density=density,
                                random_state=rng).A
    else:
        # white noise
        if seed > 0:
            rng = np.random.default_rng(seed)
            w = rng.random(shape)
        else:
            w = np.random.rand(shape[0], shape[1])

    return w


@vt.logger.show_input(log)
def worley2d_xy(x, y, res, seed=-1, distance_exponent=1, cell_filling=False):
    """
    Generate 2-dimensional Worley's noise ("cellular noise"), on a
    given (x,y) grid.

    Parameters
    ----------
    x : ndarray, 1d
        'x' coordinate values, as a 1-dimensional array, between 0 and 1.
    y : ndarray, 1d
        'y' coordinate values, as a 1-dimensional array, between 0 and 1.
    res : (int, int)
        Perlin's noise wavenumber in x and y directions.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    distance_exponent : float, optional
        Exponent applied to the distance computation: set
        distance_exponent=0.5 to get and Euclidian distance. Default is 1.
    cell_filling : boolean, optional
        If set to True, the cells are filled with consecutive constant
        values.

    Returns
    -------
    out : ndarray, 2-dimensional
        Worley's noise (amplitude not normalized).

    References
    ----------
    - Worley, S. 1996. A cellular texture basis function. Proceedings of
      the 23rd annual conference on Computer graphics and interactive
      techniques - SIGGRAPH 96, ACM Press.

    """

    return vterrain_core.worley2d_xy(x=x,
                                     y=y,
                                     res=res,
                                     seed=seed,
                                     distance_exponent=distance_exponent,
                                     cell_filling=bool(cell_filling))
