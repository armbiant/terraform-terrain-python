"""Mesh generation functions.
"""

import cv2
import matplotlib.colors
import numpy as np
import os
import trimesh
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def hmap_to_trimesh(z, x=None, y=None):
    """
    Create a triangular mesh based on a given heightmap.

    Parameters
    ----------
    z : ndarray
        Input array, 2-dimensional.
    x : ndarray, optional
        'x' coordinate values, 1D-array or as a meshgrid ('ij' indexing).
        Default is none.
    y : ndarray, optional
        'x' coordinate values, 1D-array or as a meshgrid ('ij' indexing).
        Default is none.

    Returns
    -------
    out : trimesh.Trimesh object
        The mesh.

    """

    nx, ny = z.shape

    if x is None:
        x, y = np.meshgrid(np.linspace(0, nx / max(nx, ny), nx),
                           np.linspace(0, ny / max(nx, ny), ny),
                           indexing='ij')
    else:
        x, y = np.meshgrid(x, y, indexing='ij')

    xyz = np.vstack((x.flatten(), y.flatten(), z.flatten())).T

    nn = nx * ny
    idx = np.linspace(0, nn - 1, nn).astype(int)

    faces = []
    for j in range(nx - 1):
        i1 = j * ny
        faces += list(
            np.vstack((
                idx[i1:i1 + ny - 1],
                idx[i1 + ny + 1:i1 + 2 * ny],
                idx[i1 + 1:i1 + ny],
            )).T)
        faces += list(
            np.vstack((
                idx[i1:i1 + ny - 1],
                idx[i1 + ny:i1 + 2 * ny - 1],
                idx[i1 + ny + 1:i1 + 2 * ny],
            )).T)

    return trimesh.Trimesh(vertices=xyz, faces=faces)


@vt.logger.show_input(log)
def set_color(mesh, color, mask=None, values=None, alpha=None):
    """
    Set vertex color of the mesh, a mask can be provided (with the same
    shape as the initial heightmap used to create the mesh).

    Parameters
    ----------
    mesh : trimesh.Trimesh
        The mesh
    color : string or 4-tuple
        Color of the vertices. Can be a colormap if color is a string, or
        a solid color if color = [R, G, B, A] (values in [0, 255]). If color
        is a list of colors, a linear colormap is created and applied based
        on these colors.
    mask : ndarray, optional
        Mask array, defining where the color is set. Default is None.
    alpha : float or ndarray, optional
        Texture alpha, in [0, 1]. Default is uniform and equal to 1.

    Returns
    -------
    None

    """

    # define or adjust mask
    if mask is None:
        idx = range(mesh.vertices.shape[0])
    else:
        idx = np.where(np.ravel(mask) > 0)

    # define value
    if values is None:
        values = mesh.vertices[idx, 2]
    else:
        values = np.ravel(values)[idx]

    # set color
    if type(color) == str:
        mesh.visual.vertex_colors[idx] = trimesh.visual.interpolate(
            values, color_map=color)

    elif type(color[0]) == list:
        # create linear colormap
        color = np.asarray(color) / 255
        cmap = matplotlib.colors.LinearSegmentedColormap.from_list('', color)
        color = np.apply_along_axis(cmap, 0, vt.op.remap(values)) * 255
        mesh.visual.vertex_colors[idx] = color

    else:

        if alpha is None:
            mesh.visual.vertex_colors[idx] = color

        else:
            if not (isinstance(alpha, np.ndarray)):
                alpha *= np.ones(values.shape[1])
            else:
                alpha = np.ravel(alpha)[idx]

            color_vector = np.empty((values.shape[1], 4))
            color_vector[:, :3] = color[:3]
            color_vector[:, 3] = alpha * 255

            mesh.visual.vertex_colors[idx] = color_vector

    return None


@vt.logger.show_input(log)
def set_texture_file(mesh, fname, heightmap_shape, mask=None, alpha=1):
    """
    Set vertex color of the mesh based on a texture file.

    Parameters
    ----------
    mesh : trimesh.Trimesh
        The mesh.
    fname : str
        Path to the texture file.
    mask : ndarray, optional
        Mask array, defining where the color is set. Default is None.
    heightmap_shape : (int, int)
        Array shape of the corresponding heightmap.
    mask : ndarray, optional
        Mask array, defining where the color is set. Default is None.
    alpha : float or ndarray, optional
        Texture alpha, in [0, 1]. Default is uniform and equal to 1.

    Returns
    -------
    None

    """

    # define or adjust mask
    if mask is None:
        idx = range(mesh.vertices.shape[0])
    else:
        idx = np.where(np.ravel(mask) > 0)

    if not (isinstance(alpha, np.ndarray)):
        alpha *= np.ones(heightmap_shape)

    # load and interpolate
    w = np.flipud(cv2.imread(fname))

    wi = np.zeros((heightmap_shape[0], heightmap_shape[1], 4))
    for k in range(3):
        wi[:, :, k] = vt.op.reshape_interp(w[:, :, k].T, heightmap_shape)

    # add alpha channel
    wi[:, :, 3] = alpha * 255 * np.ones((wi.shape[0], wi.shape[1]))

    wi = np.reshape(wi, (wi.shape[0] * wi.shape[1], wi.shape[2]))

    mesh.visual.vertex_colors[idx] = wi[idx]

    return None
