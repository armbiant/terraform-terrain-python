"""Grid generation.
"""

import numpy as np
import os
import scipy.stats
import skimage
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def mask_gather(x, y, mask):
    """
    Gather a random grid based on a mask: random points are gathered
    only where the mask has non-zero values.
    
    Parameters
    ----------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.
    mask : ndarray
        Mask array.

    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the output points.
    out : ndarray, 1d, float
        'y' location of the output points.

    """

    if mask is None:
        return x, y

    else:
        shape = mask.shape
        idx = np.where(mask > 0)
        i = ((x + y) * (len(idx[0][:]) - 1) / 2).astype(int)

        x += idx[0][i].astype(float)
        y += idx[1][i].astype(float)

        return x / (shape[0] - 1), y / (shape[1] - 1)


@vt.logger.show_input(log)
def mask_filter(x, y, mask, extent=[0, 1, 0, 1]):
    """
    Remove grid points outside a given mask.

    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    mask : ndarray
        Filter mask array.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 1d
        Point 'x' filtered coordinates.
    out : ndarray, 1d
        Point 'y' filtered coordinates.
    out : ndarray, 1d
        Filtering indices.

    """

    mask = mask.astype(bool)
    shape = mask.shape

    # remove points outside domain
    idx = np.where((x >= extent[0]) & (x <= extent[1]) \
                  & (y >= extent[2]) & (y <= extent[3]))
    x = x[idx]
    y = y[idx]

    # convert to indices
    xs = (x - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1)
    ys = (y - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1)

    xs = xs.astype(int)
    ys = ys.astype(int)

    idx = mask[xs, ys]

    return x[idx], y[idx], idx


@vt.logger.show_input(log)
def poisson_disk_sampler(rmin, nsearch=30, extent=[0, 1, 0, 1], seed=-1):
    """
    Parameters
    ----------
    z : ndarray
        Input minimal radius as a 2-dimensional array. Radius is provided
        in pixels.
    nsearch : int, optional
        Number of trials to find a new point for each new point (...).
        Default is 30.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray, 1d
        Point 'x' coordinates.
    out : ndarray, 1d
        Point 'y' coordinates.

    """

    # rescaled 'rmin' to work in pixels
    sx = (extent[1] - extent[0]) / rmin.shape[0]
    sy = (extent[3] - extent[2]) / rmin.shape[1]

    rmin_scaled = np.copy(rmin) / min(sx, sy)

    # to avoid any convergence issue
    if rmin_scaled.min() < 1:
        log.warning('minimum value of the minimum radius is below 1 '
                    '(rmin.min() = {:.3e}). Array values have been '
                    'clamped'.format(rmin_scaled.min()))

    rmin_scaled = np.maximum(1, rmin_scaled)

    log.debug('calling f90 core solver...')
    xy = vterrain_core.poisson_disk_sampler(rmin=rmin_scaled,
                                            nsearch=nsearch,
                                            seed=seed)

    # remove 'useless' data
    x = xy[:, :, 0]
    y = xy[:, :, 1]
    idx = np.where(x > 0)

    # filter and rescale
    x = vt.op.remap(x[idx], extent[0], extent[1])
    y = vt.op.remap(y[idx], extent[2], extent[3])

    return x, y


@vt.logger.show_input(log)
def random_grid(n, seed=-1, extent=[0, 1, 0, 1], mask=None):
    """
    Generate a random grid of 'n' points.

    Parameters
    ----------
    n : int
        Number of points.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
    mask : ndarray, 2d
        Gathering mask. Default is None.
        
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.

    """

    if seed > 0:
        rng = np.random.default_rng(seed)
        x, y = rng.random(n), rng.random(n)
    else:
        x, y = np.random.rand(n), np.random.rand(n)

    # if a mask is defined, pick up points only where the mask allows it
    x, y = mask_gather(x, y, mask)

    x = x * (extent[1] - extent[0]) + extent[0]
    y = y * (extent[3] - extent[2]) + extent[2]

    return x, y


@vt.logger.show_input(log)
def random_grid_halton(n, seed=-1, extent=[0, 1, 0, 1]):
    """
    Generate an Halton sequence grid of 'n' points.
    
    Parameters
    ----------
    n : int
        Number of points.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.

    """

    if seed < 0:
        seed = None

    sampler = scipy.stats.qmc.Halton(d=2, scramble=False, seed=seed)
    sample = sampler.random(n=n)
    sample = scipy.stats.qmc.scale(sample, extent[0:4:2], extent[1:4:2])
    x, y = np.split(sample, 2, axis=1)
    return x[:, 0], y[:, 0]


@vt.logger.show_input(log)
def random_grid_jittered(dx,
                         dy,
                         scale=1,
                         staggered=False,
                         log=(False, False),
                         seed=-1,
                         extent=[0, 1, 0, 1]):
    """
    Generate a random 'jittered' grid (random displacements are added to a
    regular grid).
    
    Parameters
    ----------
    dx : float
        Grid spacing in the 'x' direction (domain is assumed to a unit square).
    dy : float
        Grid spacing in the 'y' direction (domain is assumed to a unit square).
    scale : float, optional
        Scale of random displacements in x and y direction, normalized by 'dx'
        and 'dy'. Default is 1.
    log : (bool, bool)
        Use log scale in x and y directions. Default is (False, False).
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.

    """

    if not (log[0]):
        xval = np.arange(-dx / 2, 1 + dx / 2, dx)
    else:
        xval = np.geomspace(dx / 2, 1, int(1 / dx))

    if not (log[1]):
        yval = np.arange(-dy / 2, 1 + dy / 2, dy)
    else:
        yval = np.geomspace(dy / 2, 1, int(1 / dy))

    x, y = np.meshgrid(xval, yval, indexing='ij')

    if seed > 0:
        rng = np.random.default_rng(seed)
        n1 = rng.random((x.shape[0], x.shape[1]))
        n2 = rng.random((x.shape[0], x.shape[1]))
    else:
        n1 = np.random.rand(x.shape[0], x.shape[1])
        n2 = np.random.rand(x.shape[0], x.shape[1])

    x += dx / 2 * scale * (2 * n1 - 1)
    y += dy / 2 * scale * (2 * n2 - 1)

    if staggered:
        x[:, ::2] += dx / 2

    x, y = x.ravel(), y.ravel()

    x = x * (extent[1] - extent[0]) + extent[0]
    y = y * (extent[3] - extent[2]) + extent[2]

    return x, y


@vt.logger.show_input(log)
def random_grid_density(n, density, extent=[0, 1, 0, 1], scale=1, seed=-1):
    """
    Random grid generation based on a two-dimensional density.

    Parameters
    ----------
    n : int
        Number of samples.
    density : ndarray, 2d, float
        Point density.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
    scale : float, optional
        Warping scale. Default is 1.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.
    """

    d = vt.op.remap(density)
    shape = density.shape

    # generate base grid (purely random)
    x, y = vt.grid.random_grid(n=n, extent=extent, seed=seed)

    # add warping based on density gradients
    i, j = vt.grid.xy_to_ij(x, y, shape=shape, extent=extent)

    dx, dy = vt.op.gradient(density)
    dx /= np.max(np.abs(dx))
    dy /= np.max(np.abs(dy))

    # to avoid artifacts at boundaries
    dx = vt.op.enforce_boundaries(dx, values=0, xc=0.2, yc=0.2)
    dy = vt.op.enforce_boundaries(dy, values=0, xc=0.2, yc=0.2)

    x += 0.05 * scale * dx[i, j]
    y += 0.05 * scale * dy[i, j]

    # remove points outside domain
    idx = np.where((x >= extent[0]) & (x <= extent[1]) \
                 & (y >= extent[2]) & (y <= extent[3]))
    x = x[idx]
    y = y[idx]

    return x, y


@vt.logger.show_input(log)
def random_grid_pdf(n, pdf, extent=[0, 1, 0, 1], seed=-1):
    """
    Random grid generation based on a two-dimensional pdf.

    Parameters
    ----------
    n : int
        Number of samples.
    pdf : ndarray, 2d, float
        Probability density function.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the input points.
    out : ndarray, 1d, float
        'y' location of the input points.
    """

    if seed < 0:
        seed = None

    nx, ny = pdf.shape

    x, y = np.meshgrid(np.linspace(extent[0], extent[1], nx),
                       np.linspace(extent[2], extent[3], ny),
                       indexing='ij')

    values = range(nx * ny)
    probabilities = pdf.ravel(order='F') / np.sum(pdf)
    distrib = scipy.stats.rv_discrete(values=(values, probabilities),
                                      seed=seed)

    samples = np.asarray(distrib.rvs(size=n), dtype=int)

    j = np.floor(samples / nx).astype(int)
    i = samples - j * nx

    return x[i, j], y[i, j]


@vt.logger.show_input(log)
def random_grid1d_pdf(n, pdf, extent=[0, 1], seed=-1, preserve_extrema=True):
    """
    Random 1d grid generation based on a pdf.

    Parameters
    ----------
    n : int
        Number of samples.
    pdf : ndarray, 1d, float
        Probability density function.
    extent : tuple of 1 floats
        Extent of the resulting array (xmin, xmax). Default is [0, 1].
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1.

    Returns
    -------
    out : ndarray, 1d, float
        'x' location of the points.
    """

    if seed < 0:
        seed = None

    nx = pdf.shape[0]
    x = np.linspace(extent[0], extent[1], nx)

    values = range(nx)
    probabilities = pdf / np.sum(pdf)
    distrib = scipy.stats.rv_discrete(a=0,
                                      b=nx - 1,
                                      values=(values, probabilities),
                                      seed=seed)

    if not (preserve_extrema):
        samples = np.asarray(distrib.rvs(size=n), dtype=int)

    else:
        samples = np.append(np.asarray([0, nx - 1], dtype=int),
                            np.asarray(distrib.rvs(size=n - 2), dtype=int))

    return x[samples]


@vt.logger.show_input(log)
def xy_to_filled_contours(x, y, radius, shape, extent=[0, 1, 0, 1]):
    """
    Project of set of points to a 2d array and build filled contours based on
    a given radius.

    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    radius : float
        Reference radius.
    shape : (int, int)
        Output map array shape.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 2d, int
        Resulting 2d array with values in {0, 1}.
    
    """

    w = vt.grid.xy_to_map(x, y, shape, extent=extent)

    ir = int(radius * max(shape))
    disk = skimage.morphology.disk(ir)

    w = scipy.signal.convolve2d(w, disk, mode='same', boundary='symm')
    w = np.where(w > 0, 1.0, 0.0)
    w = vt.op.smooth_gaussian(w, sigma_radius=radius / 2)
    w = np.where(w > 0.5, 1, 0)
    w = scipy.ndimage.binary_fill_holes(w)

    return w


@vt.logger.show_input(log)
def xy_to_ij(x, y, shape, extent=[0, 1, 0, 1]):
    """
    Convert x, y coordinates to i, j indices.

    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    shape : (int, int)
        Output map array shape.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 1d, int
        'i' indices.
    out : ndarray, 1d, int
        'j' indices.
    
    """

    # remove points outside domain
    idx = np.where((x >= extent[0]) & (x <= extent[1]) \
                 & (y >= extent[2]) & (y <= extent[3]))
    x = x[idx]
    y = y[idx]

    if len(x):
        # convert to indices
        i = (x - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1)
        j = (y - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1)

        i = i.astype(int)
        j = j.astype(int)

    else:
        i = np.zeros(0, dtype=int)
        j = np.zeros(0, dtype=int)

    return i, j


@vt.logger.show_input(log)
def xy_to_map(x, y, shape, filling_value=1, extent=[0, 1, 0, 1]):
    """
    Project of set of points to a 2d array.

    Parameters
    ----------
    x : ndarray, 1d, float
        'x' location of the input points.
    y : ndarray, 1d, float
        'y' location of the input points.
    shape : (int, int)
        Output map array shape.
    filling_value : float or ndarray, optional
        Value assign to the points (can be of shape 'shape'). Default is 1.
    extent : tuple of 4 floats
        Extent of the resulting map (xmin, xmax, ymin, ymax). Points outside
        this area are discarded. Default is [0, 1, 0, 1].
        
    Returns
    -------
    out : ndarray, 2d, float
        Resulting 2d array.
    
    """

    w = np.zeros(shape)

    # remove points outside domain
    idx = np.where((x >= extent[0]) & (x <= extent[1]) \
                 & (y >= extent[2]) & (y <= extent[3]))
    x = x[idx]
    y = y[idx]

    if len(x):
        # convert to indices
        xs = (x - extent[0]) / (extent[1] - extent[0]) * (shape[0] - 1)
        ys = (y - extent[2]) / (extent[3] - extent[2]) * (shape[1] - 1)

        # fill map
        if isinstance(filling_value, np.ndarray):
            w[xs.astype(int), ys.astype(int)] = filling_value[xs.astype(int),
                                                              ys.astype(int)]
        else:
            w[xs.astype(int), ys.astype(int)] = filling_value

    return w
