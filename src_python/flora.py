"""Flora generation.
"""

import numpy as np
import os
import sklearn.neighbors
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


class TreeGrowthGrammar():
    """
    Growth grammar definition of a given specie.

    Attributes
    ----------

    Methods
    -------
    use_preset
        Use a predefined preset to define the growth grammar.
        
    """

    @vt.logger.show_input(log)
    def __init__(self,
                 name='',
                 radius=[],
                 a=[],
                 mu=[],
                 sigma=[],
                 growth_rate=1,
                 diffusion_radius=20,
                 sun_influence=1,
                 moisture_influence=1):
        """
        Constructor.

        Parameters
        ----------
        name : str, optional
            Specie name. Default is ''.
        radius : [float, float]
            Minimum and maximum radius of the tree.
        a : [float, float]
            Density amplitude in [0, 1] for height and slope influence.
        mu : [float, float]
            Density mean value for height and slope.
        sigma : [float, float]
            Density standard deviation for height and slope.
        growth_rate : float
            Growth rate.
        diffusion_radius : float
            Seed diffusion radius of influence.
        sun_influence : float
            Sun exposure influence in [-1, 1].
        moisture_influence : float
            Moisture influence in [-1, 1].

        """

        self.name = name
        self.radius = radius
        self.a = a
        self.mu = mu
        self.sigma = sigma
        self.growth_rate = growth_rate
        self.diffusion_radius = diffusion_radius
        self.sun_influence = sun_influence
        self.moisture_influence = moisture_influence

        return None

    @vt.logger.show_input(log)
    def use_preset(self, preset_name):
        """
        Define growth grammar using predefined presets.

        Parameters
        ----------
        preset_name : str
            Preset name.

        Returns
        -------
        out : TreeGrowthGrammar instance
            self
        """

        self.name = preset_name

        if preset_name == 'low_alt':
            self.radius = [2, 6]
            self.a = [1, 1]
            self.mu = [0.2, 0]
            self.sigma = [0.6, 0]

        if preset_name == 'mid_small':
            self.radius = [1, 2]
            self.a = [1, 0]
            self.mu = [0.5, 0]
            self.sigma = [0.2, 0.1]

        if preset_name == 'third':
            self.radius = [3, 5]
            self.a = [1, 1]
            self.mu = [0.6, 0]
            self.sigma = [0.3, 0.05]

        if preset_name == 'test':
            self.radius = [2, 3]
            self.a = [1, 1]
            self.mu = [0.4, 0]
            self.sigma = [0.1, 0]

        if preset_name == 'low':
            self.radius = [1, 3]
            self.a = [0.8, 1]
            self.mu = [0, 0]
            self.sigma = [0.3, 0.05]

        return self


class TreeDistribution():

    @vt.logger.show_input(log)
    def __init__(
            self,
            z,
            growth_grammar_list,
            nseeds=20,
            coeff_height=1,
            coeff_slope=0.5,
            coeff_diffusion=2,
            iterations=50,
            distance_exponent=1,
            spawning_method='random_density',  # 'random', 'random_density', 'random_pdf', 'poisson_disk'
            tree_density=None,
            poisson_ratio_min=3,
            poisson_ratio_max=10,
            density_low_cutoff=0.1,
            moisture_map=None,
            sun_map=None,
            exclusion_mask=None,
            extent=None,
            seed=-1):

        self.z = z
        self.growth_grammar_list = growth_grammar_list
        self.nseeds = nseeds
        self.coeff_height = coeff_height
        self.coeff_slope = coeff_slope
        self.coeff_diffusion = coeff_diffusion
        self.iterations = iterations
        self.distance_exponent = distance_exponent
        self.spawning_method = spawning_method
        self.tree_density = tree_density
        self.poisson_ratio_min = poisson_ratio_min
        self.poisson_ratio_max = poisson_ratio_max
        self.density_low_cutoff = density_low_cutoff
        self.moisture_map = moisture_map
        self.sun_map = sun_map
        self.exclusion_mask = exclusion_mask
        self.extent = extent
        self.seed = seed

        if self.extent is None:
            self.extent = [0, z.shape[0] - 1, 0, z.shape[1] - 1]

        #
        if self.seed > 0:
            rng = np.random.default_rng(seed)
        else:
            rng = np.random

        self.shape = z.shape
        self.label = None
        self.density = None

        if self.tree_density is None:
            # if nothing is provided the number of trees is based on
            # the domain surface: it is assumed that there is enough
            # trees to cover a fair mount of the whole surface
            rm = np.mean([gg.radius for gg in self.growth_grammar_list])
            surface = (self.extent[1] - self.extent[0]) * (self.extent[3] -
                                                           self.extent[2])
            self.ntrees = int(surface / rm**2 / 10)

        else:
            self.ntrees = int(np.prod(self.shape) * self.tree_density)

        self.ntype = len(growth_grammar_list)
        self.x_seed, self.y_seed = vt.grid.random_grid(n=nseeds,
                                                       extent=self.extent,
                                                       seed=self.seed)
        self.i_seed = rng.choice(range(self.ntype), size=nseeds)

        # create density and species label maps
        self.update_maps()

        return None

    @vt.logger.show_input(log)
    def update_maps(self):

        def weight(h, mu, sigma):
            d = np.exp(-(h - mu)**2 / sigma**2)
            return vt.op.remap(d)

        # grid used for diffusion weight
        x, y = np.meshgrid(np.linspace(0, self.shape[0] - 1, self.shape[0]),
                           np.linspace(0, self.shape[1] - 1, self.shape[1]),
                           indexing='ij')

        # compute global label and density maps
        label = -np.ones(self.shape, dtype=int)
        density = np.zeros(self.shape)
        weight_max = -np.ones(self.shape)

        # loop over the tree species
        for i, gg in enumerate(self.growth_grammar_list):

            # height
            if gg.sigma[0]:
                weight_h = weight(self.z, mu=gg.mu[0], sigma=gg.sigma[0])
            else:
                weight_h = np.ones(self.shape)
                gg.a[0] = 1

            # slope
            if gg.sigma[1]:
                dz = vt.op.gradient_norm(self.z)
                weight_s = weight(dz, mu=gg.mu[1], sigma=gg.sigma[1])
            else:
                weight_s = np.ones(self.shape)
                gg.a[1] = 1

            # diffusion weight
            weight_d = np.zeros(self.shape)
            for k in range(self.nseeds):

                if self.i_seed[k] == i:
                    r2 = (x - self.x_seed[k])**2 + (y - self.y_seed[k])**2
                    rs = self.growth_grammar_list[
                        self.i_seed[k]].diffusion_radius
                    weight_d = np.maximum(
                        np.where(r2 < rs**2, 1 - r2**0.5 / rs, 0), weight_d)

            # add all the weight for the current species
            wtmp = self.coeff_height * weight_h \
                + self.coeff_slope * weight_s \
                + self.coeff_diffusion * weight_d

            # add moisture and sun influence on the weight
            for vmap, v in zip([self.moisture_map, self.sun_map],
                               [gg.moisture_influence, gg.sun_influence]):

                if (vmap is not None):
                    if v > 0:
                        # positive influence coefficient => more flora
                        # for high values of the map
                        wtmp *= vt.op.remap(vmap, 1 - v, 1)

                    else:
                        # negative influence coefficient => less flora
                        # for high values of the map
                        wtmp *= vt.op.remap(vmap, 1, 1 + v)

            # eventually, update wich species holds the highest
            # *weight* to build up the species label map (indeed based
            # on the weigth, not the species seeding density)
            idx = np.where(wtmp > weight_max)

            label[idx] = i
            density[idx] = gg.a[0] * weight_h[idx] * gg.a[1] * weight_s[idx]
            weight_max[idx] = wtmp[idx]

        # store everything
        self.label = label
        self.density = vt.op.remap(density)

        return self.label, self.density

    @vt.logger.show_input(log)
    def spawn_trees(self):

        # --- generate tree repartition and species labelling (makes
        # --- use of a k-nearest classifier to allow species
        # --- propagation)

        # training set
        x, y = vt.grid.random_grid(
            n=int(self.ntrees / 5),  # TODO
            extent=self.extent,
            seed=self.seed)

        ii = (vt.op.remap(x) * (self.shape[0] - 1)).astype(int)
        jj = (vt.op.remap(y) * (self.shape[1] - 1)).astype(int)

        label = self.label[ii, jj]

        # classifier
        knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=3,
                                                     algorithm='kd_tree')
        knn.fit(np.dstack((x, y))[0], label)

        # sample tree locations
        if self.spawning_method == 'random':
            x, y = vt.grid.random_grid(n=self.ntrees,
                                       extent=self.extent,
                                       seed=self.seed)

        elif self.spawning_method == 'random_density':
            x, y = vt.grid.random_grid_density(n=self.ntrees,
                                               density=self.density,
                                               extent=self.extent,
                                               seed=self.seed)

        elif self.spawning_method == 'random_pdf':
            x, y = vt.grid.random_grid_pdf(n=self.ntrees,
                                           pdf=self.density,
                                           extent=self.extent,
                                           seed=self.seed)

        elif self.spawning_method == 'poisson_disk':
            # new density-based random grid using a Poisson disk
            # solver (with variable radius: minimum radius on species
            # min radius)
            rmin = np.zeros(self.shape)

            for s in range(self.ntype):
                idx = np.where(self.label == s)
                rs = self.growth_grammar_list[s].radius[0]
                rmin[idx] = vt.op.remap(self.density[idx],
                                        self.poisson_ratio_max * rs,
                                        self.poisson_ratio_min * rs)

            x, y = vt.grid.poisson_disk_sampler(rmin,
                                                extent=self.extent,
                                                seed=self.seed)

        # remove lonely nodes
        mask = np.where(
            self.density < self.density_low_cutoff * self.density.max(), 0, 1)
        x, y, _ = vt.grid.mask_filter(x, y, mask, extent=self.extent)

        # apply classifier on the new grid to get the species label
        label = knn.predict(np.dstack((x, y))[0])

        # --- determine tree growth using a self-thining procedure
        # --- (home-made...)

        # re-use the k-neighbors classifier to determine the
        # 5th-neighbors of each trees
        knn = sklearn.neighbors.KNeighborsClassifier(n_neighbors=5,
                                                     algorithm='kd_tree')
        knn.fit(np.dstack((x, y))[0], label)

        neighbors = knn.kneighbors()[1][:]
        distance = knn.kneighbors()[0][:]

        rmin = np.zeros(len(x))
        rmax = np.zeros(len(x))
        gr = np.zeros(len(x))

        for s in np.unique(label):
            idx = np.where(label == s)
            rmin[idx] = self.growth_grammar_list[s].radius[0]
            rmax[idx] = self.growth_grammar_list[s].radius[1]
            gr[idx] = self.growth_grammar_list[s].growth_rate

        # --- grow the trees...
        log.debug('self-thining growth...')

        radius = self_thining_growth(x=x,
                                     y=y,
                                     neighbors=neighbors,
                                     distance=distance,
                                     radius_min=rmin,
                                     radius_max=rmax,
                                     growth_rate=gr,
                                     iterations=self.iterations,
                                     distance_exponent=self.distance_exponent)

        # retrieve elevation (also returned for conveniency)
        ii = (vt.op.remap(x) * (self.shape[0] - 1)).astype(int)
        jj = (vt.op.remap(y) * (self.shape[1] - 1)).astype(int)
        z = self.z[ii, jj]

        # --- apply exclusion mask
        if self.exclusion_mask is not None:
            mask = np.where(self.exclusion_mask != 0, 0, 1)
            x, y, idx = vt.grid.mask_filter(x,
                                            y,
                                            mask=mask,
                                            extent=self.extent)
            z = z[idx]
            label = label[idx]
            radius = radius[idx]

        # --- remove dead trees
        idx = np.where(radius > 0)

        return x[idx], y[idx], z[idx], label[idx], radius[idx]


@vt.logger.show_input(log)
def self_thining_growth(x,
                        y,
                        neighbors,
                        distance,
                        radius_min,
                        radius_max,
                        growth_rate,
                        iterations,
                        distance_exponent,
                        dt=None):

    # number of plants and nb of neighbors
    nn = len(x)
    ng = len(neighbors[0])

    # recast some input
    if not (isinstance(radius_min, np.ndarray)):
        radius_min *= np.ones(nn)

    if not (isinstance(radius_max, np.ndarray)):
        radius_max *= np.ones(nn)

    if not (isinstance(growth_rate, np.ndarray)):
        growth_rate *= np.ones(nn)

    if dt is None:
        dt = radius_min.min() / 5

    # plant radius initialization
    radius = np.ones(nn) * radius_min.min()

    # negative influence coefficents from other plants
    cneg = np.ones((nn, ng))

    for it in range(iterations):

        do_growth = np.ones(nn)

        # start enforcing a strict minimum radius only after some
        # iterations to avoid over-mortality in very dense zones
        if it < int(0.8 * iterations):
            radius_lim = np.ones(nn) * radius_min.min()
        else:
            radius_lim = radius_min

        # compute first the influence cofficients
        for i in range(nn):
            if radius[i] < radius_lim[i]:
                # if the plant is too small is is considered dead
                radius[i] = 0
                do_growth[i] = 0

            else:
                # check radius constraint
                rcheck = distance[i] - radius[i] - radius[neighbors[i]]

                if (rcheck < 0).any():
                    # prevent further growth of the plant
                    do_growth[i] = 0
                    radius[i] -= dt * radius[i]

                else:
                    # else keep growing...
                    do_growth[i] = 1

                    #
                    delta_size = radius[neighbors[i]] - radius[i]
                    cneg[i] = np.where(delta_size > 0, delta_size, 0)
                    cneg[i] /= distance[i]**distance_exponent

        # add by plant and overall normalization
        radius_target = (1 - vt.op.remap(np.sum(cneg, axis=1)))
        radius_target = vt.op.remap(radius_target, radius_lim, radius_max)

        # growth
        radius += dt * (radius_target - radius) * do_growth * growth_rate

    return radius
