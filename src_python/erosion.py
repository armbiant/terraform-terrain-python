import copy
import numpy as np
import os
import scipy
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def hydraulic_musgrave(z,
                       water_level=0.001,
                       iterations=200,
                       kd=0.1,
                       kc=5,
                       ks=0.3,
                       tau=1e-2):
    """
    Perform hydraulic erosion/deposition of Musgrave et al. (1989) on a
    given heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    water_level : float, ndarray
        Water level, normalized by the elevation peak to peak amplitude, can
        be a float or locally defined on the heightmap.
        Default is 0.001 with a magnitude modulated by the elevation.
    iterations : int
        Maximum number of iterations. Default is 200.
    kd : float
        Sediment deposition rate, in [0, 1]. Default is 0.1
    kc : float
        Sediment capacity. Default is 5.
    ks : float
        Soil softness constant, in [0, 1]. Default is 0.3.
    tau : float
        Water level relaxation "time", in [0, 1], is related to the moisture
        (raining) intensity, i.e. at what rate the water level is replenished
        to its initial value. Default is 0.01.

    Returns
    -------
    out : ndarray
        Eroded (hydraulic) heightmap.

    References
    ----------
    - Musgrave, F.K., Kolb, C.E., and Mace, R.S. 1989. The synthesis
      and rendering of eroded fractal terrains. ACM SIGGRAPH Computer
      Graphics 23, 41–50.

    """

    # backup min and max for output rescaling
    zmin = z.min()
    zmax = z.max()

    if not (isinstance(water_level, np.ndarray)):
        water_level = water_level * vt.op.remap(z, vmin=0, vmax=1) * z.ptp()

    sediment_level = np.zeros(z.shape)

    z, w, s = vterrain_core.hydraulic_eroder_musgrave(z=vt.op.remap(z),
                                                      w=water_level,
                                                      s=sediment_level,
                                                      iterations=iterations,
                                                      kd=kd,
                                                      kc=kc,
                                                      ks=ks,
                                                      tau=tau)

    return vt.op.remap(z, vmin=zmin, vmax=zmax)


@vt.logger.show_input(log)
def hydraulic_particles(z,
                        z_bedrock=None,
                        moisture_map=None,
                        particle_density=0.2,
                        water_evaporation_rate=0.01,
                        radius=None,
                        kernel=None,
                        seed=-1,
                        gravity=1,
                        p_mass=1,
                        p_capacity=1,
                        p_erosion=0.01,
                        p_deposition=0.01,
                        velocity_lim_deposition=0.1,
                        velocity_lim_erosion=0.8,
                        p_inertia=0.2,
                        p_drag=0.05,
                        iterations=2000,
                        smoothing=True):
    """
    Perform hydraulic erosion/deposition with particles simulation on a given
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    z_bedrock : ndarray, optional
        Bedrock elevation, no erosion can be performed below this elevation.
        Default is 'None', leading to a very low bedrock level (i.e. no
        influence).
    moisture_map : ndarray, optional
        Moisture map (quantity of rain) as an ndarray between 0 and 1. Default
        is 'None', leading to a uniform moisture map.
    particle_density : float, optional
        Particle density: the number of particles simulated equals the
        particle density multiplied by the heightmap size = shape[0]*shape[1].
        Default is 0.2.
    water_evaporation_rate : float, optional
        Particle water evaporation rate (grid independent). Default is 0.01.
    radius : float or str, optional
        Particle radius. If set to None, the particle has the size of a pixel
        (minimum size). If set in ]0, 1], this parameter corresponds to the
        erosion kernel radius, adjusted to the grid shape. When radius is set,
        the eorion pattern is grid resolution independent. Default is None.
    kernel : ndarray, optional
        Erosion kernel. If set to None, a cone kernel is used, if not,
        the kernel must have an 'odd' square shape. Default is None.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    gravity : float, optional
        Gravity values (don't touch that). Default is 1.
    p_mass : float, optional
        Particle mass (don't touch that). Default is 1.
    p_capacity : float, optional
        Particle sediment capacity. Default is 1.
    p_erosion : float, optional
        Particle erosion rate. Default is 0.01.
    p_deposition : float, optional
        Particle deposition rate. Default is 0.01.
    velocity_lim_deposition : float, optional
        Deposition occurs below this velocity (see Hjulstrom's diagram).
        Default is 0.1.
    velocity_lim_erosion : float, optional
        Erosion occurs above this velocity. Default is 0.8.
    p_inertia : float, optional
        Particle direction inertia. Default is 0.2.
    p_drag : float or ndarray, optional
        Particle drag coefficient, can be defined locally as an ndarray.
        Default is 0.05.
    iterations : int, optional
        Maximum number of iterations for each particle simulation. Default
        is 2000.
    smoothing : bool, optional
        Apply Laplace smoothing as post-processing step. Default is True.

    Returns
    -------
    out : ndarray
        Eroded (hydraulic) heightmap.

    References
    ----------
    - Hjulström, F. 1935. Studies of the morphological activity of rivers as
      illustrated by the river fyris. Almqvist & Wiksells.

    """

    # backup min and max for output rescaling
    zmax = z.max()
    zmin = z.min()

    # pre-defined particle classes
    if type(radius) == str:
        log.debug(
            'using pre-defined particle class: {}, overriding some input parameters'
            .format(radius))

        if radius == 'small':
            # SMALL: highly erosive, closely follow the landscape details
            velocity_lim_deposition = 0.0
            velocity_lim_erosion = 0.5
            p_capacity = 0.2
            p_erosion = 0.01
            p_deposition = 0.0
            p_inertia = 0.05
            p_drag = 0.03
            radius = None

        elif radius == 'medium':
            velocity_lim_deposition = 0.1
            velocity_lim_erosion = 0.5
            p_capacity = 0.3
            p_deposition = 0.01
            p_erosion = 0.05
            p_inertia = 0.2
            p_drag = 0.05
            radius = 1e-3

        elif radius == 'large':
            # LARGE: large inertia (tends to go in a straight line)
            # and a large foot-print
            velocity_lim_deposition = 0.8
            velocity_lim_erosion = 1.0
            p_capacity = 0.4
            p_deposition = 0.01
            p_erosion = 0.1
            p_inertia = 0.95
            p_drag = 0.2
            radius = 5e-3

    # add missing input
    if z_bedrock is None:
        z_bedrock = z - 10 * z.ptp()

    if moisture_map is None:
        moisture_map = np.ones(z.shape)

    if not (isinstance(p_drag, np.ndarray)):
        p_drag = p_drag * np.ones(z.shape)

    # define core solver parameters based on 'less' grid resolution
    # dependent python wrapper parameters
    n_particles = particle_density * z.shape[0] * z.shape[1]

    if kernel is None:
        # convert radius between [0, 1] to radius in pixels
        if radius is None:
            radius = 0
            kernel = np.zeros((0, 0))
        else:
            radius = radius * max(z.shape)
            nr = int(np.ceil(radius))
            kernel = vt.kernel.cone((2 * nr + 1, 2 * nr + 1))

    else:
        radius = float((kernel.shape[0] - 1) / 2)
        # make sure the kernel is normalized
        if np.sum(kernel) > 0:
            kernel = kernel / np.sum(kernel)

    # time step
    particle_velocity_reference = 2.0 * gravity
    dt = 1.0 / particle_velocity_reference

    z = vterrain_core.hydraulic_eroder_particles(
        z=vt.op.remap(z),
        z_bedrock=z_bedrock,
        moisture_map=moisture_map,
        iterations=iterations,
        n_particles=n_particles,
        dt=dt,
        gravity=gravity,
        water_evaporation_rate=water_evaporation_rate,
        velocity_lim_deposition=velocity_lim_deposition,
        velocity_lim_erosion=velocity_lim_erosion,
        p_mass=p_mass,
        p_capacity=p_capacity,
        p_erosion=p_erosion,
        p_deposition=p_deposition,
        p_inertia=p_inertia,
        p_drag=p_drag,
        radius=radius,
        seed=seed,
        kernel=kernel)

    if smoothing:
        z = vt.op.smooth_laplace(z, iterations=2)

    return vt.op.remap(z, vmin=z.min() * zmax / z.max(), vmax=zmax)


@vt.logger.show_input(log)
def hydraulic_particles_multiscale(
    z,
    particle_classes_dict={
        'large': {
            'radius': 'large',
            'particle_density': 0.1,
        },
        'medium': {
            'radius': 'medium',
            'particle_density': 0.1,
        },
        'small': {
            'radius': 'small',
            'particle_density': 0.1,
        }
    }):
    """
    Wrapper to performe multiscale hydraulic erosion.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    particle_classes_dict : dictionnary, optional
        Dictionnary defining erosion parameters for each particle class.

    Returns
    -------
    out : ndarray
        Eroded (hydraulic) heightmap.

    """

    for k, v in particle_classes_dict.items():
        log.debug('particle class: {} -> {}'.format(k, v))
        z = vt.erosion.hydraulic_particles(z, **v)

    return z


@vt.logger.show_input(log)
def hydraulic_flow_stream(z,
                          slope_threshold=0.1,
                          intensity=0.01,
                          flow_stream=1,
                          iterations=10):
    """
    Perform simple explicit hydraulic erosion/deposition on a given
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    slope_threshold : float
        Slope limit between erosion and deposition. Slope is normalized
        between 0 (flat) and 1 (vertical). Default is 0.1.
    intensity : float or ndarray
        Elevation modification intensity. Default is 0.1.
    flow_stream : float or ndarray
        Local water volume flow: can be constant or aligned with the
        river flow rate is known. Default is 1.
    iterations : integer
        Maximum number of iterations. Default is 10.

    Returns
    -------
    out : ndarray
        Eroded (hydraulic) heightmap.

    """

    # backup min and max for output rescaling
    zmin = z.min()
    zmax = z.max()

    for _ in range(iterations):
        # gradient norm = terrain slope
        dn = vt.op.remap(vt.op.gradient_norm(z), vmin=0, vmax=1)

        # elevation variations
        dz = np.zeros(z.shape)

        # erosion
        idx = np.where(dn > slope_threshold)
        dz[idx] = -5 * (dn[idx] - slope_threshold) / (1 - slope_threshold)

        # deposition
        idx = np.where(dn < slope_threshold)
        dz[idx] = 1 - dn[idx] / slope_threshold

        z += intensity * flow_stream * vt.op.remap(dz, vmin=0,
                                                   vmax=1) * z.ptp()

    return vt.op.remap(z, vmin=zmin, vmax=zmax)


@vt.logger.show_input(log)
def hydraulic_thermal(z,
                      z_bedrock=None,
                      iterations=20,
                      hydraulic_param={},
                      thermal_param={}):
    """
    Wrapper to combine hydraulic and thermal erosion.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    z_bedrock : ndarray, optional
        Bedrock elevation, no erosion can be performed below this elevation.
        Default is 'None', leading to a very low bedrock level (i.e. no
        influence).
    iterations : int, optional
        Number of iterations of hydraulic/thermal erosion. Default is 20.
    hydraulic_param : dictionnary, optional
        Additional parameters passed to the hydraulic eroder
        'vt.erosion.hydraulic_particles'.
    thermal_param : dictionnary, optional
        Additional parameters passed to the thermal eroder 'vt.erosion.thermal'.

    Returns
    -------
    out : ndarray
        Eroded heightmap.

    References
    ----------
    - Olsen, J. 2004. Realtime procedural terrain generation. University of
      Southern Denmark.

    """

    # re-set some default parameters
    if 'iterations' not in thermal_param.keys():
        thermal_param['iterations'] = 100

    for _ in range(iterations):
        z = vt.erosion.hydraulic_particles(z,
                                           z_bedrock=z_bedrock,
                                           **hydraulic_param)
        z, smap = vt.erosion.thermal(z, z_bedrock=z_bedrock, **thermal_param)

    return z, smap


@vt.logger.show_input(log)
def landslide(z,
              mask,
              smoothing_size=None,
              low_ratio=1e-2,
              shape_exponent=4,
              thermal_param={}):
    """
    Generate landslides based on thermal erosion.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    mask : ndarray
        Landslide mask (1: landslide, 0: None).
    smoothing_size : int, optional
        Kernel smoothing size. Default is domain shape / 20.
    low_ratio : float, optional
        Low value of talus as a ratio. Default is 0.01.
    shape_exponent : int, optional
        Talus decrease exponent. Default is 4.
    thermal_param : dictionnary, optional
        Additional parameters passed to the thermal eroder 'vt.erosion.thermal'.

    Returns
    -------
    out : ndarray
        Heightmap with landslides.
    
    """

    if smoothing_size is None:
        nk = max(3, int(max(z.shape) / 20))
    else:
        nk = smoothing_size

    k = vt.kernel.smooth_cosine((nk, nk))
    mask_smooth = scipy.signal.fftconvolve(mask, k, mode='same')
    mask_sharp = np.where(mask_smooth > 1e-10, 1.0, 0.0)

    mask_label, nlabels = scipy.ndimage.label(mask_sharp)

    mask_shape = np.ones(z.shape)
    for v in range(1, nlabels):
        idx = np.where(mask_label == v)
        mask_shape[idx] = 1 - vt.op.lerp(mask_smooth[idx], mask_sharp[idx],
                                         vt.op.remap(z[idx])**shape_exponent)

    talus = vt.op.gradient_talus(z) * vt.op.remap(mask_shape, low_ratio, 1)

    z, smap = vt.erosion.thermal(z, talus=talus, **thermal_param)

    return z


@vt.logger.show_input(log)
def scree_deposition(z,
                     talus,
                     drop_map=None,
                     particle_density=0.2,
                     radius=None,
                     kernel=None,
                     seed=-1,
                     dt=0.5,
                     gravity=1,
                     p_mass=1,
                     p_sediment=0.1,
                     p_deposition=0.01,
                     p_inertia=0.0,
                     p_drag=1.5,
                     iterations=2000,
                     smoothing=True):
    """
    Scree (small rocks) deposition procedure.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    talus : float or ndarray
        Talus limits. Can be defined locally.
    particle_density : float, optional
        Particle density: the number of particles simulated equals the
        particle density multiplied by the heightmap size = shape[0]*shape[1].
        Default is 0.2.
    radius : float or str, optional
        Particle radius. If set to None, the particle has the size of a pixel
        (minimum size). If set in ]0, 1], this parameter corresponds to the
        erosion kernel radius, adjusted to the grid shape. When radius is set,
        the eorion pattern is grid resolution independent. Default is None.
    kernel : ndarray, optional
        Erosion kernel. If set to None, a cone kernel is used, if not,
        the kernel must have an 'odd' square shape. Default is None.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    dt : float, optional
        Simulation time step. Default is 0.5.
    gravity : float, optional
        Gravity values (don't touch that). Default is 1.
    p_mass : float, optional
        Particle mass (don't touch that). Default is 1.
    p_sediment : float, optional
        Particle initial sediment amount. Default is 0.1.
    p_deposition : float, optional
        Particle deposition rate. Default is 0.01.
    p_inertia : float, optional
        Particle direction inertia. Default is 0.0.
    p_drag : float or ndarray, optional
        Particle drag coefficient, can be defined locally as an ndarray.
        Default is 1.5.
    iterations : int, optional
        Maximum number of iterations for each particle simulation. Default
        is 2000.
    smoothing : bool, optional
        Apply Laplace smoothing as post-processing step. Default is True.

    Returns
    -------
    out : ndarray
        Heightmap with scree deposition.
    out : ndarray
        Scree thickness.

    """

    z0 = np.copy(z)

    # backup min and max for output rescaling
    zmax = z.max()
    zmin = z.min()

    if drop_map is None:
        drop_map = np.ones(z.shape)

    if not (isinstance(talus, np.ndarray)):
        talus *= np.ones(z.shape)

    if not (isinstance(p_drag, np.ndarray)):
        p_drag = p_drag * np.ones(z.shape)

    # define core solver parameters based on 'less' grid resolution
    # dependent python wrapper parameters
    n_particles = particle_density * np.count_nonzero(drop_map > 0.05)

    if kernel is None:
        # convert radius between [0, 1] to radius in pixels
        if radius is None:
            radius = 0
            kernel = np.zeros((0, 0))
        else:
            radius = radius * max(z.shape)
            nr = int(np.ceil(radius))
            kernel = vt.kernel.cone((2 * nr + 1, 2 * nr + 1))

    else:
        radius = float((kernel.shape[0] - 1) / 2)
        # make sure the kernel is normalized
        if np.sum(kernel) > 0:
            kernel = kernel / np.sum(kernel)

    z = vterrain_core.scree_deposition(z=vt.op.remap(z),
                                       talus=talus,
                                       drop_map=drop_map,
                                       n_particles=n_particles,
                                       iterations=iterations,
                                       dt=dt,
                                       gravity=gravity,
                                       p_mass=p_mass,
                                       p_sediment=p_sediment,
                                       p_deposition=p_sediment,
                                       p_inertia=p_inertia,
                                       p_drag=p_drag,
                                       radius=radius,
                                       seed=seed,
                                       kernel=kernel)

    if smoothing:
        z = vt.op.smooth_laplace(z, iterations=2)

    z = vt.op.remap(z, vmin=zmin, vmax=zmax)

    smap = z - z0

    return z, smap


@vt.logger.show_input(log)
def sediment_deposition(z,
                        max_deposition,
                        talus,
                        iterations=5,
                        thermal_erosion_subiterations=100,
                        deposition_step=None,
                        deposition_map=None,
                        thermal_param={}):
    """
    Perform sediment deposition combine with thermal erosion on a given
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    max_deposition : float
        Maximum height of sediment deposition.
    talus : float or ndarray
        Talus limits, value between 0 (strong erosion) and 1 (no erosion).
        Can be defined locally.
    iterations : int, optional
        Number of iterations deposition/thermal erosion. Default is 5.
    thermal_erosion_subiterations : int, optional
        Number of thermal erosion iterations for each pass. Default is 100.
    deposition_step : float, optional
        Sediment thickness deposited at each iteration. Default
        is 'maximum thickness'/2.
    deposition_map : ndarray, optional
        Deposition map (defined between and 0 and 1 and used to scale the deposit
        thickness). Default is a uniform map.
    thermal_param : dictionnary, optional
        Additional parameters passed to the thermal eroder 'vt.erosion.thermal'.

    Returns
    -------
    out : ndarray
        Heightmap with sediment deposition.
    out : ndarray
        Snow thickness.

    Notes
    -----
    The thickness of the sediment layer is the difference between this output
    and this input heightmap.

    """

    if deposition_step is None:
        deposition_step = max_deposition / 2

    if deposition_map is None:
        deposition_map = np.ones(z.shape)

    # sediment thickness
    smap = np.zeros(z.shape)

    for _ in range(iterations):
        dmap = copy.deepcopy(deposition_map)
        dmap[np.where(vt.op.gradient_talus(z) > talus)] = 0

        smap += deposition_step * deposition_map
        smap = np.minimum(smap, max_deposition)

        zt, _ = vt.erosion.thermal(z + smap,
                                   z_bedrock=z,
                                   auto_bedrock=False,
                                   talus=talus,
                                   iterations=thermal_erosion_subiterations,
                                   **thermal_param)
        smap = zt - z

    return z + smap, smap


@vt.logger.show_input(log)
def snow_deposition(z,
                    max_deposition,
                    snow_talus,
                    lowest_elevation=None,
                    elevation_transition_ratio=0.1,
                    sun_exposure_influence=1,
                    iterations=20,
                    thermal_erosion_subiterations=100,
                    deposition_step=None,
                    thermal_param={}):
    """
    Perform sediment deposition combine with thermal erosion on a given
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    max_deposition : float
        Maximum height of sediment deposition.
    snow_talus : float or ndarray
        Snow talus limits, value between 0 (flat) and 1 (steep). Can be defined
        locally.
    lowest_elevation : float, optional
        lowest_elevation : float
        Snow occurence lowest elevation.
    elevation_transition_ratio : float, optional
        Elevation transition between snow and no snow, normalized by
        heightmap peak-to-peak amplitude. Default is 0.05.
    sun_exposure_influence : float, optional
        Influence coefficient of the sun exposure on snow melting (0: no
        influence, 1: full influence). Default is 0.
    iterations : int, optional
        Number of iterations snow deposition/thermal erosion. Default is 20.
    thermal_erosion_subiterations : int, optional
        Number of thermal erosion iterations for each pass. Default is 100.
    deposition_step : float, optional
        Snow thickness deposited at each iteration. Default
        is 'maximum thickness'/2.
    thermal_param : dictionnary, optional
        Additional parameters passed to the thermal eroder 'vt.erosion.thermal'.

    Returns
    -------
    out : ndarray
        Heightmap with snow deposition.
    out : ndarray
        Snow thickness.

    Notes
    -----
    The thickness of the snow layer is the difference between this output
    and this input heightmap.

    """

    if lowest_elevation is None:
        lowest_elevation = z.min()

    if deposition_step is None:
        deposition_step = max_deposition / 2

    # sediment thickness
    smap = np.zeros(z.shape)

    for _ in range(iterations):
        dmap = vt.climate.snowfall_map(
            z + smap,
            lowest_elevation=lowest_elevation,
            elevation_transition_ratio=elevation_transition_ratio,
            talus=5 * snow_talus,
            sun_exposure_influence=sun_exposure_influence,
            sigma_radius=0)

        smap += deposition_step * dmap
        smap = np.minimum(smap, max_deposition)

        zt, _ = vt.erosion.thermal(z + smap,
                                   z_bedrock=z,
                                   auto_bedrock=False,
                                   talus=snow_talus,
                                   iterations=thermal_erosion_subiterations,
                                   **thermal_param)
        smap = zt - z

    return z + smap, smap


@vt.logger.show_input(log)
def splatmap(z_eroded, z_initial, z_bedrock=None):
    """
    Compute erosion/deposition splatmap.

    Parameters
    ----------
    z_eroded : ndarray
        Heightmap after erosion.
    z_initial : ndarray
        Initial heightmap.
    z_bedrock : ndarray, optional
        Bedrock heightmap. Default is None


    Returns
    -------
    out : ndarray
        Splatmap, values equals to 1 (deposition), 2 (erosion) or 3 (bedrock).

    """

    if z_bedrock is None:
        z_bedrock = np.ones(z_initial.shape) * z_initial.min()

    # splatmap (bedrock elevation is trapped here)
    zsp = np.ones(z_initial.shape) * 3

    # particle deposition <= elevation after erosion is higher than
    # the initial elevation
    idx = np.where(z_eroded >= z_initial)
    zsp[idx] = 1

    # erosion <= elevation after erosion is lower than the initial
    # elevation but higher than the bedrock elevation
    idx = np.where((z_initial > z_eroded) & (z_eroded > z_bedrock))
    zsp[idx] = 2

    return zsp


@vt.logger.show_input(log)
def thermal(z,
            talus=0.015,
            auto_bedrock=True,
            z_bedrock=None,
            ct=0.5,
            iterations=1000,
            cv_tol=1e-2,
            smooth_landing=False,
            smooth_landing_talus_ratio=0.5,
            smooth_landing_sigma_radius=0.05,
            smooth_landing_ncycles=10):
    """
    Perform thermal weathering on a given heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    talus : float or ndarray, optional
        Talus limits. Can be defined locally. Default value is 0.015.
    auto_bedrock : bool, optional
        Iterative automatic bedrock definition. Default is True.
    z_bedrock : ndarray, optional
        Bedrock elevation, no erosion can be performed below this elevation.
        Default is 'None', leading to a very low bedrock level (i.e. no
        influence).
    ct : float
        "Avalanching" intensity, between 0 and 1.
    iterations : integer
        Maximum number of iterations. Default is 1000.
    cv_tol : float
        Convergence tolerance, algorithm stops is the sum of all changes
        between two iterations is smaller than cv_tol. Default is 1e-2.
    smooth_landing : bool, optional
        Smooth talus values at the bottom of sediment piles to smooth the
        transition between corrected talus and initial map. Default is False.
    smooth_landing_talus_ratio : float, optinal
        Used when 'smooth_landing' is True. Default is 0.5.
    smooth_landing_sigma_radius : float, optinal
        Used when 'smooth_landing' is True. Default is 0.05.
    smooth_landing_ncycles : int, optional
        Used when 'smooth_landing' is True. Default is 10.
    
    Returns
    -------
    out : ndarray
        Eroded (thermal) heightmap.
    out : ndarray
        Sediment thickness.

    References
    ----------
    - Musgrave, F.K., Kolb, C.E., and Mace, R.S. 1989. The synthesis
      and rendering of eroded fractal terrains. ACM SIGGRAPH Computer
      Graphics 23, 41–50.

    """

    z0 = np.copy(z)

    if z_bedrock is None:
        z_bedrock = np.ones(z.shape) * z.min() - 10 * z.ptp()

    if not (isinstance(talus, np.ndarray)):
        talus *= np.ones(z.shape)

    if auto_bedrock:
        sub_iterations = 100
        nsub = int(np.ceil(iterations / sub_iterations))
        smooth_landing_ncycles = 1
    else:
        nsub = 1
        sub_iterations = iterations

    # cycles
    for _ in range(nsub):

        if not (smooth_landing):
            z = vterrain_core.thermal_eroder_cycle(z=z,
                                                   talus=talus,
                                                   z_bedrock=z_bedrock,
                                                   ct=ct,
                                                   iterations=sub_iterations,
                                                   cv_tol=cv_tol)

        else:
            talus_init = copy.deepcopy(talus)
            z_init = copy.deepcopy(z)

            for _ in range(smooth_landing_ncycles):
                z = vterrain_core.thermal_eroder_cycle(
                    z=z,
                    talus=talus,
                    z_bedrock=z_bedrock,
                    ct=ct,
                    iterations=int(sub_iterations / smooth_landing_ncycles),
                    cv_tol=cv_tol)

                # update mask
                mask = np.where(np.abs(z - z_init) < 1e-6 * z.ptp(), 0.0, 1.0)
                if smooth_landing_sigma_radius:
                    sigma = smooth_landing_sigma_radius * max(z.shape)
                    scipy.ndimage.gaussian_filter(mask,
                                                  sigma=sigma,
                                                  output=mask)

                # modify talus distribution with smaller values at the
                # transitions sediment <-> ground
                talus = talus_init * vt.op.remap(
                    mask, vmin=smooth_landing_talus_ratio, vmax=1)

        if auto_bedrock:
            # only keep what's above the initial ground level
            z = np.maximum(z0, z)
            z_bedrock = np.where(z0 > z, z0, z0 - 1 * z0.ptp())

    return z, np.maximum(z - z0, 0)


@vt.logger.show_input(log)
def wind_particles(z,
                   smap=None,
                   wind_angle=30 / 180 * np.pi,
                   particle_density=0.05,
                   dt=1,
                   radius=5e-3,
                   kernel=None,
                   seed=-1,
                   p_gravity=0.05,
                   p_initial_mass=0.005,
                   p_velocity_relaxation=0.01,
                   p_abrasion=0.005,
                   p_suspension=0.005,
                   iterations=500):
    """
    Perform hydraulic erosion/deposition with particles simulation on a given
    heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    smap : ndarray, optional
        Sediment map. Default is 1/20th of z peak to peak amplitude
    wind_angle : ndarray or flaot, optional
        Wind angle float or map in radian. Default value corresponds to 30°.
    particle_density : float, optional
        Particle density: the number of particles simulated equals the
        particle density multiplied by the heightmap size = shape[0]*shape[1].
        Default is 0.1.
    dt : float, optional
        Time step. Default is 1.
    radius : float, optional
        Particle radius. If set to None, the particle has the size of a pixel
        (minimum size). If set in ]0, 1], this parameter corresponds to the
        erosion kernel radius, adjusted to the grid shape. When radius is set,
        the eorion pattern is grid resolution independent. Default is None.
    kernel : ndarray, optional
        Erosion kernel. If set to None, a smooth cosine kernel is used, if not,
        the kernel must have an 'odd' square shape. Default is None.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).
    p_gravity : float, optional
        Gravity factor (drives particle acceleration). Default is 0.05.
    p_initial_mass : float, optional
        Particle initial mass. Default is 0.005.
    p_velocity_relaxation : float, optional
        Particle velocity relaxation. Default is 0.01.
    p_abrasion : float, optional
        Particle abrasion rate. Default is 0.005.
    p_suspension : float, optional
        Particle suspension rate. Default is 0.005.
    iterations : int, optional
        Maximum number of iterations for each particle simulation. Default
        is 500.

    Returns
    -------
    out : ndarray
        Sediment map.

    """

    # convert scalar inputs to field
    if smap is None:
        smap = z.ptp() / 20

    if not (isinstance(smap, np.ndarray)):
        smap *= np.ones(z.shape)

    if not (isinstance(wind_angle, np.ndarray)):
        wind_angle *= np.ones(z.shape)

    windx = np.cos(wind_angle)
    windy = np.sin(wind_angle)

    if kernel is None:
        # convert radius between [0, 1] to radius in pixels
        if radius is None:
            radius = 0
            kernel = np.zeros((0, 0))
        else:
            radius = radius * max(z.shape)
            nr = int(np.ceil(radius))
            # kernel = vt.kernel.smooth_cosine((2 * nr + 1, 2 * nr + 1))
            kernel = vt.kernel.gaussian((2 * nr + 1, 2 * nr + 1))

    else:
        radius = float((kernel.shape[0] - 1) / 2)
        # make sure the kernel is normalized
        if np.sum(kernel) > 0:
            kernel = kernel / np.sum(kernel)

    n_particles = particle_density * z.shape[0] * z.shape[1]

    smap = vterrain_core.wind_eroder_particles(
        z=z,
        smap=smap,
        windx=windx,
        windy=windy,
        iterations=iterations,
        n_particles=n_particles,
        dt=dt,
        p_initial_mass=p_initial_mass,
        p_gravity=p_gravity,
        p_velocity_relaxation=p_velocity_relaxation,
        p_suspension=p_suspension,
        p_abrasion=p_abrasion,
        radius=radius,
        seed=seed,
        kernel=kernel)

    return smap
