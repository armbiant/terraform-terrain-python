import logging
import os

sdic = {
    # logger
    'LOGGER_LEVEL': logging.INFO,
    'LOGGER_SHOW_FUNCTION_INPUT': False,  # w/ debug level
    'LOGGER_SHOW_FUNCTION_ENTERING': True,
    'LOGGER_FILE': None,
}

# convert settings dictionary to variables, but check before if the
# default value of a setting has been over overridden by an
# environment vairable
for k, v in sdic.items():
    environ_value = os.environ.get('VTERRAIN_' + k)
    if k == 'LOGGER_LEVEL' and environ_value:
        environ_value = getattr(logging, environ_value)

    locals()[k] = (environ_value or v)
