"""Rendering framework.
"""

import matplotlib.pyplot as plt
import numpy as np
import os
import os
import pyrender
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def show(mesh, print_kcmds=True, offscreen=False, plot_image=True):
    '''
    Open rendering window and render the given mesh(es).

    Parameters
    ----------
    mesh : trimesh.Trimesh, singleton or list
        Mesh(es) to render.
    print_kcmds : bool
        Print the viewer keyboard commands in the console. Default is True.
    offscreen : bool, optional
        Turn on or off the off screen rendering. Default is False.
    plot_image : bool, optional
        Plot offscreen rendering. Default is True.

    Returns
    -------
    None
    
    '''

    if type(mesh) != list:
        mesh = [mesh]

    scene = pyrender.Scene(ambient_light=np.array([0.2, 0.2, 0.2, 1.0]),
                           bg_color=(15, 15, 15))

    centroid = np.zeros(3)
    for m in mesh:
        centroid += np.array(m.centroid)
        rmesh = pyrender.Mesh.from_trimesh(m, smooth=True)
        scene.add(rmesh)
    centroid /= len(mesh)

    if print_kcmds and not (offscreen):
        print_keyboard_cmds()

    if not (offscreen):
        viewer = pyrender.Viewer(scene,
                                 viewport_size=(800, 600),
                                 use_direct_lighting=True,
                                 cull_faces=False,
                                 shadows=False,
                                 show_world_axis=False,
                                 rotate_rate=np.pi / 20)

        cam_pose = viewer._camera_node.matrix

        return None

    else:
        os.environ['PYOPENGL_PLATFORM'] = 'egl'
        light = pyrender.SpotLight(color=np.ones(3),
                                   intensity=15.0,
                                   innerConeAngle=np.pi / 2.5,
                                   outerConeAngle=np.pi / 2)

        cam = pyrender.PerspectiveCamera(yfov=(np.pi / 3.0))

        scale = 0.9
        s2 = 1.0 / np.sqrt(2.0)
        cp = np.eye(4)
        cp[:3, :3] = np.array([[0.0, -s2, s2], [1.0, 0.0, 0.0], [0.0, s2, s2]])
        hfov = np.pi / 6.0
        dist = scale / (2.0 * np.tan(hfov))
        cp[:3, 3] = dist * np.array([1.0, 0.0, 1.0]) + centroid

        cam_pose = cp

        scene.add(cam, pose=cam_pose)

        # --- side light
        cp = np.eye(4)
        alpha = 45 / 180 * np.pi
        ca = np.cos(alpha)
        sa = np.sin(alpha)
        cp[:3, :3] = np.array([[0.0, sa, -ca], [-ca, 0.0, 0.0], [0.0, 1.0,
                                                                 sa]])
        cp[:3, 3] = dist * np.array([-1.5, 0.0, 2.0]) + centroid
        cam_pose = cp

        scene.add(light, pose=cam_pose)

        r = pyrender.OffscreenRenderer(viewport_width=640 * 2,
                                       viewport_height=480 * 2)
        img, depth = r.render(scene)
        r.delete()

        if plot_image:
            plt.figure()
            plt.axis('off')
            plt.imshow(img)

        return img


@vt.logger.show_input(log)
def birdview(mesh, plot_image=True):
    '''
    Render (offscreen) a birdview of the mesh.

    Parameters
    ----------
    mesh : trimesh.Trimesh, singleton or list
        Mesh(es) to render.
    plot_image : bool, optional
        Plot offscreen rendering. Default is True.

    Returns
    -------
    None
    
    '''
    if type(mesh) != list:
        mesh = [mesh]

    scene = pyrender.Scene(bg_color=(255, 255, 255))

    centroid = np.zeros(3)
    for m in mesh:
        centroid += np.array(m.centroid)
        rmesh = pyrender.Mesh.from_trimesh(m, smooth=True)
        scene.add(rmesh)
    centroid /= len(mesh)

    os.environ['PYOPENGL_PLATFORM'] = 'egl'

    light = pyrender.DirectionalLight(color=np.ones(3), intensity=10)

    cam = pyrender.OrthographicCamera(xmag=0.6, ymag=0.6)

    # --- top view
    scale = 0.9
    cp = np.eye(4)
    hfov = np.pi / 6.0
    dist = scale / (2.0 * np.tan(hfov))
    cp[:3, 3] = dist * np.array([0.0, 0.0, 1.0]) + centroid
    cam_pose = cp

    scene.add(cam, pose=cam_pose)

    # --- side light
    cp = np.eye(4)
    alpha = 45 / 180 * np.pi
    ca = np.cos(alpha)
    sa = np.sin(alpha)
    cp[:3, :3] = np.array([[0.0, sa, -ca], [-ca, 0.0, 0.0], [0.0, 1.0, sa]])
    cp[:3, 3] = dist * np.array([-1.5, 0.0, 2.0]) + centroid
    cam_pose = cp

    scene.add(light, pose=cam_pose)

    r = pyrender.OffscreenRenderer(viewport_width=2048, viewport_height=2048)

    flags = pyrender.constants.RenderFlags()

    img, depth = r.render(scene, flags=flags.SHADOWS_ALL)
    r.delete()

    if plot_image:
        plt.figure()
        plt.axis('off')
        plt.imshow(img)

    return img


@vt.logger.show_input(log)
def print_keyboard_cmds():
    '''
    Print rendering GUI keyboard commands.
    '''

    print('''
    a: Toggles rotational animation mode.
    c: Toggles backface culling.
    f: Toggles fullscreen mode.
    h: Toggles shadow rendering.
    i: Toggles axis display mode (no axes, world axis, mesh axes, all axes).
    l: Toggles lighting mode (scene lighting, Raymond lighting, or direct lighting).
    m: Toggles face normal visualization.
    n: Toggles vertex normal visualization.
    o: Toggles orthographic mode.
    q: Quits the viewer.
    r: Starts recording a GIF, and pressing again stops recording and opens a file dialog.
    s: Opens a file dialog to save the current view as an image.
    w: Toggles wireframe mode (scene default, flip wireframes, all wireframe, or all solid).
    z: Resets the camera to the initial view.
    ''')
