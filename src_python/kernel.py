"""Kernels for convolution product.
"""

import numpy as np
import os
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


def biweight(shape):
    """
    Return a "biweight" 2d kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    r2 = x**2 + y**2
    k = (1 - r2)**2
    k[np.where(r2 > 1)] = 0
    return k / np.sum(k)


def cone(shape):
    """
    Return a cone-like kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """
    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    kernel = np.maximum(np.zeros(shape), 1 - (x**2 + y**2)**0.5)
    return kernel / np.sum(kernel)


def gabor_shape(shape, f0, w0, footprint_threshold=0.05):
    """
    Return a Gabor kernel as an ndarray of a given shape.

    Kernel width is deduced from the array shape: the kernel footprint
    is limited to where envelop is larger than the value
    'footprint_threshold'.
    
    Parameters
    ----------
    shape : (int, int)
        Kernel nd array shape.
    f0 : float
        Kernel wavenumber.
    w0 : float
        Kernel 'angle' in radian.
    footprint_threshold : float, optional
        The kernel footprint is limited where envelop is larger than
        the value 'footprint_threshold'. Default is 0.05.
    
    Returns
    -------
    out : ndarray
        Kernel.

    """

    width = (-np.pi / 2 / np.log(footprint_threshold))**0.5
    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    k = gabor_xy(x, y, width, f0, w0)
    return k / np.sum(k)


def gabor_width(width, f0, w0, reference_shape, footprint_threshold=0.05):
    """
    Return a Gabor kernel as an ndarray of a given width.

    Kernel shape is deduced from the kernel width: the kernel footprint
    is limited to where envelop is larger than the value
    'footprint_threshold'.
    
    Parameters
    ----------
    
    width : float
        Kernel width.
    f0 : float
        Kernel wavenumber.
    w0 : float
        Kernel 'angle' in radian.
    reference_shape : (int, int)
        Reference shape of an ndarray representing a unit square of
        extent [0, 1]x[0, 1].
    footprint_threshold : float, optional
        The kernel footprint is limited where envelop is larger than
        the value 'footprint_threshold'. Default is 0.05.
    
    Returns
    -------
    out : ndarray
        Kernel.

    """

    r = (-np.log(footprint_threshold) / np.pi * 2 * width**2)**0.5
    n = int(np.ceil(r * max(reference_shape)))

    x, y = np.meshgrid(np.linspace(-r, r, n),
                       np.linspace(-r, r, n),
                       indexing='ij')
    k = gabor_xy(x, y, width, f0, w0)
    return k / np.sum(k)


def gabor_xy(x, y, width, f0, w0):
    """
    Return Gabor function value(s) at location(s) (x, y). The kernel is
    not normalized.

    Parameters
    ----------
    x : float or ndarray
        'x' locations.
    y : float or ndarray
        'y' locations.
    width : float
        Kernel width.
    f0 : float
        Kernel wavenumber.
    w0 : float
        Kernel 'angle' in radian.
    
    Returns
    -------
    out : float or ndarray
        Kernel value(s).
    """

    return np.exp(-np.pi*(x**2+y**2)/2/width**2) \
        * np.cos(2*np.pi*f0*(x*np.cos(w0)+y*np.sin(w0)))


def gaussian(shape, footprint_threshold=0.05):
    """
    Return values of a Gaussian kernel as an ndarray of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.
    footprint_threshold : float, optional
        The gaussian width is determined using the footprint
        threshold. Default is 0.05.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    r2 = x**2 + y**2
    k = np.exp(r2 * np.log(footprint_threshold))
    return k / np.sum(k)


def lorentzian(shape, footprint_threshold=0.05):
    """
    Return values of a "Lorentzian" kernel as an ndarray of a given shape.

    Parameters
    ----------
    shape : (int, int)
        Kernel nd array shape.
    footprint_threshold : float, optional
        The gaussian width is determined using the footprint threshold.
        Default is 0.05.
    
    Returns
    -------
    out : float or ndarray
        Kernel.
    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')

    cross_width = (1 / (1 / footprint_threshold - 1))**0.5
    k = lorentzian_xy(x, y, cross_width)
    return k / np.sum(k)


def lorentzian_xy(x, y, cross_width):
    """
    Return value(s) of a 2d-kernel based on the Lorentzian function at
    location(s) (x, y).  The kernel is not normalized.

    Parameters
    ----------
    x : float or ndarray
        'x' locations.
    y : float or ndarray
        'y' locations.
    cross_width : float
        Kernel cross width.
    
    Returns
    -------
    out : float or ndarray
        Kernel value(s).
    """

    r2 = x**2 + y**2
    k = 1 / (1 + r2 / cross_width**2)
    return k


def mountain_xy(x, y, cross_width, spanwise_width, alpha):
    """
    Return value(s) of a 'mountain' kernel at location(s) (x, y). The
    kernel is the product of a Lorentzian function (cross direction)
    with a gaussian enveloppe (spanwise direction).  The kernel is not
    normalized.

    Parameters
    ----------
    x : float or ndarray
        'x' locations.
    y : float or ndarray
        'y' locations.
    cross_width : float
        Kernel cross width.
    spanwise_width : float
        Kernel spanwise width.
    alpha : float
        Kernel 'angle' in radian.
    
    Returns
    -------
    out : float or ndarray
        Kernel value(s).
    """

    alpha += np.pi / 2

    xr = np.cos(alpha) * x - np.sin(alpha) * y
    yr = np.sin(alpha) * x + np.cos(alpha) * y

    k = cross_width**2 / (xr**2 + cross_width**2)
    k *= np.exp(-yr**2 / spanwise_width**2 / 2)
    return k


def parabolic(shape):
    """
    Return a "parabolic" 2d kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    k = (1 - x**2) * (1 - y**2)
    return k / np.sum(k)


def smooth_cosine(shape):
    """
    Return a smooth cosine kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    k = 0.5 + 0.5 * np.cos(np.pi * (x**2 + y**2)**0.5)
    k[np.where(x**2 + y**2 > 1)] = 0
    return k / np.sum(k)


def smooth_disk(shape, radius):
    """
    Return a smooth cosine kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.
    radius : float
        Inner disk radius, normalized by the kernel shape, e.g. radius in [0, 1].

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    r2 = x**2 + y**2
    radius2 = radius**2

    k = np.ones(shape)
    k = 0.5 + 0.5 * np.cos(np.pi * ((r2 - radius2) / (1 - radius2))**0.5)
    k[np.where(r2 > 1)] = 0
    k[np.where(r2 < radius2)] = 1

    return k / np.sum(k)


def smooth_square(shape, distance_exponent=0.5):
    """
    Return a smooth cosine kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')

    # use a smooth cosine along with a Chebyshev distance to get a
    # square
    r = np.maximum(np.abs(x), np.abs(y))
    k = 0.5 + 0.5 * np.cos(np.pi * r**distance_exponent)
    k[np.where(r > 1)] = 0
    return k / np.sum(k)


def tricube(shape):
    """
    Return a "tricube" 2d kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    r = (x**2 + y**2)**(1.5)
    k = (1 - r)**3
    k[np.where(r > 1)] = 0
    return k / np.sum(k)


def triweight(shape):
    """
    Return a "triweight" 2d kernel of a given shape.

    Parameters
    ----------

    shape : (int, int)
        Kernel nd array shape.

    Returns
    -------
    out : ndarray
        Kernel.

    """

    x, y = np.meshgrid(np.linspace(-1, 1, shape[0]),
                       np.linspace(-1, 1, shape[1]),
                       indexing='ij')
    r2 = x**2 + y**2
    k = (1 - r2)**3
    k[np.where(r2 > 1)] = 0
    return k / np.sum(k)
