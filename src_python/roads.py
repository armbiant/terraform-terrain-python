"""Road network generation.
"""

import copy
import numpy as np
import os
import scipy.interpolate
import scipy.spatial
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def dig_path(z, path, path_width=3, path_depth=0.05, sigma=2):
    """
    Dig a path on a heightmap.

    Parameters
    ----------
    z : ndarray
        Input array ("elevation" map).
    path : ndarray
        Path indices, integers, shape (n, 2).
    path_width : float, optional
        Path width (in pixels). Default is 3.
    path_depth : float, optional
        Path depth (absolute value). Default is 0.05.
    sigma : float, optional
        Gaussian filter half-width (in pixels). Default is 2.

    Returns
    -------
    out : ndarray, 2d
        Elevation map with path dug.
    out : ndarray, 2d
        Path mask: equals 1 where the path has been dug and zero elsewhere.
    """

    mask = np.zeros(z.shape)
    mask[path[:, 0], path[:, 1]] = 1

    # rescale elevation data to avoid negative values (not working
    # with grey dilation filter)
    zf = mask * scipy.ndimage.gaussian_filter(vt.op.remap(z), sigma=sigma)
    zf = scipy.ndimage.grey_dilation(zf, size=(path_width, path_width))

    # adjust mask footprint following path dilation (i.e. widening)
    mask[np.where(zf > 0)] = 1

    # rescale filtered elevation
    zmin, zmax = np.min(z), np.max(z)
    zf = zf * (zmax - zmin) + zmin - path_depth

    return (1 - mask) * z + mask * zf, mask


@vt.logger.show_input(log)
def find_path(z,
              idx_start,
              idx_end,
              subsampling=None,
              mask_nogo=None,
              distance_exponent=0.5):
    """
    Find the lowest elevation path between two points in a 2d-array.
    
    Parameters
    ----------
    z : ndarray
        Input array ("elevation" map).
    idx_start : (int, int)
        Starting point pair of indices.
    idx_end : (int, int)
        Ending point pair of indices.
    subsampling : bool, optional
        Subsampling rate of the initial array before applying the Dijkstra
        algorithm (which can be ressource intensive for large arrays). Default
        is None.
    mask_nogo : ndarray, optional
        A large penalization is added to "no go" zones in order to enforce
        avoidance of those regions for the path. Default is None
    distance_exponent : float, optional
        Exponent of the distance calculation between two points. Increasing
        the "distance exponent" of the cost function increases the cost of
        elevation gaps: path then tends to stay at the same elevation if
        possible (i.e. reduce the overall cumulative elevation gain). Default
        is 0.5.

    Returns
    -------
    out : ndarray
        Shortest path indices, integers, shape (n, 2).

    See also
    --------
    vt.op.dijkstra
    
    """

    if mask_nogo is not None:
        # add large penalization to "no go" zones
        z_p = z + 10 * np.max(z) * vt.op.remap(mask_nogo)
    else:
        z_p = z

    if subsampling is None:
        log.debug('starting Dijkstra algorithm (without subsampling)...')
        idx = vt.op.dijkstra(z_p, idx_start, idx_end, distance_exponent)
        path = np.vstack(([p[0] for p in idx], [p[1] for p in idx])).T

    else:
        log.debug('Dijkstra algorithm with subsampling...')
        z_sub = vt.op.subsample(z_p, subsampling)

        idx_start_sub = (int(np.floor(idx_start[0] / subsampling)),
                         int(np.floor(idx_start[1] / subsampling)))
        idx_end_sub = (int(np.floor(idx_end[0] / subsampling)),
                       int(np.floor(idx_end[1] / subsampling)))

        path_sub = vt.op.dijkstra(z_sub, idx_start_sub, idx_end_sub,
                                  distance_exponent)

        # send back to original index array and use interpolation to
        # link those discontinuous dots
        path_h = [(subsampling * p[0], subsampling * p[1]) for p in path_sub]
        path_h.append(idx_end)

        log.debug('interpolating...')
        t = np.linspace(0, 1, len(path_h))

        if len(path_h) > 3:
            itp_type = 'cubic'
        else:
            itp_type = 'linear'

        fitp_i = scipy.interpolate.interp1d(t, [p[0] for p in path_h],
                                            kind=itp_type)
        fitp_j = scipy.interpolate.interp1d(t, [p[1] for p in path_h],
                                            kind=itp_type)

        n = 2 * max(z.shape)  # wild guess
        path = np.vstack(
            (fitp_i(np.linspace(0, 1, n)), fitp_j(np.linspace(0, 1, n)))).T
        path = np.floor(path).astype(int)

        # add start and end points in case they were lost in
        # interpolation
        path = np.insert(path, 0, idx_start, axis=0)
        path = np.insert(path, -1, idx_end, axis=0)

        # remove consecutive duplicates
        path_u = [path[0, :]]
        for i in range(1, path.shape[0]):
            if path[i, 0] != path[i - 1, 0] or path[i, 1] != path[i - 1, 1]:
                path_u.append(path[i, :])
        path = np.array(path_u, dtype=int)

    return path


@vt.logger.show_input(log)
def generate_network(z,
                     nodes_idx,
                     nodes_weight=1,
                     subsampling=10,
                     distance_exponent=1,
                     mask_nogo=None):
    """
    Generate a large-scale road network between a set of nodes. 
    
    Parameters
    ----------
    z : ndarray
        Input array ("elevation" map).
    nodes_idx : ndarray (int)
        Indices (i, j) of nodes.
    nodes_weight : ndarray (int), optional
        Nodes weight, by default all the nodes have a weight equal to 1. 
    subsampling : bool, optional
        Subsampling rate of the initial array before applying the Dijkstra
        algorithm (which can be ressource intensive for large arrays). Default
        is None.
    mask_nogo : ndarray, optional
        A large penalization is added to "no go" zones in order to enforce
        avoidance of those regions for the path. Default is None
    distance_exponent : float, optional
        Exponent of the distance calculation between two points. Increasing
        the "distance exponent" of the cost function increases the cost of
        elevation gaps: path then tends to stay at the same elevation if
        possible (i.e. reduce the overall cumulative elevation gain). Default
        is 0.5.

    Returns
    -------
    weights : ndarray, int
        Weighted Connectivity matrix between the 'n' nodes. Triangular matrix
        of size (n, n). Separate primary and secondary roads: primary roads
        (positive weights) = roads with higher weights and necessary to
        connect the graph and secondary roads (negative weights) = alternative
        routes, not necessary to connect the nodes
    path_dict : dictionnary
        Contains the shortest path indices, integers, shape (n, 2), for nodes
        that are connected. Keys of the dictionnary are tuples (p, q) where p
        and q are the indices of the pair of nodes.
    
    """

    nd = len(nodes_idx)

    if not (isinstance(nodes_weight, np.ndarray)):
        nodes_weight *= np.ones(nodes_idx.shape[0])

    # base connectivity between the nodes is based on a Voronoi
    # diagram (gives a fully connected network without intersecting
    # connections)
    vor = scipy.spatial.Voronoi(nodes_idx)

    # --- connectivity matrix
    log.debug('computing connectivity matrix...')

    is_connected = np.zeros((nd, nd)).astype(bool)
    for rp in vor.ridge_points:
        p = min(rp[0], rp[1])
        q = max(rp[0], rp[1])
        is_connected[p, q] = True

    # --- adjacency (proximity) matrix and also store paths computed
    # --- between nodes
    log.debug('computing adjacency matrix (may take a while)...')

    path_dict = {}
    adjacency = np.zeros((nd, nd))
    for p in range(nd):
        for q in range(p + 1, nd):
            if is_connected[p, q]:
                path = vt.roads.find_path(z,
                                          idx_start=nodes_idx[p],
                                          idx_end=nodes_idx[q],
                                          subsampling=subsampling,
                                          distance_exponent=distance_exponent,
                                          mask_nogo=mask_nogo)
                d = ((path[:-1, 0] - path[1:, 0])**2 +
                     (path[:-1, 1] - path[1:, 1])**2)**0.5
                adjacency[p, q] = np.sum(d)

                path_dict[(p, q)] = path

    # --- shortest path between nodes
    log.debug('determining shortest path between nodes...')

    adjacency_sym = np.maximum(adjacency, adjacency.T)
    dist, pred = scipy.sparse.csgraph.shortest_path(adjacency_sym,
                                                    method='D',
                                                    return_predecessors=True)

    def get_path(pred, i, j):
        path = [j]
        k = j
        while pred[i, k] != -9999:
            path.append(pred[i, k])
            k = pred[i, k]
        return path[::-1]

    # 'weight' defines the usage of the connection between two nodes
    weights = np.zeros((nd, nd))
    for p in range(nd):
        for q in range(p + 1, nd):
            path = get_path(pred, p, q)
            if len(path) > 1:
                for i, v in enumerate(path[:-1]):
                    i1 = min(v, path[i + 1])
                    i2 = max(v, path[i + 1])
                    weights[i1,
                            i2] += 0.5 * (nodes_weight[i1] + nodes_weight[i2])

    # separate primary and secondary roads: primary roads (positive
    # weights) = roads with higher weights and necessary to connect
    # the graph and secondary roads (negative weights) = alternative
    # routes, not necessary to connect the nodes
    tmp_mat = np.zeros((nd, nd))
    componant_nb = nd

    while componant_nb > 1:
        idx_max = np.unravel_index(np.argmax(weights, axis=None),
                                   weights.shape)
        tmp_mat[idx_max[0], idx_max[1]] = weights[idx_max[0], idx_max[1]]
        weights[idx_max[0], idx_max[1]] = 0

        # number of connected components, should be 1 if all the nodes
        # are connected by the road network
        componant_nb = scipy.sparse.csgraph.connected_components(
            tmp_mat, directed=False)[0]

    weights = tmp_mat - weights

    return weights, path_dict
