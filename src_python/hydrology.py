'''
Wikipedia: "Hydrology [...] is the scientific study of the
movement, distribution, and management of water on Earth and other
planets, including the water cycle, water resources, and environmental
watershed sustainability."
'''

import numpy as np
import os
import scipy
#
import vterrain_core
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


@vt.logger.show_input(log)
def system_map(z,
               openclose_filter=True,
               area_threshold=None,
               iterations=5000,
               n_particles=10000,
               moisture_map=None,
               seed=-1):
    """
    Compute complete hydrology system: wrapper to compute pool and
    stream maps and determine their intersection.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    openclose_filter : bool, optional
        Determine wether an open/close binary filter is applied beforehand
        to remove background noise. Default is True.  
    area_threshold : float, optional
        Pool with an area (in pixels) smaller that 'area_threshold' are
        discarded. Default is None (all the pools are kept).
    iterations : int, float
        Maximum number of iterations ('particle lifetime'). Default is 5000.
    n_particles : int, float
        Maximum number of particles. Default is 10000.
    moisture_map : ndarray
        Moisture map (quantity of rain) as an ndarray between 0 and 1. Default
        is 'None', leading to a uniform moisture map.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).

    Returns
    -------
    out : ndarray
        Pool map: an ndarray of the same of the heightmap with pool contant
        elevations as values where the pools are located, and NaN elsewhere.
    out : ndarray
        Label map: an ndarray of the same of the heightmap with pool labels
        (1, 2, etc...) as values  where the pools are located, and 0 elsewhere.
    out : ndarray
        Stream map: log10 of the number of water particles passing through
        a points, normalized between 0 and 1.

    """

    # pool map
    pmap, mask_label = vt.hydrology.pool_map(z,
                                             openclose_filter=openclose_filter,
                                             area_threshold=area_threshold)

    # stream map, based on an elevation map with the pools filled
    z_pool = np.where(mask_label > 0, pmap, z)

    smap = vt.hydrology.stream_map(z_pool,
                                   iterations=iterations,
                                   n_particles=n_particles,
                                   moisture_map=moisture_map,
                                   seed=seed,
                                   depression_filling=False)

    # intersect smap and pmap
    labels = np.unique(mask_label)
    mask_label_inter = np.zeros(z.shape) * np.nan
    pmap_inter = np.zeros(z.shape) * np.nan
    smap_inter = smap

    for v in labels[1:]:
        idx = np.where(mask_label == v)
        m = np.zeros(z.shape)
        m[idx] = 1

        if np.sum(m * smap) > 0:
            pmap_inter[idx] = pmap[idx]
            mask_label_inter[idx] = mask_label[idx]
            smap_inter[idx] = 0

    return smap_inter, pmap_inter, mask_label_inter


@vt.logger.show_input(log)
def pool_map(z, openclose_filter=True, area_threshold=None):
    """
    Determine the pool maps of the input heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    openclose_filter : bool, optional
        Determine wether an open/close binary filter is applied beforehand
        to remove background noise. Default is True.  
    area_threshold : float, optional
        Pool with an area (in pixels) smaller that 'area_threshold' are
        discarded. Default is None (all the pools are kept).

    Returns
    -------
    out : ndarray
        Pool map: an ndarray of the same of the heightmap with pool contant
        elevations as values where the pools are located, and NaN elsewhere.
    out : ndarray
        Label map: an ndarray of the same of the heightmap with pool labels
        (1, 2, etc...) as values  where the pools are located, and 0 elsewhere.

    """

    # starting point is a depression filling algorithm
    zp = vt.hmap.depression_filling(z)

    # potential pools correspond to areas that have been filled
    mask = zp - z
    mask[np.where(mask > 0)] = 1

    # do some clean-up
    if openclose_filter:
        mask = scipy.ndimage.binary_opening(mask)
        mask = scipy.ndimage.binary_closing(mask)

    # image segmentation to separate pools
    mask_label, nlabels = scipy.ndimage.label(mask)

    areas = scipy.ndimage.sum(mask, mask_label, range(nlabels + 1))

    if area_threshold is not None:
        mask_size = areas < area_threshold
        mask_label[mask_size[mask_label]] = 0

        # re-label
        mask_label = np.searchsorted(np.unique(mask_label), mask_label)

    labels = np.unique(mask_label)

    # compute pond elevations and build up the map
    pmap = np.zeros(z.shape)
    elevations = []
    for v in labels:
        idx_v = np.where(mask_label == v)

        # lowest elevation, where the water flows out of the pool
        zmin = np.min(zp[idx_v])
        elevations.append(zmin)

        # mask needs to be shrinked a little, so that pool border is
        # at the same elevation all around the mask
        idx_p = np.where((mask_label == v) & (z <= zmin))
        pmap[idx_p] = zmin

        #
        mask_label[idx_v] = 0
        mask_label[idx_p] = v

    # remove 'background', only keep the pools
    pmap[np.where(mask_label == 0)] = np.nan

    return pmap, mask_label


@vt.logger.show_input(log)
def stream_map(z,
               iterations=500,
               n_particles=10000,
               moisture_map=None,
               p_inertia=0.01,
               depression_filling=True,
               seed=-1):
    """
    Compute the stream map, representing the water flowing on the heightmap.

    Parameters
    ----------
    z : ndarray
        Heightmap.
    iterations : int, float
        Maximum number of iterations ('particle lifetime'). Default is 500.
    n_particles : int, float
        Maximum number of particles. Default is 10000.
    moisture_map : ndarray
        Moisture map (quantity of rain) as an ndarray between 0 and 1. Default
        is 'None', leading to a uniform moisture map.
    p_inertia : float, optional
        Particle direction inertia. Default is 0.01.
    depression_filling : bool, optional
        Apply depression filling before-hand. Default is True.
    seed : int, optional
        Random seed, if set to -1, the random number generator is
        seeded with random data retrieved from the operating system.
        Default is -1. (Some random numbers are used by the particle system
        solver).

    Returns
    -------
    out : ndarray
        Stream map: log10 of the number of water particles passing through
        a points, normalized between 0 and 1.

    """

    if moisture_map is None:
        moisture_map = np.ones(z.shape)

    if depression_filling:
        zp = vt.hmap.depression_filling(z, eps=1e-6)
    else:
        zp = z

    smap = vterrain_core.stream_map(zp,
                                    iterations=iterations,
                                    n_particles=n_particles,
                                    moisture_map=moisture_map,
                                    p_inertia=p_inertia,
                                    seed=seed)

    smap = vt.op.remap(np.log10(smap + 1), vmin=0, vmax=1)

    return smap
