"""File input and ouput module.
"""

import cv2
import netpbmfile
import numpy as np
import os
#
import vterrain as vt

log = vt.logger.create(os.path.basename(__file__))


def load(fname, **kwargs):
    """
    File loader (main wrapper).

    Parameters
    ----------
    fname : string
        Path to the file.
    kwargs, optional
        Any additional input arguments that will be passed to the
        specific loader function.

    Returns
    -------
    out : ndarray
        Data.

    """

    extension = os.path.splitext(fname)[1]

    if extension == '.pgm':
        w = load_pgm(fname)

    elif extension == '.raw':
        w = load_raw(fname, **kwargs)

    elif extension == '.hgt':
        w = load_hgt(fname, **kwargs)

    else:  # last chance...
        log.debug('reading file: {}'.format(fname))
        # only keep first channel for image files
        w = np.flipud(cv2.imread(fname, 0)).T

    return w.astype(np.float32)


def load_hgt(fname, resolution):
    """
    'hgt' file loader.

    Parameters
    ----------
    fname : string
        Path to the file.
    resolution : string
        Image resolution, can be 'SRTM1' or 'SRTM3'.

    Returns
    -------
    out : ndarray
        Data.

    """

    if resolution == 'SRTM1':
        n = 3601
    else:
        n = 1201
    with open(fname, 'rb') as f:
        w = np.fromfile(f, np.dtype('>i2'), n * n).reshape((n, n))
    w = np.maximum(w, np.zeros(w.shape))
    return np.flipud(w).T


def load_pgm(fname):
    """
    'pgm' file loader.

    Parameters
    ----------
    fname : string
        Path to the file.

    Returns
    -------
    out : ndarray
        Data.

    """

    log.debug('reading pgm file: {}'.format(fname))
    w = netpbmfile.imread(fname)
    w = vt.op.remap(w, vmin=0, vmax=1)
    return w


def load_raw(fname, shape=None):
    """
    'raw' file loader.

    Parameters
    ----------
    fname : string
        Path to the file.
    shape : (int, int), optional
        Data ndarray shape. If not provided, the ndarray is assumed
        to be square and the shape is infered.

    Returns
    -------
    out : ndarray
        Data.

    """

    log.debug('reading raw file: {}'.format(fname))
    w = np.fromfile(fname, dtype=np.uint16).astype(np.float64)
    if shape is not None:
        w = w.reshape(shape)
    else:
        w = w.reshape((int(w.shape[0]**0.5), int(w.shape[0]**0.5)))
    return vt.op.remap(w, vmin=0, vmax=1)


def write(w, fname, **kwargs):
    """
    File writer (main wrapper).

    Parameters
    ----------
    w : ndarray
        Data.
    fname : string
        Path to the file.
    kwargs, optional
        Any additional input arguments that will be passed to the
        specific loader function.

    Returns
    -------
    None

    """

    extension = os.path.splitext(fname)[1]

    if extension == '.pgm':
        write_pgm(w, fname)

    elif extension == '.raw':
        write_raw(w, fname, **kwargs)

    else:  # last chance...
        log.debug('writing file: {}'.format(fname))
        w = np.array(vt.op.remap(w) * 255).astype('uint8')
        w = np.flipud(w.T)
        cv2.imwrite(fname, w)

    return None


def write_pgm(w, fname):
    """
    'pgm' file writer.

    Parameters
    ----------
    w : ndarray
        Data.
    fname : string
        Path to the file.

    Returns
    -------
    None
    """

    log.debug('writing pgm file: {}'.format(fname))
    wi = (65535 * vt.op.remap(w, vmin=0, vmax=1)).astype(np.uint16)
    netpbmfile.imwrite(fname, wi)
    return None


def write_raw(w, fname):
    """
    'raw' file writer.

    Parameters
    ----------
    w : ndarray
        Data.
    fname : string
        Path to the file.

    Returns
    -------
    None
    """

    log.debug('writing raw file: {}'.format(fname))
    zi = (65535 * vt.op.remap(w, vmin=0, vmax=1)).astype(np.uint16)
    with open(fname, 'wb') as f:
        zi.tofile(f)
    return None
