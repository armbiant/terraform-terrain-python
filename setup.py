import glob
from numpy.distutils.core import Extension
import os

lib_path = os.path.join('.', 'lib')

if __name__ == '__main__':
    from numpy.distutils.core import setup

    src_f90 = glob.glob(os.path.join(lib_path, '*.f90'))
    ext = [Extension(name='vterrain_core',
                     sources=src_f90)]
    
    setup(name='vterrain',
          packages=['vterrain'],
          package_dir={'vterrain': 'src_python'},
          ext_modules=ext)
    
